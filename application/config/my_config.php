<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Description of my_config
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Oct 31, 2011 8:09:28 PM
 * @filesource my_config.php
 */

// Asset URL 
$config['assets_url'] = '/assets/';
?>
