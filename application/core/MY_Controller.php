<?php
/**
 * Description of MY_Controller
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Oct 31, 2011 2:23:09 PM
 * @filesource MY_Controller.php
 */
class MY_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }
}

class FrontControler extends MY_Controller
{
    public $main_title = 'Dating Site';
    public $data = array();

    public function __construct()
    {
	parent::__construct();
	$this->template->set_layout('default');
    }
    
     public function refineUsersWithPhoto($users)
    {
        $newUsers = array();
        $newUser = array();
        $this->load->model('Photo_model', 'photo');
        
        foreach($users as $key=>$user)
        {
            $newUser = $user;
            $photo = $this->photo->getUserProfilePicture($user->id);     
            if(isset($photo['photo']))
                $newUser->photo = $photo['photo'];
            else
                $newUser->photo =null;
            
            $newUsers[$key] = $newUser;
        }       
        return $newUsers;
    }
}

?>