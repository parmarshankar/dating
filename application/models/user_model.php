<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model
{

    function __construct()
    {
	// Call the Model constructor
	parent::__construct();
    }

    public function addUser($post)
    {
	unset($post['password_confirm']);
	$this->db->insert('users', $post);
    }

    function register_user($username, $firstname, $lastname, $email, $location, $sex, $age, $password, $activation_code)
    {

	$enc_password = md5($password);
	$query_str = "INSERT INTO users(username,firstname,lastname,email,password,activation_code,activated,location,sex,age,bday) values(?,?,?,?,?,?,?,?,?,?)";
	$this->db->query($query_str, array($username, $firstname, $lastname, $email, $enc_password, $activation_code, 0, $location, $sex, $age,$bday));

	//Insert record in Profile table
	$insert_data = array(
	    'userid' => $this->getUserID($username)
	);
	$this->db->insert('profiles', $insert_data);
    }

    function confirm_registration($registration_code)
    {
	$query_str = "SELECT id FROM users WHERE activation_code = ?";
	$result = $this->db->query($query_str, $registration_code);
	if ($result->num_rows() == 1)
	{
	    $query_str = "UPDATE users SET activated = 1 WHERE activation_code = ?";
	    $this->db->query($query_str, $registration_code);
	    return true;
	}
	else
	{
	    return false;
	}
    }

    function check_exists_username($username)
    {
	$query_str = "SELECT username FROM users WHERE username = ? AND removed=0";
	$result = $this->db->query($query_str, $username);

	if ($result->num_rows() > 0)
	{
	    return false;
	}
	else
	{
	    return true;
	}
    }

    function getUserID($username)
    {
	$query_str = "SELECT id FROM users WHERE username = ? AND removed=0";
	$result = $this->db->query($query_str, $username);

	$row = $result->row();
        if(isset($row->id))
            return $row->id;
        else
           return 0; 
    }

    /**
     * Return User
     * @param type $username
     * @return type 
     */
    function getUser($username)
    {
	$this->db->select('*');
	$this->db->from('users');
	$this->db->where('username', $username);
        $this->db->where('removed', 0);

	return $this->db->get();
    }
    
    /**
     * Return User
     * @param type $username
     * @return type 
     */
    function getUserByEmail($email)
    {
	$this->db->select('*');
	$this->db->from('users');
	$this->db->where('email', $email);

	return $this->db->get();
    }
    
    /**
     * Make User verified
     */
    public function makeVerified($id)
    {
	$data = array('activated' => 1);
	$this->db->update('users', $data, "id ='" . $id . "'");
    }

    function getUserByID($username)
    {	
        $this->db->select('users.* ,user_locations.name as location');
        $this->db->from('users');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->where('users.id', $username);
	$query = $this->db->get();  
	return $query->row_array();
    }
    
    function getUserByUID($id)
    {
	$this->db->select('users.*, member_settings.telephone_no, users.sex, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age ,user_locations.name as location');
        $this->db->from('users');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('member_settings','users.id = member_settings.userid','LEFT'); 
        $this->db->where('users.id', $id);
	$query = $this->db->get();          
	return $query->result();
    }

    function validate($username, $password)
    {

	$this->db->where('username', $username);
	$this->db->where('password', md5($password));
        $this->db->where('removed', 0);
	$query = $this->db->get('users');

	if ($query->num_rows == 1)
	{
	    return true;
	}
	else
	{
	    return false;
	}
    }
    
    function validateUser($uid, $password)
    {

	$this->db->where('id', $uid);
	$this->db->where('password', $password);
        $this->db->where('removed', 0);
	$query = $this->db->get('users');

	if ($query->num_rows == 1)
	{
	    return true;
	}
	else
	{
	    return false;
	}
    }

//$zodiacSign->GetSign(date("m",time()),date("d",time()));



    function GetSign($month, $day)
    {
	$dat = getdate(mktime(2, 0, 0, $month, $day));
	$dat = $dat["yday"];
	$g = Array(13 => 356, 12 => 326, 11 => 296, 10 => 266, 9 => 235, 8 => 203, 7 => 172, 6 => 140, 5 => 110, 4 => 78, 3 => 51, 2 => 20, 1 => 0);
	foreach ($g as $key => $value)
	    if ($dat >= $value)
		break;
	if ($key > 12)
	    $key = 1;
	return $key;
    }

   function UpdateUser($update_data)
   {
       $userid = $this->session->userdata('id');  
       $this->db->where('id', $userid);
       $this->db->update('users', $update_data);
   }
   
   function isUserNameAvailble($username)
   {
        $userid = $this->session->userdata('id');  
        
	$this->db->select('*');	
	$this->db->where('username', $username);
        $this->db->where_not_in('id', $userid);
        $result = $this->db->get('users');
        return $result->result();
      
	
   }
   
   function increaseLoggedCount($id) {
        $date = date("Y-m-d");

        $this->db->select('logged_count');
        $this->db->from('users');
        $this->db->where('id', $id);       
        $query = $this->db->get();
        $result = $query->result();
        
        $new_count = (int) $result[0]->logged_count + 1;

        $this->db->set('logged_count', $new_count);
        $this->db->set('last_login', $date);
        $this->db->where('id', $id);        
        $this->db->update('users');
    }
    
    function getUserTypes()
    {
        $userTypes = array();
        $this->db->select('*');
        $this->db->from('user_types');
        $this->db->where('published', '1');  
        $this->db->order_by('order');
        $query = $this->db->get();
        $result = $query->result();
        
        foreach ($result as $rs)
        {
            $userTypes[$rs->type_id]  = $rs->name;
        }
        return $userTypes;
    }
    
    function getUserLocations($all=null)
    {
        $userLocations = array();
        $this->db->select('*');
        $this->db->from('user_locations');
        $this->db->where('published', '1');
        if($all==null)
            $this->db->where_not_in('code', '0');
        $this->db->order_by('order');
        $query = $this->db->get();
        $result = $query->result();
        
        foreach ($result as $rs)
        {
            $userLocations[$rs->id]  = $rs->name;
        }
        return $userLocations;
    }
    
    function getLocations($all=null)
    {
        $userLocations = array();
        $this->db->select('*');
        $this->db->from('user_locations');
        $this->db->where('published', '1');
        if($all==null)
            $this->db->where_not_in('code', '0');
        $this->db->order_by('order');
        $query = $this->db->get();
        $result = $query->result();        
        
        return $result;
    }
    
    function getTheLocation($id)
    {
        $userLocations = array();
        $this->db->select('*');
        $this->db->from('user_locations');
        $this->db->where('id', $id);       
        $this->db->order_by('order');
        $query = $this->db->get();
        $result = $query->result(); 
        return $result;
    }
    
    public function getAllUserCount()
    {
        $this->db->select('*');
        $this->db->where('removed', 0);
        $this->db->where('activated', 1);
	$query = $this->db->get('users');

	return $query->num_rows;
    }
    
    
    public function profile($currentid=null)
    {
	$this->load->model('Members_model', 'member');
	$this->load->model('User_model', 'user');
        $this->load->model('messages_model', 'message');
        $this->load->model('Flirter_model', 'flirter');
        $this->load->model('Lists_model', 'lists');
        $this->load->model('Stats_model', 'stats');
        $this->load->model('Photo_model', 'photo');
        
	if($currentid==null)
            $currentid = $this->input->get('id');
        
         if (!$this->member->record_exists($currentid)) {
             $insert_data = array(
                'userid' => $currentid
            );
            $this->member->InsertMembersProfile($insert_data);            
         }
         
	//select user data by userid
	$user = $this->user->getUserByID($currentid);

	//select profile data by userid
	$profile = $this->member->GetProfileByUserId($currentid);
        
         $insert_data = array(
            'userid' => $currentid		
        );
         
         if(count($this->member->GetLookingForByUserId($currentid))== 0)          
            $this->member->InsertMembersLookingFor($insert_data);       
       
        
        $profile['lookingFor'] = $this->member->GetLookingForByUserId($currentid);
         $profile['usertypes'] =$this->user->getUserTypes();
	
	//get only public photos
	$photos['photos'] = $this->photo->getPublicPhotosByUser($currentid);
        
        //get ALL photos
	//$photos['privatephotos'] = $this->photo->getPhotosByUser($currentid);
        $photos['photosCount'] = $this->photo->getPhotosCountByUser($currentid);
        
       
        $unReadCount['unReadMsg'] = $this->message->getUnReadCount($currentid);        
        
        $flirters['flirters'] = $this->flirter->getFlirters();
        
        //get friends order        
        $friends['friends'] =$this->refineUsersWithPhoto($this->lists->getFriendsByOrder($this->session->userdata('id')));
        $mem_settings['mem_settings'] = $this->member->GetMemberSettingByUserId($currentid);
        
        //update profile views
        $userid = $this->session->userdata('id');
        if($currentid != $userid){
        $this->stats->updateProfileView($currentid,$userid);
        
        /*notification data*/
        $text = 'Your profile is view by <a href="'.site_url('profile?id='.$userid).'" traget="_blank">'.$this->session->userdata('firstname'). '</a>';
        $insert_info = array(
            'user_id' => $currentid,
            'text' => $text            
        );
       
        $notf = $this->member->GetNotificationByUserId($currentid);
        $dontinsert = false;
        foreach($notf as $not)
        {
            if($not->text == $text )
            {
                $dontinsert = true;
            }
        }
        if(!$dontinsert)
            $this->member->InsertNotification($insert_info);
        
        }
        
	$view_data = array_merge($user, $profile, $photos,$unReadCount,$flirters,$friends,$mem_settings);
	return $view_data;

    }
    
    public function refineUsersWithPhoto($users)
    {
        $newUsers = array();
        $newUser = array();
        $this->load->model('Photo_model', 'photo');
        
        foreach($users as $key=>$user)
        {
            $newUser = $user;
            $photo = $this->photo->getUserProfilePicture($user->id);     
            if(isset($photo['photo']))
                $newUser->photo = $photo['photo'];
            else
                $newUser->photo =null;
            
            $newUsers[$key] = $newUser;
        }       
        return $newUsers;
    }
}

?>
