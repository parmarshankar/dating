<?php

class Event_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function insert($insert_data) {
        $this->db->insert('events', $insert_data);
        return $this->db->insert_id()  ;
    }
    
    function sendEventAlert($insert_data)
    {
        $this->db->insert('event_alert', $insert_data);
        $insert_id = $this->db->insert_id()  ;
        
        /*notification data*/        
        $this->load->model('Members_model', 'member');       
        $text = 'You have recieved a new <a href="'.site_url('event/alert').'" traget="_blank">event alert </a>';
        $insert_info = array(
            'user_id' => $insert_data['user_id'],
            'text' => $text            
        );
        $this->member->InsertNotification($insert_info);
    }
    function update($update_data)
    {
       $this->db->where('id', $update_data['id']);
       $this->db->update('events', $update_data);
    }

    function deleteEvent($id) {
        $this->db->where('id', $id);
        $this->db->delete('events');
    }

    function getEventCategoies() {
        $event_categories = array();
        $this->db->select('*');
        $this->db->from('event_categories');
        $this->db->order_by('order');
        $query = $this->db->get();
        $result = $query->result();

        foreach ($result as $rs) {
            $event_categories[$rs->id] = $rs->name;
        }
        return $event_categories;
    }
    
    function getEventCategoy($Categoyid) {
        $event_categories = array();
        $this->db->select('*');
        $this->db->from('event_categories');
        $this->db->where('id',$Categoyid);        
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function getEvents($num,$offset, $type='current')
    {
       //$type = current : all : past 
        $events = array();
        $today = date('Y-m-d');
        $this->db->select('events.*,users.firstname as addedName'); 
        $this->db->from('events');
         $this->db->join('users', 'events.event_added_by = users.id', 'left');
        if($type=='current')
            $this->db->where('held_date >=', $today);
        if($type=='past')
            $this->db->where('held_date <', $today);
        $this->db->order_by('held_date');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        $events = $query->result();
        $count=0;
        foreach ($events as $ev)
        {
            $this->db->select('*');
            $this->db->from('event_categories');
            $this->db->where_in('id',unserialize($ev->category_id));
            $this->db->order_by('order');
            $query = $this->db->get();
            $result = $query->result();
            $events[$count]->categories = $result;
            $events[$count]->guestCount = $this->getGuestCount($ev->id);
            $count++;           
        }
        return $events;
    }
    
    function getMyEvents($num,$offset, $type='current')
    {
       //$type = current : all : past 
        $userid = $this->session->userdata('id');  
        $events = array();
        $today = date('Y-m-d');
        $this->db->select('events.*,users.firstname as addedName'); 
         $this->db->distinct();
        $this->db->from('events');
         $this->db->join('users', 'events.event_added_by = users.id', 'left');
         $this->db->join('event_guests', 'events.id = event_guests.event_id', 'left');
        if($type=='current')
            $this->db->where('held_date >=', $today);
        if($type=='past')
            $this->db->where('held_date <', $today);
        
        $this->db->where('event_added_by', $userid);
        $this->db->or_where('event_guests.guest_id', $userid);
        
        $this->db->order_by('held_date');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        $events = $query->result();
        $count=0;
        foreach ($events as $ev)
        {
            $this->db->select('*');
            $this->db->from('event_categories');
            $this->db->where_in('id',unserialize($ev->category_id));
            $this->db->order_by('order');
            $query = $this->db->get();
            $result = $query->result();
            $events[$count]->categories = $result;
            $events[$count]->guestCount = $this->getGuestCount($ev->id);
            $count++;           
        }
        return $events;
    }
    
    function getEventsCount($type='current')
    {
       //$type = current : all : past 
        $today = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('events');
        if($type=='current')
            $this->db->where('held_date >=', $today);
        if($type=='past')
            $this->db->where('held_date <', $today);
        $this->db->order_by('held_date');
        $query = $this->db->get();       
        return $query->num_rows;
    }
    
    function getMyEventsCount($type='current')
    {
       //$type = current : all : past 
        $userid = $this->session->userdata('id');  
        $today = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('events');
        $this->db->join('event_guests', 'events.id = event_guests.event_id', 'left');
        if($type=='current')
            $this->db->where('held_date >=', $today);
        if($type=='past')
            $this->db->where('held_date <', $today);
        
        $this->db->where('event_added_by', $userid);
        $this->db->or_where('event_guests.guest_id', $userid);
         
        $this->db->order_by('held_date');
        $query = $this->db->get();       
        return $query->num_rows;
    }
    
    function getEventsByCategoryCount($category, $type='current')
    {
       //$type = current : all : past  
        $length = strlen((string)$category);        
        
        $today = date('Y-m-d');
        if($type=='current')
            $where ="`held_date` >= '".$today."'";
        if($type=='past')
            $where ="`held_date` < '".$today."'"; 
        $query_str = "SELECT * FROM (`events`) WHERE ".$where." AND `category_id` LIKE '%;s:$length:\"?\";%' ORDER BY `held_date`";
	$result = $this->db->query($query_str, $category);  
        return $result->num_rows;
    }
    
    function getEventsByLocationCount($location, $type='current')
    {
         //$type = current : all : past                
        $location = trim($location);
        if($location=='0')
        {
            $wherelocation='';
        }
        else {
        $wherelocation = " AND location ='$location'";
        }
        $today = date('Y-m-d');
        if($type=='current')
            $where ="`held_date` >= '".$today."'";
        if($type=='past')
            $where ="`held_date` < '".$today."'"; 
        $query_str = "SELECT * FROM (`events`) WHERE ".$where."  $wherelocation ORDER BY `held_date`";
	$result = $this->db->query($query_str);  
        return $result->num_rows;
    }
    
    function getEventsByCategoryLocationCount($category, $location, $type='current')
    {
         //$type = current : all : past  
        $length = strlen((string)$category);        
        $location = trim($location);
        if($location=='0')
        {
            $wherelocation='';
        }
        else {
        $wherelocation = " AND location ='$location'";
        }
        $today = date('Y-m-d');
        if($type=='current')
            $where ="`held_date` >= '".$today."'";
        if($type=='past')
            $where ="`held_date` < '".$today."'"; 
        $query_str = "SELECT * FROM (`events`) WHERE ".$where." AND `category_id` LIKE '%;s:$length:\"?\";%' $wherelocation ORDER BY `held_date`";
	$result = $this->db->query($query_str, $category);  
        return $result->num_rows;
    }
    
    function getEvent($id)
    {       
        $events = array();
        $today = date('Y-m-d');
        $this->db->select('events.*,users.firstname as addedName'); 
        $this->db->from('events');        
        $this->db->join('users', 'events.event_added_by = users.id', 'left');
        $this->db->where('events.id', $id);
        $query = $this->db->get();
        $events = $query->result();
        
        $count=0;
        foreach ($events as $ev)
        {
            $this->db->select('*');
            $this->db->from('event_categories');
            $this->db->where_in('id',unserialize($ev->category_id));
            $this->db->order_by('order');
            $query = $this->db->get();
            $result = $query->result();
            $events[$count]->categories = $result;
            $count++;           
        }
        
        if(isset($events[0]))
            return $events[0];
        else {
            return 0;
        }
    }
    
    function registerEvent($insert_data)
    {
        $this->db->select('*');
        $this->db->from('event_guests');
        $this->db->where('event_id', $insert_data['event_id']);
        $this->db->where('guest_id', $insert_data['guest_id']);
        $query = $this->db->get(); 
        
                
        if($query->num_rows== 0)         
           $this->insertEventGuest($insert_data);
        else
            $this->updateEventGuest($insert_data);
    }
    
    function insertEventGuest($insert_data)
    {
         $this->db->insert('event_guests', $insert_data); 
    }
    
    function updateEventGuest($update_data)
    {
         $this->db->where('event_id', $update_data['event_id']);
         $this->db->where('guest_id', $update_data['guest_id']);
         $this->db->update('event_guests', $update_data);
    }
    
    function getGuestCount($event_id)
    {
         $this->db->select('*');
         $this->db->from('event_guests'); 
         $this->db->where('event_id', $event_id);
         $query = $this->db->get(); 
         
         return $query->num_rows;
    }
    
    function getGuests($event_id)
    {
         $this->db->select('event_guests.*, users.id, users.sex, user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
         $this->db->from('event_guests');         
         $this->db->join('users','event_guests.guest_id = users.id','LEFT');
         $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
         $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
         $this->db->where('event_guests.event_id', $event_id);
         $query = $this->db->get();          
         return $query->result();
    }
    
    function getEventsByCategory($category,$num,$offset, $type='current')
    {
       //$type = current : all : past 
         $length = strlen((string)$category);        
        $events = array();
        $today = date('Y-m-d');
       if($type=='current')
            $where ="`held_date` >= '".$today."'";
        if($type=='past')
            $where ="`held_date` < '".$today."'";     
        
        $query_str = "SELECT `events`.*, `users`.`firstname` as addedName FROM (`events`) LEFT JOIN `users` ON `events`.`event_added_by` = `users`.`id` WHERE $where  AND `category_id` LIKE '%;s:$length:\"?\";%'  ORDER BY `held_date` LIMIT $offset,$num";
        
        $query = $this->db->query($query_str, $category);          
        
        $events = $query->result();
        $count=0;
        foreach ($events as $ev)
        {
            $this->db->select('*');
            $this->db->from('event_categories');
            $this->db->where_in('id',unserialize($ev->category_id));
            $this->db->order_by('order');
            $query = $this->db->get();
            $result = $query->result();
            $events[$count]->categories = $result;
            $events[$count]->guestCount = $this->getGuestCount($ev->id);
            $count++;           
        }
        return $events;
    }
    
    public function getEventsByCategoryLocation($location,$category,$num,$offset, $type='current')
    {
         //$type = current : all : past 
         $length = strlen((string)$category);  
         $location = trim($location);
        if($location=='0')
        {
            $wherelocation='';
        }
        else {
        $wherelocation = " AND events.location ='$location'";
        }
         
        $events = array();
        $today = date('Y-m-d');
       if($type=='current')
            $where ="`held_date` >= '".$today."'";
        if($type=='past')
            $where ="`held_date` < '".$today."'";     
        
        $query_str = "SELECT `events`.*, `users`.`firstname` as addedName FROM (`events`) LEFT JOIN `users` ON `events`.`event_added_by` = `users`.`id` WHERE $where  AND `category_id` LIKE '%;s:$length:\"?\";%' $wherelocation ORDER BY `held_date` LIMIT $offset,$num";
        
        $query = $this->db->query($query_str, $category);          
        
        $events = $query->result();
        $count=0;
        foreach ($events as $ev)
        {
            $this->db->select('*');
            $this->db->from('event_categories');
            $this->db->where_in('id',unserialize($ev->category_id));
            $this->db->order_by('order');
            $query = $this->db->get();
            $result = $query->result();
            $events[$count]->categories = $result;
            $events[$count]->guestCount = $this->getGuestCount($ev->id);
            $count++;           
        }
        return $events;
    }
    
    public function getEventsByLocation($location,$num,$offset, $type='current')
    {
         //$type = current : all : past 
        
         $location = trim($location);
        if($location=='0')
        {
            $wherelocation='';
        }
        else {
        $wherelocation = " AND events.location ='$location'";
        }
         
        $events = array();
        $today = date('Y-m-d');
       if($type=='current')
            $where ="`held_date` >= '".$today."'";
        if($type=='past')
            $where ="`held_date` < '".$today."'";     
        
        $query_str = "SELECT `events`.*, `users`.`firstname` as addedName FROM (`events`) LEFT JOIN `users` ON `events`.`event_added_by` = `users`.`id` WHERE $where   $wherelocation ORDER BY `held_date` LIMIT $offset,$num";
        
        $query = $this->db->query($query_str);          
        
        $events = $query->result();
        $count=0;
        foreach ($events as $ev)
        {
            $this->db->select('*');
            $this->db->from('event_categories');
            $this->db->where_in('id',unserialize($ev->category_id));
            $this->db->order_by('order');
            $query = $this->db->get();
            $result = $query->result();
            $events[$count]->categories = $result;
            $events[$count]->guestCount = $this->getGuestCount($ev->id);
            $count++;           
        }
        return $events;
    }
    
    public function getLocations($type='current')
    {
         $today = date('Y-m-d');
        $this->db->select(' location'); 
        $this->db->distinct(); 
        $this->db->from('events');      
        if($type=='current')
            $this->db->where('held_date >=', $today);
        if($type=='past')
            $this->db->where('held_date <', $today);
        $this->db->order_by('held_date');        
        $query = $this->db->get();
        return $query->result();
    }
    
    /******************Event allerts********************/
    public function getEventAlertTemplate($code)
    {
         $this->db->select('template');
         $this->db->from('event_allert_templates');
         $this->db->where('code',$code);
         $query = $this->db->get();
         return $query->result();
    }
    
    public function getSuitableUsers($event_id)
    {
        $this->db->select('users.id');
        $this->db->from('users');
        $this->db->join('events','events.location = users.location','LEFT');
        $this->db->where('events.id',$event_id);
        $query = $this->db->get();
        return $query->result();
        
    }
    
    public function getUnReadAllertCount($userid)
    {
        $this->db->select('*');
        $this->db->from('event_alert');
        $this->db->where('event_alert.user_id',$userid);
        $this->db->where('event_alert.read','0');
        $query = $this->db->get();          
        return $query->num_rows;
    }
    
    public function getAllertCount($userid)
    {
        $this->db->select('*');
        $this->db->from('event_alert');
        $this->db->where('event_alert.user_id',$userid);        
        $query = $this->db->get();          
        return $query->num_rows;
    }
    
    public function getAllerts($userid,$num,$offset)
    {
        $this->db->select('*');
        $this->db->from('event_alert');
        $this->db->where('event_alert.user_id',$userid);
        $this->db->order_by('date','DESC');
        $this->db->limit($num, $offset);
        $query = $this->db->get();          
        return $query->result();
    }
    
    function updateRead($id)
    {
	$this->db->set('read', 1);
	$this->db->where('id', $id);
	$this->db->update('event_alert');
    }
    
    function deleteAlert($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('event_alert');
    }

}

?>