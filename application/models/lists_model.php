<?php

class Lists_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function sendRequest($insert_data) {
        $this->db->insert('user_lists', $insert_data);
    }
    
    function getRequest($userid, $list_user, $list_code) {
        $this->db->select('*');
        $this->db->from('user_lists');
        $this->db->where('userid', $userid);
        $this->db->where('list_user', $list_user);
        $this->db->where('list_code', $list_code);
        $query = $this->db->get();
        return $query->result();
    }
    
    function getMyProfileViews()
    {
        $userid = $this->session->userdata('id');
        $this->db->select('profile_views.last_viewed, profile_views.count,users.firstname, users.lastname, users.id, users.sex,user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
        $this->db->from('profile_views');
        $this->db->join('users', 'users.id = profile_views.viewed_by', 'left');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('profiles','users.id = profiles.userid','right');
        $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
        $this->db->join('member_settings','users.id = member_settings.userid','LEFT'); 
        $this->db->where('profile_views.profile_id', $userid);
        $this->db->where('member_settings.not_let_know_i_view', NULL);
        $this->db->or_where('member_settings.not_let_know_i_view', 0);
        $this->db->order_by('last_viewed', 'DESC');
        $this->db->limit(20,0);
        $query = $this->db->get();        
        return $query->result();
        
    }
    
    function getProfileViewsByMe()
    {
        $userid = $this->session->userdata('id');
        $this->db->select('profile_views.last_viewed, profile_views.count, users.firstname, users.lastname,users.id, users.sex,user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
        $this->db->from('profile_views');
        $this->db->join('users', 'users.id = profile_views.profile_id', 'left');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('profiles','users.id = profiles.userid','right');
        $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
        $this->db->where('profile_views.viewed_by', $userid);
        $this->db->order_by('last_viewed','DESC');
        $this->db->limit(20,0);
        $query = $this->db->get();        
        return $query->result();        
    }

    function getFriendsInLists($list_code=null, $accepted=null) {
        $userid = $this->session->userdata('id');

        $this->db->select('users.firstname, users.lastname, date, users.id, users.sex,user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
        $this->db->from('user_lists');
        $this->db->join('users', 'users.id = user_lists.list_user', 'left');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('profiles','users.id = profiles.userid','right');     
        $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
        $this->db->where('user_lists.userid', $userid);
        
        if($accepted != null)
            $this->db->where('user_lists.accepted', $accepted);

        if ($list_code != null) {
            $this->db->where('user_lists.list_code', $list_code);
        }
        $query = $this->db->get();
        
        return $query->result();
    }

    function getFriendsInListsR($list_code=null, $accepted=null) {
        $userid = $this->session->userdata('id');

        $this->db->select('users.firstname, users.lastname, date, users.id, users.sex,user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
        $this->db->from('user_lists');
        $this->db->join('users', 'users.id = user_lists.userid', 'left');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('profiles','users.id = profiles.userid','right');  
        $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
        $this->db->where('user_lists.list_user', $userid);
        
        if($accepted != null)
        $this->db->where('user_lists.accepted', $accepted);

        if ($list_code != null) {
            $this->db->where('user_lists.list_code', $list_code);
        }
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function updateAccepted($userid, $list_user, $list_code) {
        $this->db->set('accepted', 1);
        $this->db->where('userid', $userid);
        $this->db->where('list_user', $list_user);
        $this->db->where('list_code', $list_code);
        $this->db->update('user_lists');
    }

    function getUnReadFriendRequestCount($id) {
        $this->db->select('COUNT(*) as count');
        $this->db->from('user_lists');        
        $this->db->where('accepted', 0);
        $this->db->where('list_code', 'FRIEND');
        $this->db->where('list_user', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    // freind requests  send to me
    function getPendingRequests($id,$list_code=null) {
        $this->db->select('users.firstname, users.lastname, date, users.id, users.sex,user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
        $this->db->from('user_lists');
        $this->db->join('users', 'users.id = user_lists.userid', 'left');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('profiles','users.id = profiles.userid','right');
        $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
        $this->db->where('user_lists.accepted', 0);
        $this->db->where('user_lists.list_code', $list_code);
        $this->db->where('user_lists.list_user', $id);
        $this->db->order_by('date');
        $query = $this->db->get();
        return $query->result();
    }
    
    //send freind requests by me
    function getPendingRequestsR($id,$list_code=null) {
        $this->db->select('users.firstname, users.lastname, date, users.id, users.sex,user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
        $this->db->from('user_lists');
        $this->db->join('users', 'users.id = user_lists.list_user', 'left');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('profiles','users.id = profiles.userid','right');  
        $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
        $this->db->where('user_lists.accepted', 0);
        $this->db->where('user_lists.list_code', $list_code);
        $this->db->where('user_lists.userid', $id);
        $this->db->order_by('date');
        $query = $this->db->get();
        return $query->result();
    }
    
    //delete from list
    function delete($user_id, $list_user, $code)
    {
       $this->db->where('userid', $user_id);
       $this->db->where('list_user', $list_user);
       $this->db->where('list_code', $code);
       $this->db->delete('user_lists');  
    }
    
    //ordering friends
    public function insertFriendOrder($insert_data)
    {
        $this->db->insert('my_friends', $insert_data);
    }
    
    public function deleteFriendOrder($user_id)
    {
       $this->db->where('userid', $user_id);
       $this->db->delete('my_friends');  
    }
    
    public function deleteFriendOrderR($user_id, $friend_id)
    {
       $this->db->where('userid', $user_id);
       $this->db->where('friend_id', $friend_id);
       $this->db->delete('my_friends');  
       
       $this->db->where('friend_id', $user_id);
       $this->db->where('userid', $friend_id);
       $this->db->delete('my_friends');
       
    }
    
    public function getFriendsByOrder($user_id)
    {
        $this->db->select('my_friends.order, users.firstname, users.lastname, users.id, users.sex,user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location');
        $this->db->from('my_friends');
        $this->db->join('users', 'users.id = my_friends.friend_id', 'left');
        $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
        $this->db->join('profiles','users.id = profiles.userid','right');
        $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
        $this->db->where('my_friends.userid',$user_id);
        $this->db->order_by('order');
        $query = $this->db->get();
        $order = $query->result();
        
        $friends = array();
        $friends_order = array();
        foreach($order as $or)
        {
            if($or->id > 0)
            {
                if(!isset($friends[$or->id]))
                       $friends_order[] = $or;

                $friends[$or->id] = $or;
            }
        }
        return $friends_order; 
    }
    
    function isBlockedMe($id)
    {
        $userid = $this->session->userdata('id');
        
	$query_str = "SELECT * FROM user_lists WHERE userid= ? AND list_user=$userid AND list_code='BLOCK'";
	$result = $this->db->query($query_str, $id);

	if ($result->num_rows() > 0)
	{
	    return true;
	}
	else
	{
	    return false;
	}
    }
    
    public function getHotModels()
    {
        $this->db->select('Count(list_user) as cnt ,list_user as id, users.firstname, users.username ');        
        $this->db->from('user_lists');       
        $this->db->join('users', 'users.id = user_lists.list_user', 'left');
        $this->db->where('user_lists.list_code', 'HOT');
        $this->db->group_by('user_lists.list_user');
        $this->db->order_by('cnt','DESC');
        $this->db->limit(16,0);
       
        $query = $this->db->get();
        
        return $query->result();
    }

}

