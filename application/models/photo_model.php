<?php


class Photo_model extends CI_Model
{

    function __construct()
    {
	// Call the Model constructor
	parent::__construct();
    }

    function insertInfo($insert_data)
    {
	$this->db->insert('photo_info', $insert_data);
    }
    
    function record_exists($photo_id)
    {
        $query_str = "SELECT count(*) cnt FROM photo_info WHERE photo_id = ?";
        $result = $this->db->query($query_str, $photo_id);

        $row = $result->row();
        $cnt = $row->cnt;
        if ($cnt == 1) return true; else return false;
    }

    function getPhotoInfo($id)
    {
	$this->db->select('photo_info.*,photo_category.name as cat_name, photos.photo');
	$this->db->from('photo_info');
        $this->db->join('photos', 'photo_info.photo_id = photos.id', 'left');   
        $this->db->join('photo_category', 'photo_info.category = photo_category.id', 'left');
             
        $this->db->order_by('photos.id','DESC');        
	
	$this->db->where('photo_id', $id);
	$query = $this->db->get();
	return $query->row();
    }
    
    function getPhotosByUser($userid)
    {
        $this->db->select('photo_info.*,photo_category.name as cat_name, photos.photo, photos.id as pid');
        $this->db->from('photo_info');
        $this->db->join('photos', 'photo_info.photo_id = photos.id', 'right');   
        $this->db->join('photo_category', 'photo_info.category = photo_category.id', 'left');             
        $this->db->where('photos.userid',$userid);
        $this->db->order_by('photos.id','DESC'); 
        $result = $this->db->get();
        return $result->result();

    }
    
    function getPhotosCountByUser($userid)
    {
        $photos = null;
        $this->db->select('photo_category.*');
        $this->db->from('photo_category');       
        $this->db->group_by('photo_category.id','DESC'); 
        $result = $this->db->get();
        $cats = $result->result();
        $c = 0;
        foreach( $cats as $cat)
        {            
            $ph = $this->getPhotosByUserAbllum($userid, $cat->id);            
                $cats[$c]->count = count($ph);
            $c ++;
        }
        return $cats;

    }
    
    function getPublicPhotosByUser($userid)
    {
        $this->db->select('photo_info.*,photo_category.name as cat_name, photos.photo, photos.id as pid');
        $this->db->from('photo_info');
        $this->db->join('photos', 'photo_info.photo_id = photos.id', 'right');   
        $this->db->join('photo_category', 'photo_info.category = photo_category.id', 'left');             
        $this->db->where('photos.userid',$userid);
        $this->db->where('photo_category.ispublic',1);
        $this->db->order_by('photo_category.name','DESC'); 
        $result = $this->db->get();
        return $result->result();

    }
    
    function getPhotosByUserAbllum($userid,$album)
    {
        $this->db->select('photo_info.*,photo_category.name as cat_name, photos.photo, photos.id as pid, photo_category.ispublic');
        $this->db->from('photo_info');
        $this->db->join('photos', 'photo_info.photo_id = photos.id', 'right');   
        $this->db->join('photo_category', 'photo_info.category = photo_category.id', 'left');             
        $this->db->where('photos.userid',$userid);
        $this->db->where('photo_category.id',$album); 
        $result = $this->db->get();
        return $result->result();

    }
    
    function getPhotoCategorys()
    {
	$this->db->select('*');
	$this->db->from('photo_category');  
	$query = $this->db->get();
	return $query->result();
    }


    function updateCategory($id,$category)
    {
	$this->db->set('category', $category);
	$this->db->where('photo_id', $id);
	$this->db->update('photo_info');
    }
    
    function updateName($id,$name)
    {
	$this->db->set('name', $name);
	$this->db->where('photo_id', $id);
	$this->db->update('photo_info');
    }
    
    function updateDescription($id,$description)
    {
	$this->db->set('description', $description);
	$this->db->where('photo_id', $id);
	$this->db->update('photo_info');
    }
    
    
    function deletePhotoInfo($id)
    {
        $this->db->where('photo_id', $id);
        $this->db->delete('photo_info');        
    }
    
    function hasAccess($id)
    {
        $userid = $this->session->userdata('id');
        
        if (!$this->session->userdata('logged')) 
            return false;
        
	$query_str = "SELECT * FROM user_lists WHERE list_user= $userid AND userid=? AND list_code='PVTPHOTO'";
	$result = $this->db->query($query_str, $id);

	if ($result->num_rows() > 0)
	{
	    return true;
	}
	else
	{
	    return false;
	}
    }
    
    function getUserProfilePicture($userid)
    {
        $this->db->select('photo');
        $this->db->from('photos');        
        $this->db->join('photo_info', 'photos.id = photo_info.photo_id', 'left');   
        $this->db->join('photo_category', 'photo_info.category = photo_category.id', 'left');             
        $this->db->where('photos.userid',$userid);
        $this->db->where('photo_category.ispublic',1);
        $this->db->order_by('photos.id','DESC'); 
        $query = $this->db->get();
        return $query->row_array();
    }

}
