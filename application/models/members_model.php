<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Members_model extends CI_Model  {

   function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
 
 function record_exists($userid)
   {
      $query_str = "SELECT count(*) cnt FROM profiles WHERE userid = ?";
      $result = $this->db->query($query_str, $userid);

     $row = $result->row();
     $cnt = $row->cnt;
     
     if ($cnt == 1) return true; else return false;



   }

   function InsertMembersProfile($insert_data)
   {
      $this->db->insert('profiles', $insert_data);
   }

   function insert_photo($insert_data)
   {
      $re = $this->db->insert('photos', $insert_data);
      return $re;
   }
   
   function getPhotoIDbyName($name)
   {
      $this->db->select('*');
      $this->db->where('photo',$name);
      $result = $this->db->get('photos');
      return $result->row_array();
   }


   function UpdateMembersProfile($update_data)
   {
       $this->db->where('userid', $update_data['userid']);
       $this->db->update('profiles', $update_data);
   }

    function GetProfileByUserId($userid)
   {
      $this->db->select('*');
      $this->db->where('userid',$userid);
      $result = $this->db->get('profiles');
      return $result->row_array();

   }
   
   function get_photos_by_id($userid)
   {
      $this->db->select('*');
      $this->db->where('userid',$userid);
      $result = $this->db->get('photos');
      return $result->result();

   }
   
   function getPhoto($id)
   {
      $this->db->select('photo');
      $this->db->where('id',$id);
      $result = $this->db->get('photos');
      return $result->result();

   }
   
   function deletePhoto($id)
   {
       $this->db->where('id', $id);
       $this->db->delete('photos');    

   }
   

   /****************looking for**************/
   function InsertMembersLookingFor($insert_data)
   {
      $this->db->insert('user_lookinfor', $insert_data);
   }
   
   function deleteMembersLookingFor($id)
   {
       $this->db->where('userid', $id);
       $this->db->delete('user_lookinfor');    

   }

   function UpdateMembersLookingFor($update_data)
   {
       $this->db->where('userid', $update_data['userid']);
       $this->db->update('user_lookinfor', $update_data);
   }

    function GetLookingForByUserId($userid)
   {
      $this->db->select('*');
      $this->db->where('userid',$userid);
      $result = $this->db->get('user_lookinfor');
      return $result->row_array();

   }

/****************member setting**************/
   function InsertMembersSetting($insert_data)
   {
      $this->db->insert('member_settings', $insert_data);
   }
   
   function deleteMembersSetting($id)
   {
       $this->db->where('userid', $id);
       $this->db->delete('member_settings');    

   }

   function UpdateMembersSetting($update_data)
   {
       $this->db->where('userid', $update_data['userid']);
       $this->db->update('member_settings', $update_data);
   }

   function GetMemberSettingByUserId($userid)
   {
      $this->db->select('*');
      $this->db->where('userid',$userid);
      $result = $this->db->get('member_settings');
      return $result->row_array();

   }
   
   function notAcceptNewMessages($userid)
   {
       $this->db->select('not_accept_new_msg');
       $this->db->where('userid',$userid);
       $result = $this->db->get('member_settings');
       return $result->result();
   }
   
   function getProfileComplete($userid)
   {
       $retunval['totalvalue'] = 0;
       $retunval['nextlink']= ''; 
       $retunval['nexttitle'] = ''; 
       $this->load->library('xml');
       $this->xml->load('core/profilecomplete');
       $completeSettings = $this->xml->parse(); 
       foreach($completeSettings as $cs)
       {           
           foreach($cs[0] as $sec)
           {
               foreach($sec as $section)
               {
                   $value = $section['value'][0];
                   $title = $section['title'][0];
                   $link = $section['link'][0];
                   $sql = $section['sql'][0];
                   $numofrs = $section['numofresults'][0];
                   $criteria = $section['criteria'][0];
                   $result = $this->db->query($sql, $userid);
                   
                    if ( $criteria=='larger' && $result->num_rows() > $numofrs)
                    {
                       $retunval['totalvalue'] += $value;
                    }
                    else if ( $criteria=='smaller' && $result->num_rows() < $numofrs)
                    {
                       $retunval['totalvalue'] += $value;
                    }
                    else if ( $criteria=='equal' && $result->num_rows() == $numofrs)
                    {                       
                       $retunval['totalvalue'] += $value;
                    }
                    else
                    {
                        
                        $retunval['nextlink']= $link; 
                        $retunval['nexttitle'] = $title; 
                       
                    }
               
               }
           }
       }
      
       return $retunval;
   }


/****************notification**************/

   function InsertNotification($insert_data)
   {
      $this->db->insert('notification', $insert_data);
   }
   
   function deleteNotification($id)
   {
       $this->db->where('id', $id);
       $this->db->delete('notification');    

   }

   function UpdateNotification($update_data)
   {
       $this->db->where('id', $update_data['id']);
       $this->db->update('notification', $update_data);
   }

   function GetNotificationByUserId($userid)
   {
      $this->db->select('*');
      $this->db->where('user_id',$userid);
      $this->db->from('notification'); 
      $this->db->where('read', 0);
      $this->db->order_by('datetime','DESC');
      $this->db->limit(3,0);
      $result = $this->db->get();
      return $result->result();
   }
      
}

?>
