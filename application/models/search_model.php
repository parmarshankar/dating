<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Search_model extends CI_Model  {

   function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

   function searchAll($aged1,$aged2,$location,$city_suburb,$postcode,$looking_for,$date_joined)
   {
       $last_week = date('Y-m-d', strtotime("-1 week"));
       $last_month = date('Y-m-d', strtotime("-1 month"));
       
    $this->db->select("users.id, user_lookinfor.* ,users.sex, user_types.name as cat, FROM_DAYS(TO_DAYS(now()) - TO_DAYS(users.bday)) as age , users.username, user_locations.name as location, member_settings.visible_to_others");
    $this->db->from('users');
    $this->db->join('profiles','users.id = profiles.userid','right');   
    $this->db->join('user_locations','users.location = user_locations.id','LEFT'); 
    $this->db->join('user_lookinfor','users.id = user_lookinfor.userid','LEFT' );
    $this->db->join('user_types','users.sex = user_types.type_id','LEFT' );
    $this->db->join('member_settings','users.id = member_settings.userid','LEFT'); 
    
    $where = "YEAR(CURDATE())-YEAR(users.bday) BETWEEN ".$aged1." AND ".$aged2."  ";
    
    if($location != '0') $where.= " AND users.location like '".$location."' ";  
    if(trim($city_suburb) != '') $where.= " AND users.city_suburb like '".trim($city_suburb)."' ";  
    if(trim($postcode) != '') $where.= " AND users.postcode like '".trim($postcode)."' "; 
    
    if($looking_for !='')  $where.= "AND users.sex = '".$looking_for."' "; 
    
    if($date_joined=='Last week')  $where.= "AND users.date_joined > '".$last_week."' ";
    
    if($date_joined=='Last month')  $where.= "AND users.date_joined > '".$last_month."' ";
    
    $where.= "AND (member_settings.visible_to_others =  '1' OR  member_settings.visible_to_others IS NULL) ";
    
    $this->db->where($where);
    $result = $this->db->get();

    //$str = $this->db->last_query();
    //    var_dump($str);
    //die();
    //return $str;
    return $result->result();

   }

   



}
?>
