<?php


class Flirter_model extends CI_Model
{

    function __construct()
    {
	// Call the Model constructor
	parent::__construct();
    }

    //create a new filler 
    function create_flirter($insert_data)
    {
	$this->db->insert('flirter', $insert_data);
        
    }
    
    //send filrter to user
    function send_flirter($insert_data)
    {
	$this->db->insert('user_flirter', $insert_data);
         $insert_id = $this->db->insert_id()  ;
        
        /*notification data*/
        $this->load->model('User_model', 'user');
        $this->load->model('Members_model', 'member');
        $u = $this->user->getUserByUID($insert_data['from']);
       
        $text = 'You have revieved <a href="'.site_url('flirter/view/'.$insert_id.'/action/inbox').'" traget="_blank">flirter from '. $u[0]->firstname. '</a>';
        $insert_info = array(
            'user_id' => $insert_data['to'],
            'text' => $text            
        );
        $this->member->InsertNotification($insert_info);
    }

    function getUnReadCount($id)
    {
	$this->db->select('COUNT(*) as count');
	$this->db->from('user_flirter');        
	$this->db->join('users', 'users.id = user_flirter.from', 'left');        
	$this->db->where('read', 0);
	$this->db->where('to',$id);

	$query = $this->db->get();
	return $query->row();
    }
    
    function getUserFlirtersCount($id, $action=null)
    {
	$this->db->select('user_flirter.* ');
	$this->db->from('user_flirter');
        
	if ($action == 'sent')
	    $this->db->join('users', 'users.id = user_flirter.to', 'left');
	else
	    $this->db->join('users', 'users.id = user_flirter.from', 'left');
        
        $this->db->order_by('user_flirter.created','DESC');
        
	$trash = $action == 'trash' ? 1 : 0;
	$this->db->where('trash', $trash);

	if($action == 'sent')
	    $this->db->where('from',$id);
	else
	    $this->db->where('to',$id);        
        
	$query = $this->db->get();       
        return count($query->result_array());
    }
    

    //get all filirters
    function getFlirters($published=1 ,$flirter_category_id=0)
    {
	$this->db->select('flirter.*, flirter_category.id as cat_id, flirter_category.category as cat_name');
	$this->db->from('flirter');        
        $this->db->join('flirter_category', 'flirter_category.id = flirter.category_id', 'inner');
	if ($flirter_category_id != 0)
        {
	    $this->db->where('flirter_category.id',$flirter_category_id);
        }
        
        if ($published != 0)
        {
	    $this->db->where('flirter.published',$published);
        }
        
        $this->db->order_by('flirter.category_id, flirter.text','DESC'); 
	$query = $this->db->get();
	return $query->result();
    }
    
    function getUserFlirters($id,$num,$offset, $action=null)
    {
	$this->db->select('user_flirter.*,users.firstname');
	$this->db->from('user_flirter');
        
	if ($action == 'sent')
	    $this->db->join('users', 'users.id = user_flirter.to', 'left');
	else
	    $this->db->join('users', 'users.id = user_flirter.from', 'left');
        
        $this->db->order_by('user_flirter.created','DESC');
        
	$trash = $action == 'trash' ? 1 : 0;
	$this->db->where('trash', $trash);

	if($action == 'sent')
	    $this->db->where('from',$id);
	else
	    $this->db->where('to',$id);

        $this->db->limit($num, $offset);
	$query = $this->db->get();

	return $query->result_array();
    }
    
    function getUserFlirter($id, $action)
    {
	$this->db->select('user_flirter.*,users.firstname');
	$this->db->from('user_flirter');
	if ($action == 'inbox' || $action == 'trash')
	    $this->db->join('users', 'users.id = user_flirter.from', 'left');
	else
	    $this->db->join('users', 'users.id = user_flirter.to', 'left');

	$this->db->where('user_flirter.id', $id);
	$query = $this->db->get();

	return $query->row();


    }
    
    function updateRead($id)
    {
	$this->db->set('read', 1);
	$this->db->where('id', $id);
	$this->db->update('user_flirter');
    }
    
    //update filtter 
    function UpdateFlirter($update_data)
    {
       $this->db->where('id', $update_data['id']);
       $this->db->update('flirter', $update_data);
    }

    function getFlirter($id)
    {
	$this->db->select('flirter.*');
	$this->db->from('flirter');	
	$this->db->where('flirter.id', $id);
	$query = $this->db->get();
	return $query->row();
    }
    
   
    function getFlirterCategories()
    {
        $this->db->select('flirter_category.*');
        $this->db->from('flirter_category');
        $query = $this->db->get();
        return $query->result();
    }    
  
    function updatePublished($id)
    {
	$this->db->set('published', 1);
	$this->db->where('id', $id);
	$this->db->update('flirter');
    }
    
    function moveToTrash($id)
    {
	$this->db->set('trash', 1);
	$this->db->where('id', $id);
	$this->db->update('user_flirter');
    }
    
    function moveToInbox($id)
    {
        $this->db->set('trash', 0);
	$this->db->where('id', $id);
	$this->db->update('user_flirter');
    }
    
    function deleteUserFlirter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user_flirter');        
    }

}

