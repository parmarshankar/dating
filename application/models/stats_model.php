<?php

class Stats_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function addProfileView($insert_data) {
        $this->db->insert('profile_views', $insert_data);
    }

    function increaseProfileView($insert_data) {
        $date = date("Y-m-d");

        $this->db->select('count');
        $this->db->from('profile_views');
        $this->db->where('profile_id', $insert_data['profile_id']);
        $this->db->where('viewed_by', $insert_data['viewed_by']);
        $query = $this->db->get();
        $result = $query->result();

        $new_count = (int) $result[0]->count + 1;

        $this->db->set('count', $new_count);
        $this->db->set('last_viewed', $date);
        $this->db->where('profile_id', $insert_data['profile_id']);
        $this->db->where('viewed_by', $insert_data['viewed_by']);
        $this->db->where_not_in('last_viewed', $insert_data['last_viewed']);
        $this->db->update('profile_views');
    }

    function updateProfileView($currentid, $userid) {
        $date = date("Y-m-d");
        $query_str = "SELECT * FROM profile_views WHERE profile_id= ? AND viewed_by='" . $userid . "'";
        $result = $this->db->query($query_str, $currentid);

        $insert_data = array(
            'profile_id' => $currentid,
            'viewed_by' => $userid,
            'last_viewed' => $date
        );

        if ($result->num_rows() > 0) {
            $this->increaseProfileView($insert_data);
        } else {
            $this->addProfileView($insert_data);
        }
        
     
        
    }
    
    public function getLoginCount($id)
    {
       $this->db->select('logged_count,last_login');
       $this->db->from('users');
       $this->db->where('id', $id);       
       $query = $this->db->get();
       $result = $query->result();      
       return $result[0];
    }
    
    public function getProfileViewCount($id, $dateFrom=null)
    {
        $this->db->select('COUNT(*) as viewcnt');
        $this->db->from('profile_views');
        $this->db->where('profile_id', $id);
        if($dateFrom != null)           
            $this->db->where('last_viewed >=', $dateFrom);
        
       $query = $this->db->get();
       $result = $query->result();
       return $result[0]->viewcnt;
    }
    
    public function getProfileViewMyCount($id, $dateFrom=null)
    {
        $this->db->select('COUNT(*) as viewcnt');
        $this->db->from('profile_views');
        $this->db->where('viewed_by', $id);
        if($dateFrom != null)           
            $this->db->where('last_viewed >=', $dateFrom);
        
       $query = $this->db->get();
       $result = $query->result();
       return $result[0]->viewcnt;
    }
    
    public function getFriendCount($id)
    {
        $this->db->select('userid as fid ');
        $this->db->from('user_lists');
        $this->db->where('list_code', 'FRIEND');
        $this->db->where('list_user', $id);
        $this->db->where('accepted', '1');
        $query = $this->db->get();
        $result = $query->result_array();
        
        $this->db->select('list_user as fid ');
        $this->db->from('user_lists');
        $this->db->where('list_code', 'FRIEND');
        $this->db->where('userid', $id);
        $this->db->where('accepted', '1');
        $query = $this->db->get();        
        $result2 = $query->result_array();
        
        $results = array_merge($result,$result2);
        $friens= array();
        foreach($results as $rs)
        {
            if($id!=$rs['fid'])
                $friens[$rs['fid']]  = $rs['fid'];
        }
        
        return $friens;
    }
    
    public function hotListCount($id)
    {
        $this->db->select('COUNT(*) as cnt ');
        $this->db->from('user_lists');
        $this->db->where('list_code', 'HOT');
        $this->db->where('list_user', $id);       
        $query = $this->db->get();        
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function shortListCount($id)
    {
        $this->db->select('COUNT(*) as cnt ');
        $this->db->from('user_lists');
        $this->db->where('list_code', 'SHORT');
        $this->db->where('list_user', $id);       
        $query = $this->db->get();        
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getMessageFromCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('messages');
        $this->db->where('from',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getMessageReplyCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('messages');
        $this->db->where_in('reply','SELECT `id` FROM `messages` WHERE `from` ='.$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getMessageToCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('messages');
        $this->db->where('to',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getFlirterFromCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('user_flirter');
        $this->db->where('from',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getFlirterReplyCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('user_flirter');
        $this->db->where_in('reply','SELECT `id` FROM `user_flirter` WHERE `from` ='.$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getFlirterToCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('user_flirter');
        $this->db->where('to',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getPostEventCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('events');
        $this->db->where('event_added_by',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function getInterestEventCount($id)
    {
        $this->db->select('COUNT(*) as cnt');
        $this->db->from('event_guests');
        $this->db->where('guest_id',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->cnt;
    }

}

?>