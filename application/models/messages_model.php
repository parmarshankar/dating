<?php

/**
 * Description of message
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Nov 12, 2011 12:36:49 PM
 * @filesource message.php
 */
class Messages_model extends CI_Model
{

    function __construct()
    {
	// Call the Model constructor
	parent::__construct();
    }

    function send_message($insert_data)
    {
	$this->db->insert('messages', $insert_data);
        $insert_id = $this->db->insert_id()  ;
        
        /*notification data*/
        $this->load->model('User_model', 'user');
        $this->load->model('Members_model', 'member');
        $u = $this->user->getUserByUID($insert_data['from']);
       
        $text = 'You have recieved <a href="'.site_url('message/view/'.$insert_id.'/action/inbox').'" traget="_blank">message from '. $u[0]->firstname. '</a>';
        $insert_info = array(
            'user_id' => $insert_data['to'],
            'text' => $text            
        );
        $this->member->InsertNotification($insert_info);
    }
    function getMessagesCount($id, $action=null)
    {
	$this->db->select('messages.* ');
	$this->db->from('messages');
        
	if ($action == 'sent')
	    $this->db->join('users', 'users.id = messages.to', 'left');
	else
	    $this->db->join('users', 'users.id = messages.from', 'left');
        
        $this->db->order_by('messages.created','DESC');
        
	$trash = $action == 'trash' ? 1 : 0;
	$this->db->where('trash', $trash);

	if($action == 'sent')
	    $this->db->where('from',$id);
	else
	    $this->db->where('to',$id);        
        
	$query = $this->db->get();       
        return count($query->result_array());
    }
    
    function getMessages($id, $num,$offset, $action=null)
    {
	$this->db->select('messages.*,users.firstname');
	$this->db->from('messages');
        
	if ($action == 'sent')
	    $this->db->join('users', 'users.id = messages.to', 'left');
	else
	    $this->db->join('users', 'users.id = messages.from', 'left');
        
        $this->db->order_by('messages.created','DESC');
        
	$trash = $action == 'trash' ? 1 : 0;
	$this->db->where('trash', $trash);

	if($action == 'sent')
	    $this->db->where('from',$id);
	else
	    $this->db->where('to',$id);

       // $this->db->limit(3,$from);
        $this->db->limit($num, $offset);
	$query = $this->db->get();

	return $query->result_array();
    }
    
    function getUnReadCount($id)
    {
	$this->db->select('COUNT(*) as count');
	$this->db->from('messages');        
	$this->db->join('users', 'users.id = messages.from', 'left');        
	$this->db->where('read', 0);
	$this->db->where('to',$id);

	$query = $this->db->get();
	return $query->row();
    }

    function getMsg($id, $action)
    {
	$this->db->select('messages.*,users.firstname');
	$this->db->from('messages');
	if ($action == 'inbox' || $action == 'trash')
	    $this->db->join('users', 'users.id = messages.from', 'left');
	else
	    $this->db->join('users', 'users.id = messages.to', 'left');

	$this->db->where('messages.id', $id);
	$query = $this->db->get();

	return $query->row();


    }

    function moveToTrash($id)
    {
	$this->db->set('trash', 1);
	$this->db->where('id', $id);
	$this->db->update('messages');
    }
    
    function moveToInbox($id)
    {
        $this->db->set('trash', 0);
	$this->db->where('id', $id);
	$this->db->update('messages');
    }


    function updateRead($id)
    {
	$this->db->set('read', 1);
	$this->db->where('id', $id);
	$this->db->update('messages');
    }
    
    function deleteForever($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('messages');        
    }
    
    function getAutoReplyMsg($userid)
    {
        $this->db->select('member_settings.auto_reply_text');
	$this->db->from('member_settings');
	
	$this->db->where('member_settings.userid', $userid);
        $this->db->where('member_settings.auto_reply_on_off', '1');
	$query = $this->db->get();

	return $query->row();
    }

}

/* End of file messages_model.php */
/* Location: ./application/controllers/messages_model.php */