<html>
    <head>
	<title>User Registration | Dating Site</title>	
    </head>
    <body style="font: 12px Arial,Helvetica,sans-serif;margin: 0;padding: 0;">
	<div id="top" style="min-height: 400px;width: 600px;padding: 20px;">
	    <br />
	    <p style="font: 12px tahoma,verdana, Arial, Helvetica, Garuda, sans-serif;">Dear <?=$first_name;?>,</p>	    
	    <p style="font: 12px tahoma,verdana, Arial, Helvetica, Garuda, sans-serif;">To activate your account & verify your details, please click on</p>
	    <p><?=$active_url;?></p>	
	    <p style="font: 12px tahoma,verdana, Arial, Helvetica, Garuda, sans-serif;">If the link above does not work upon clicking, please copy and paste it to your browser.</p>
	    <p style="font: 12px tahoma,verdana, Arial, Helvetica, Garuda, sans-serif;">Thanks again for joining us!<br/>
		Administrator &nbsp;</p>	  
	    <br/>
	    <p style="font: 12px tahoma,verdana, Arial, Helvetica, Garuda, sans-serif;">Note: This is a computer generated message, do not reply to it.</p>
	    <br/>	    
	</div>
    </body>
</html>