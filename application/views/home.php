<link href="../../assets/css/tabular.css" rel="stylesheet" type="text/css"/>
<script src="../../assets/js/functions.js" type="text/javascript"></script> 
<div id="home-header">
    <div id="home-header-left"> 
        
        <?php if (!$this->session->userdata('logged')): ?>
        <div id="home-login" class="home-top-form">
            <div class="main-module-head">Members Login</div>
            <div id="home_form_error_1" class="home_form_errors">
                    <?php echo validation_errors(); ?>                    
            </div>
            <div class="main-module-content">
                <?=form_open('user/login',array('name' => 'user_login', 'id' => 'user_login'))?>                
                <table>
                    <tbody>                    
                    <tr> 
                        <td class="home-form-lable"><label>Username</label></td>
                        <td class="home-form-input"><input type="text" id="username" name="username" value="<?=set_value('username');?>" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"><label>Password</label></td>
                        <td class="home-form-input"><input type="password" id="password" name="password" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"></td>
                        <td class="home-form-input">
                            <input type="submit" name="Login" value="Login"/>
                            <input type="hidden" name="location" value="home"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <?=form_close();?>
            </div>
            <div class="main-module-foot"></div>
        </div>
        
        <div id="home-registration" class="home-top-form">
           <div class="main-module-head"> Quick Registration </div>           
           <div class="main-module-content">
               <?=form_open('user/quickregister',array('name' => 'user_register', 'id' => 'user_register'))?>
                <table>
                    <tbody>
                    <tr> 
                        <td class="home-form-lable"><label>Email</label></td>
                        <td class="home-form-input"><input type="text" id="email" name="email" value="<?=set_value('email');?>" /></td>
                    </tr>
                    <tr> 
                        <td class="home-form-lable"><label>Username</label></td>
                        <td class="home-form-input"><input type="text" id="username" name="username" value="<?=set_value('username');?>" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"><label>Password</label></td>
                        <td class="home-form-input"><input type="password" id="password" name="password" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"><label>Verify Password</label></td>
                        <td class="home-form-input"><input type="password" id="password_confirm" name="password_confirm" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"></td>
                        <td class="home-form-input"><input type="submit" name="Join" value="Join"/></td>
                    </tr>
                    </tbody>
                </table>
                <?=form_close();?>
           </div>
           <div class="main-module-foot"></div>
        </div>  
        <?php endif;?>
        <?php if ($this->session->userdata('logged')): ?>
            <!--TODO: show member data-->
            <?php echo $template['partials']['member_data']; ?>           
        <?php  endif;?>
    </div>    
    <div id="home-header-r">
    	<div id="home-header-middle">
            <table>
                <tr>
                    <td><img src="assets/images/noImageAvailable.jpg" alt="image1"/></td>
                    <td><img src="assets/images/noImageAvailable.jpg" alt="image2"/></td>
                    <td><img src="assets/images/noImageAvailable.jpg" alt="image2"/></td>
                </tr>
                <tr>
                    <td colspan="3">Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para </td>                    
                </tr>
                <tr>
                    <td colspan="3"> <div class="stats"> <span class="statsnumber"><?=$getAllUserCount?></span> member listings - <span class="statsnumber">0,000</span> online now - <span class="statsnumber">00</span> in webcam chat</div></td>                    
                </tr>
            </table>
	</div>
        <div id="home-header-right">
           <div id="home-quick-search" class="quick-search">               
                    <div class="quick-search-head">Quick Search</div>
                    <?php echo form_open('search/searchresults'); ?>
                    <div class="quick-search-content">
                        
                            <table>
                                <tr> 
                                     <td class="home-form-lable"><label>looking for a</label></td>
                                    <td class="home-form-input"> 
                                         <?php echo form_dropdown('looking_for', $user_types); ?>
                                    </td>
                                </tr>
                                <tr> 
                                    <td class="home-form-lable"><label>Seeking</label></td>
                                    <td class="home-form-input">                                        
                                         <?php echo form_dropdown('seeking', $user_types); ?>
                                    </td>
                                   
                                    
                                </tr>
                                <tr> 
                                    <td class="home-form-lable"><label>between</label></td>
                                    <td>
                                        <input type="text" style="width: 17pt" id="aged1" maxlength="2" value="18" name="aged1">
                                        &nbsp;<label>and</label>&nbsp;
                                        <input type="text" style="width: 17pt" id="aged2" maxlength="2" value="99" name="aged2">
                                    </td>
                                </tr>
                                <tr> 
                                    <td class="home-form-lable"><label>in</label></td>
                                    <td class="home-form-input">
                                       
                                         <?php echo form_dropdown('location', $location_options); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="home-form-lable"></td>
                                    <td class="home-form-input"><input type="submit" name="Search" value="Search"/></td>
                                </tr>
                            </table>
                        
                    </div>
                    <?php echo form_close();?>
                    <div class="quick-search-foot"></div>               
            </div>            
            <div id="home-latest-artical"class="tabular" >
               <?php if (!$this->session->userdata('logged')): ?>
              <img src="assets/images/noImageAvailable.jpg" alt="image2"/>
              <?php  endif;?>
              <?php if ($this->session->userdata('logged')): ?>            
                <?php echo $template['partials']['profile_complete']; ?>
            <?php  endif;?>
            </div>    
        </div>   
    </div>   
</div>
<div class="clear"></div>
<div id="home-middle1">    
    <div id="home-middle1-left" class="home-middle">
        <div id="tablule-l1" class="tabular">
            <div class="tabbed_area_gallery">                
                <ul class="tabs-gallery">
                    <li><a href="javascript:tabSwitch_2(1, 3, 'tab_', 'content_');" id="tab_1" class="active">Hot Models</a></li>
<!--                    <li><a href="javascript:tabSwitch_2(2, 3, 'tab_', 'content_');" id="tab_2">Top Amature Pics</a></li>-->
                    
                </ul>

                <div id="content_1" class="content">
                     <ul>
                        <li>
                        <table>
                            <?php 
                            $count = 0;
                            ?>
                            <?php foreach($hot as $hm):?>
                            <?php $count ++; 
                            $name = $hm->username;
                            if($hm->firstname !='')
                                    $name = $hm->firstname;
                            
                                if($count % 4 == 1) echo "<tr>";
                            ?>
                                <td>
                                    <a href ="profile?id=<?=$hm->id?>" traget="_blank">
                                    <?php if($hm->photo !=''):?>
                                    <img src="uploads/<?=$hm->photo?>" title="<?=$name?>" />
                                    <?php endif;?>
                                    <?php if($hm->photo ==''):?>
                                    <img src="assets/images/no_avatar.jpg" title="<?=$name?>" />
                                    <?php endif;?>
                                    </a>
                                </td>
                            <?php 
                             if($count % 4 == 0) echo "</tr>";
                            ?>                            
                            <?php endforeach;?>                            
                        </table>
                        </li>
                    </ul>                     
                </div>
<!--                <div id="content_2" class="content">
                         <ul>
                        <li>
                            <table>
                            <tr>
                                <td><img src="assets/images/no_avatar.jpg" /></td>
                                <td><img src="assets/images/no_avatar.jpg" /></td>
                                <td><img src="assets/images/no_avatar.jpg" /></td>
                                <td><img src="assets/images/no_avatar.jpg" /></td>
                            </tr>
                            <tr>
                                <td><img src="assets/images/no_avatar.jpg" /></td>
                                <td><img src="assets/images/no_avatar.jpg" /></td>
                                <td><img src="assets/images/no_avatar.jpg" /></td>                                
                            </tr>
                            
                        </table>
                        </li>                        
                        </ul>
                </div>
               -->

            </div>
        </div>
        <div id="tabluler-l2" class="tabular">
            <div class="tabbed_area">
                <ul class="tabs">
                    <li><a href="javascript:tabSwitch_2(1, 3, 'tabl2_', 'contentl2_');" id="tabl2_1" class="active">Tab1</a></li>
                    <li><a href="javascript:tabSwitch_2(2, 3, 'tabl2_', 'contentl2_');" id="tabl2_2" class="">Tab2</a></li>
                    <li><a href="javascript:tabSwitch_2(3, 3, 'tabl2_', 'contentl2_');" id="tabl2_3" class="">Tab3</a></li>
                </ul>
                <div id="contentl2_1" class="content">
                     <ul>
                        <ul>
                        <li> Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para
</li>                            
                        </ul>
                    </ul>                     
                </div> 
                <div id="contentl2_2" class="content">
                     <ul>
                        <ul>
                        <li> 1. Sample text </li>  
                        <li> 2. Sample text </li>                            
                        </ul>
                    </ul>                     
                </div> 
                <div id="contentl2_3" class="content">
                     <ul>
                        <ul>
                        <li> 1. Sample text </li>  
                        <li> 2. Sample text </li>
                        <li> 3. Sample text </li>
                        </ul>
                    </ul>                     
                </div> 
            </div>
        </div>
    </div>
     <div id="home-middle1-m" >
        <div id="home-middle1-middle" class="">  
            <div id="tabluler-m1" class="">
                <div id="tablule-l1" class="">
                    <div class="tabbed_area">
                        <ul class="tabs">
                            <li><a href="javascript:tabSwitch_2(1, 2, 'tabm_', 'contentm_');" id="tabm_1" class="active">Tab1</a></li>
                            <li><a href="javascript:tabSwitch_2(2, 2, 'tabm_', 'contentm_');" id="tabm_2">Tab2</a></li>
                            
                        </ul>

                        <div id="contentm_1" class="content">
                             <ul>
                                <li>
                                 <img src="assets/images/noImageAvailable.jpg" alt="image2"/>
                                 <p>Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para   </p>
                                </li>                        
                             </ul>                     
                        </div>
                        <div id="contentm_2" class="content">
                                 
                             <ul>
                                <li>
                                Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para. Sample content para
                                </li>
                            </ul> 
                        </div>                  

                    </div>
                </div>
            </div>
            <div id="tabluler-m2">
                <img src="assets/images/noImageAvailable.jpg" alt="image2"/>
            </div>
            <div id="tabluler-m3" class="tabular">
                <div class="tabbed_area">
                    <ul class="tabs">
                        <li><a href="javascript:tabSwitch_2(1, 2, 'tabm2_', 'contentm2_');" id="tabm2_1" class="active">Tab1</a></li>
                        
                    </ul>
                    <div id="contentm2_1" class="content">
                         <ul>
                            <ul>
                            <li> 1. Sample text </li>  
                            <li> 2. Sample text </li>                            
                            </ul>
                        </ul>                     
                    </div> 
                </div>
            </div>
        </div>
         <div id="home-middle1-right" class="">
            <div id="tabluler-r1" class="tabular">
                <div class="tabbed_area">
                    <ul class="tabs">
                        <li><a href="javascript:tabSwitch_2(1, 2, 'tabr_', 'contentr_');" id="tabr_1" class="active">Tab1</a></li>
                        <li><a href="javascript:tabSwitch_2(2, 2, 'tabr_', 'contentr_');" id="tabr_2">Tab2</a></li>

                    </ul>

                    <div id="contentr_1" class="content">
                         <ul>
                            <li>
                            <img src="assets/images/noImageAvailable.jpg" alt="image2"/>
                             <p>Sample text. Sample text </p>
                            </li>
                            <li>
                            <img src="assets/images/noImageAvailable.jpg" alt="image2"/>
                             <p>Sample text. Sample text </p>
                            </li>
                            <li>
                            <img src="assets/images/noImageAvailable.jpg" alt="image2"/>
                             <p>Sample text. Sample text </p>
                            </li>
                        </ul>                     
                    </div>
                    <div id="contentr_2" class="content">
                            <ul>
                            <li> Sample text. </li>  
                            <li> Sample text. </li>
                            <li> Sample text. </li>
                            <li> Sample text. </li>
                            <li> Sample text. </li>
                            <li> Sample text. </li>
                            <li> Sample text. </li>
                            <li> Sample text. </li>
                            <li> Sample text. </li>
                            </ul>
                    </div>                  

                </div>
        </div>
    </div>   
    </div>
     
</div>
<div class="clear"></div>
<div id="home-middle2">
    
    <div id="home-middle2-left" class="home-middle"> 
    	<h2>Dating search</h2>
    </div>
    <div id="home-middle2-r">
    <div id="home-middle2-middle" class="home-middle">   
         <h2>What's New</h2>
         <hr/>
         <div class="content">
         <ul>
        <li> Sample text. </li>  
        <li> Sample text. </li>
        <li> Sample text. </li>
        <li> Sample text. </li>
        <li> Sample text. </li>
        <li> Sample text. </li>
        <li> Sample text. </li>
        </ul>
        </div>
    </div>
    <div id="home-middle2-right" class=""> 
         <img src="assets/images/noImageAvailable.jpg" alt="image2"/>       
    </div>
    </div>
</div>
<div class="clear"></div>



