<html>
<head>
<title>Search</title>
<style type="text/css">

form li {
  list-style: none;
}


</style>
</head>
<body>

<?php echo anchor(base_url().'members/view_profile', 'View my profile'); ?>
<br/>
<?php echo anchor(base_url().'members/edit_profile', 'Edit my profile'); ?>
<br/>
<?php echo anchor(base_url().'members/my_photos', 'My photos'); ?>
<br/>
<?php echo anchor(base_url().'user/logout', 'Logout'); ?>
<br/>
<h4>Search</h4>
<?php
    echo form_open(base_url().'members/searchresults');
?>
<?php
$looking_for = array(
                'Men' => 'Men',
                'Women' => 'Women',
                'Couples' => 'Couples',
                'Groups' => 'Groups',
                'Couples (FF)' => 'Couples (FF)',
                'Couples (MM)' => 'Couples (MM)',
                'TV/TS' => 'TV/TS'

                );

$aged = array(
                '18' => '18',
                '20' => '20',
                '25' => '25',
                '30' => '30',
                '35' => '35',
                '40' => '40',
                '45' => '45',
                '50' => '50',
                '55' => '55',
                '60' => '60',
                '65' => '65',
                '70' => '70',
                '80' => '80',
                '90' => '90'
                );

 $location_options = array(
                  'Australian Capital Territory'  => '- Australian Capital Territory',
                  'New South Wales'               => '- New South Wales',
                  'Northern Territory'            => '- Northern Territory',
                  'Queensland'                    => '- Queensland',
                  'South Australia'               => '- South Australia',
                  'Tasmania'                      => '- Tasmania',
                  'Victoria'                      => '- Victoria',
                  'Western Australia'             => '- Western Australia'
                );
?>
<ul>
    <li>
        <label>I am looking for</label>
        
         <?php echo form_dropdown('looking_for', $looking_for); ?>
        
    </li>

    <li>
        <label>Seeking</label>

         <?php echo form_dropdown('seeking', $looking_for); ?>

    </li>
    <li>
        <label>Aged between</label>
         <?php echo form_dropdown('aged1', $aged); ?>
        and
        <?php echo form_dropdown('aged2', $aged); ?>
    </li>
    <li>
        <label>Location</label>
         <?php echo form_dropdown('location', $location_options); ?>
       
    </li>
</ul>

<h4>Display preferences</h4>
<label>Show me members:</label>
<ul>
    <li>
        
        <?php 
        echo form_checkbox('who_are_online_now');
        ?>
        Who are online now
    </li>
    <li>

        <?php
        echo form_checkbox('with_photos','1');

        ?>
        With photos
    </li>
    <li>

        <?php
        echo form_checkbox('who_are_verified');
        ?>
        Who are verified
    </li>
    <li>

        <?php
        echo form_checkbox('who_are_looking_for_me');
        ?>
        Who are looking for me
    </li>
</ul>

<?php
$date_joined = array(
                'Any time'    => 'Any time',
                'Last week'   => 'Last week',
                'Last month'  => 'Last month'
);
?>
<label>Date Joined:</label>
<ul>
    <li>
     <?php echo form_dropdown('date_joined', $date_joined); ?>
    </li>
</ul>
<?php
$sort_by = array(
                'Last visit'           => 'Last visit',
                'Distance from me'     => 'Distance from me'
                
);
?>
<label>Sort By:</label>
<ul>
    <li>
     <?php echo form_dropdown('sort_by', $sort_by); ?>
    </li>
</ul>

<?php echo form_submit(array('name'=>'search'),'Search'); ?>

<?php
echo form_close();
?>

</body>
</html>