<html>
<head>
<title>Upload Photo</title>
</head>
<body>

<h3>Your photo was successfully uploaded!</h3>

<ul>
<?php foreach ($upload_data as $item => $value):?>
<li><?php echo $item;?>: <?php echo $value;?></li>
<?php endforeach; ?>
</ul>

<p><?php echo anchor('members/my_photos', 'Upload Another Photo!'); ?></p>

</body>
</html>