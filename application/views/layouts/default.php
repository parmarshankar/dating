<html>
    <head>
	<title>Dating Site</title>
<!--	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold"/>-->
	<link href="<?= asset_url(); ?>css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?= asset_url(); ?>css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css"/>
	<link href="<?= asset_url(); ?>css/superfish.css" rel="stylesheet" type="text/css"/>

	<script type="text/javascript" src="<?= asset_url(); ?>js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?= asset_url(); ?>js/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="<?= asset_url(); ?>js/superfish.js"></script>
    </head>
    <body>
        <script>            
(function($)
{
    $(document).ready(function()
    {
        $.ajaxSetup(
        {
            cache: false,
            beforeSend: function() {
                $('#notification').hide();               
            },
            complete: function() {               
                $('#notification').show();
            },
            success: function() {                
                $('#notification').show();
            }
        });
        var $container = $("#notification");
        $container.load("<?=site_url('member/notification')?>");
        var refreshId = setInterval(function()
        {
            $container.load("<?=site_url('member/notification')?>");
        }, 9000);
    });
})(jQuery);
</script>
<script>
    function updateNotificationRead(id)
    {       
        jQuery.ajax({
			type: "POST",
			dataType:"json",
			url: "<?= site_url('member/notificationupdate') ?>",
			data: "id="+id
                        
		    });
    }
</script>

	<div id="wrapper">
	    <div id="header">
		<div class="logo">
		    <?php if ($this->session->userdata('logged'))
		    { ?>
    		    <div class="my_account">
                        <div class="welcome">
    			<label>Welcome! </label> <?= anchor('member', $this->session->userdata('username')); ?> &nbsp;[<?= anchor('user/logout', 'logout'); ?>]
                        </div> 
                        <div id="notification">                             
                         </div>                        
                    </div>
                   
                    <?php } ?>
		    <h1>Welcome to Dating site</h1>
		</div>
		<div class="menu">
		    <ul class="sf-menu">
			<li>
			    <a href="/">Home</a>
			</li>
                        <?php if ($this->session->userdata('logged')) :?>
                            <li>
				<?php echo anchor('message','Mail Box') ?> 
                                <ul>
				<li><a href="<?= site_url('message'); ?>">My Messages</a></li>                                
                                <li> <a href="<?= site_url('flirter'); ?>">My Flirts</a></li>
                                <li><a href="<?= site_url('event/alert'); ?>">My Event Alerts</a></li>
                                
                                </ul>
			    </li>
                            <?php endif; ?>
			<li>
			    <a href="<?= site_url('publicevent/all'); ?>">Events</a>
			    <ul>
				<li><a href="<?= site_url('publicevent/all'); ?>">Event Listings</a></li>
                                 <?php if ($this->session->userdata('logged')):?>
                                <li> <a href="<?= site_url('event/myevents'); ?>">My Events</a></li>
                                <li><a href="<?= site_url('event/add'); ?>">Add Event</a></li>
                                <?php endif;?>
			    </ul>
			</li>
			<li>
			    <a href="<?= site_url('search'); ?>">Search</a>			    
			</li>
		    </ul>
		    <div class="login_panel">
			<ul class="sf-menu">
                            <?php if ($this->session->userdata('logged')) :?>
                            <li>
				<?php echo anchor('member','My Account') ?>
			    </li>
                            <?php endif; ?>
                            <?php if ($this->session->userdata('logged') == false) :?>
			    <li>
				<?php echo anchor('user/login','Sign In') ?>
			    </li>
			    <li>
				<?php echo anchor('user/register','Join') ?>
			    </li>
                            <?php endif; ?>
			</ul>
		    </div>
		</div>
	    </div>
	    <div id="middle">
		<?= isset($template['partials']['member_header']) ? $template['partials']['member_header'] : ''; ?>
		<?= $template['body']; ?>
	    </div>
	    <div class="push"></div>
            
	</div>
        
	<div id="footer">
            <div id="home-bottom"> 
                <ul class="bottom-menu">
                <li><a href="#">  Menu1.</a> </li>  
                <li><a href="#">  Menu2.</a> </li>
                <li><a href="#">  Menu3.</a> </li>
                <li><a href="#">  Menu4.</a> </li>
                <li><a href="#">  Menu5.</a> </li>
                <li><a href="#">  Menu6.</a> </li> 
                <li><a href="#">  Menu7.</a> </li>
                </ul>
                <span class="copywrite">
                &#xA9; 2011.  Dating.com.au .  All rights reserved
                </span>
            </div>
	    
	</div>

    </body>
    
</html>