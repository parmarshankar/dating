<html>
<head>
<title>Search</title>
<style type="text/css">

form li {
  list-style: none;
}






.thumbnails img {
height: 90px;
border: 1px solid #555;
padding: 1px;

}

.search_result {
 width:800px;
}

.thumbnails img:hover {
border: 1px solid #00ccff;
cursor:pointer;
}



</style>
<link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<?php echo anchor(base_url().'members/view_profile', 'View my profile'); ?>
<br/>
<?php echo anchor(base_url().'members/edit_profile', 'Edit my profile'); ?>
<br/>
<?php echo anchor(base_url().'members/my_photos', 'My photos'); ?>
<br/>
<?php echo anchor(base_url().'members/search', 'Search'); ?>
<br/>
<?php echo anchor(base_url().'user/logout', 'Logout'); ?>
<br/>

<h2>Search results</h2>
<?php

foreach ($search_results as $result):

?>
<div class="search_result">
    <table>
        <tr>
        <td>
        <div class="thumbnails">
        <?php
        if (!is_null($result['photo']))
        {
        ?>
        <img onclick="" src="<?php echo base_url().'uploads/'.$result['photo']; ?>" alt="Image Not Loaded"/>
        
        <?php
        }
        else echo "Image is not available";
            
        ?>
        </div>
        </td>
        <td>
        <a href="<?php echo base_url().'members/search_profile/'.$result['username']?>"> <?php echo $result['username']; ?> </a>
        
        <br/>
        <?php
        if ($result['sex'] == 1) echo "Man "; else echo "Woman "; echo $result['age']."yrs"
        ?>
        <br/>
        <?php
        echo $result['location'];
        ?>
        </td>
    </tr>
</table>
</div>


<?php
endforeach;
?>
</body>
</html>