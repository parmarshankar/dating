<link href="<?= asset_url(); ?>css/tabular.css" rel="stylesheet" type="text/css"/>
<script>
    jQuery.noConflict();
	jQuery(function() {
		jQuery( "#accordion1" ).accordion({ collapsible: true });                
	});
</script>

<?php
$datas = $this->data['searchpast'];
?>
<div class="main-module-head">
 Past Parties and Events
</div>
<div id="accordion1">
    <h3><a href="#"> Past Parties & events</a></h3>
<div>
        <div class="content">
        <ul>
         <?php foreach($datas as $key => $data):?>
                        <li><a href="<?=site_url('publicevent/past/'.$data['location'])?>"> <?=$key?>(<?=$data['count']?>)</a></li>
        <?php endforeach; ?>
         </ul>
         </div>   
</div>
</div>


