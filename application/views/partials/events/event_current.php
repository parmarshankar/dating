<link href="<?= asset_url(); ?>css/tabular.css" rel="stylesheet" type="text/css"/>
<?php
$datas = $this->data['search'];
if(isset($this->data['active_menu']))
    $active_menu = $this->data['active_menu'];
else
    $active_menu=0;
?>
<script>
    jQuery.noConflict();
	jQuery(function() {
		jQuery( "#accordion" ).accordion({ collapsible: true, active: <?=$active_menu?>, autoHeight: false});                
	});
</script>


<div class="main-module-head">
 Parties and Events
</div>
<div id="accordion">
    <?php foreach($datas as $key => $data):?>
    <h3><a href="#"> Parties & events in <?=$key?></a></h3>
    <div>
        <div class="content">
            <ul>
        <?php foreach($data as $dtkey => $dt):?>
                        <li><a href="<?=site_url('publicevent/location/'.$dt['location']."/".$dt['category'])?>"> <?=$dtkey?> (<?=$dt['count']?>)</a></li>
        <?php endforeach; ?>
             </ul>
         </div>
    </div>
    <?php endforeach; ?>   
</div>
<div id="add">
 <?php echo anchor('event/add', ' + Add an Event',array('class' => 'btn small')); ?>
</div>




