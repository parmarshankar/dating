<?php
$complatevalue = $this->data['profile_complete']['totalvalue'];
$nextlink = $this->data['profile_complete']['nextlink'] ;
$nexttext = $this->data['profile_complete']['nexttitle'] ; 

?>
<script>
    jQuery.noConflict();
	jQuery(function() {
            jQuery("#progressbar").progressbar({ 
              value:<?=$complatevalue?>}); 
              jQuery('#completeprecentage').html('Complete precentage <?=$complatevalue?>%'); 
              jQuery('#completeprecentage').show(); 
              jQuery('#nextlink').html('<?php echo anchor($nextlink, $nexttext);?>');
              <?php if($nextlink==''):?>
              jQuery('#nextlink').hide(); 
              <?php endif?>
              <?php if($nextlink!=''):?>
              jQuery('#nextlink').show(); 
              <?php endif?>
            });
              
</script>

<div class="profileComplete">
<div id="progressbar" width="300px"></div> 
<div id="completeprecentage"></div> 
<div id="nextlink"></div>
</div>




