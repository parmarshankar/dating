<div id="home-myaccount" class="my-account">                   
           <div class="main-module-content">
               <div id="home-img">
               <div class="thumbnails">
                   <?php if(isset($this->data['photos'][0]->photo)){?>
                   <img  src="<?php echo base_url() . 'uploads/' . $this->data['photos'][0]->photo; ?>" alt="No Image Loaded"/>
                   <?php }
                   else
                   {?>
                   <img src="assets/images/no_avatar.jpg">
                    <?php   
                   }?>
               </div>
               </div>
                <div id="Requests">
                <div class="Requests">
                    
                    <ul>
                     <li><a href="<?=site_url('message');?>"> ( <?=$this->data['unReadMsg']->count;?> )  New Message(s) </a> </li>  
                     <li><a href="<?=site_url('flirter');?>"> ( <?=$this->data['unReadFlirter']->count;?> )   New Flirt(s) </a></li>  
                     <li><a href="<?=site_url('event/alert');?>"> ( <?=$this->data['unReadEventAlert']?> )   New Event Alert(s) </a></li>  
                     </ul>
                </div>
                <div class="Requests">
                    
                    <ul>
                    <li><a href="<?=site_url('lists/friends');?>"> ( <?=$this->data['unReadFriend']->count;?> ) Friend Request(s) </a></li>  
                    </ul>
                </div>
             </div>
               
           </div>
           <?php if(count($this->data['friends'])> 0):?>  
           <div id="friends">             
            <h4>Friends</h4>
            <?php foreach($this->data['friends'] as $friend):?>
            <div class="thumbnails">
                <a href="<?php echo base_url().'profile?id='.$friend->id?>">
                <?php if($friend->photo != ''):?>
               <img  src="uploads/<?=$friend->photo?>" alt="" title="<?=$friend->firstname?>">
               <?php endif;?>
               <?php if($friend->photo == ''):?>
               <img src="assets/images/no_avatar.jpg" title="<?=$friend->firstname?>">
               <?php endif;?>
               </a>
           </div>
            <?php endforeach;?>
        </div>
        <?php endif;?>     
       <div class="main-module-foot"></div>
</div>  