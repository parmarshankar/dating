<link href="../../assets/css/tabular.css" rel="stylesheet" type="text/css"/>
<script src="../../assets/js/functions.js" type="text/javascript"></script> 
<div class="g12">
<?php echo anchor('member', 'my home',array('class' => 'btn small')); ?>

<?php echo anchor('member/settings', 'Edit Profile / Settings',array('class' => 'btn small')); ?>

<?php echo anchor('lists/friends', 'My Friends',array('class' => 'btn small')); ?>
    
<?php echo anchor('member/photos', 'My photos',array('class' => 'btn small')); ?>
    
<?php echo anchor('lists', 'My Lists',array('class' => 'btn small')); ?>
    
<?php echo anchor('profile/statistics', 'Profile Statistics',array('class' => 'btn small')); ?>

</div>
<div class="clear"></div>