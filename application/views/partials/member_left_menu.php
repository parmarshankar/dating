<link href="<?= asset_url(); ?>css/tabular.css" rel="stylesheet" type="text/css"/>
<script>
	$(function() {
		$( "#accordion" ).accordion({ collapsible: true, active: <?=$active_menu?>, autoHeight: false  });                
	});
</script>

<!--<div class="menu">
    <ul class="sf-menu sf-vertical">
        <li>
            <a href="<?=site_url('message')?>"> My Messages</a>
                <ul>        
                <li><a href="<?=site_url('message/show')?>"> Inbox</a></li>  
                <li><a href="<?=site_url('message/show/sent')?>"> Sent</a></li>
                <li><a href="<?=site_url('message/show/trash')?>"> Trash</a></li>
                </ul>
        </li>
        <li>
            <a href="<?=site_url('message')?>"> My Messages</a>
                <ul>        
                <li><a href="<?=site_url('flirter/show')?>"> Inbox</a></li>  
                <li><a href="<?=site_url('flirter/show/sent')?>"> Sent</a></li>
                <li><a href="<?=site_url('flirter/show/trash')?>"> Trash</a></li>
                </ul>
        </li>
        <li>
           <a href="<?=site_url('event/alert')?>" id="Alerts"> My Event Alerts</a>                
        </li>
                        
    </ul>
</div>-->

<div id="accordion">    
    <h3><a href="<?=site_url('message')?>"> My Messages</a></h3>
    <div>
        <div class="content-menu">
            <ul>        
                <li><a href="<?=site_url('message/show')?>"> Inbox</a></li>  
                <li><a href="<?=site_url('message/show/sent')?>"> Sent</a></li>
                <li><a href="<?=site_url('message/show/trash')?>"> Trash</a></li>
             </ul>
         </div>
    </div>  
    
    <h3><a href="<?=site_url('flirter')?>"> My Flirts</a></h3>
    <div>
        <div class="content-menu">
            <ul>        
                <li><a href="<?=site_url('flirter/show')?>"> Inbox</a></li>  
                <li><a href="<?=site_url('flirter/show/sent')?>"> Sent</a></li>
                <li><a href="<?=site_url('flirter/show/trash')?>"> Trash</a></li>
             </ul>
         </div>
    </div> 
    <h3><a href="<?=site_url('event/alert')?>" id="Alerts"> My Event Alerts</a></h3>
    <div>
    <div class="content-menu">
    <ul>        
                <li><a href="<?=site_url('event/alert')?>"> Event Alerts</a></li>  
                
             </ul>
         </div>
   </div>  
    
</div>


