<html>
<head>
<title>user registration</title>
<style type="text/css">

form li {
  list-style: none;
}


</style>
</head>
<body>
<h1>User login</h1>
<p>Please fill in details  below</p>
<?php
    echo form_open(base_url().'user/login');

    $username = array(
          'name'   => 'username',
          'id'     => 'username',
          'value'  => set_value('username'),
    );


    $password = array(
          'name'   => 'password',
          'id'     => 'password',
          'value'  => '',
    );



?>
<ul>
    <li>
    <label>Username</label>
    <div>
    <?php echo form_input($username); ?>
    </div>
    </li>


    <li>
    <label>Password</label>
    <div>
    <?php echo form_password($password); ?>
    </div>
    </li>



    <li>
    <?php
    echo validation_errors();
    ?>
    </li>

    <li>
    <?php
    echo $this->view_data['user_pass_invalid'];
    ?>
    </li>

    <li>
    <div>
    <?php echo form_submit(array('name'=>'login'),'Log in'); ?>
    </div>
    </li>

</ul>
<?php
echo form_close();
?>
</body>
</html>