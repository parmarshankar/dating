<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Edit my profile</title>
<style type="text/css">

form li {
  list-style: none;
}

#edit_profile {
    width: 500px;
}

</style>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.multiSelect.js"></script>
<link href="<?php echo base_url(); ?>css/jquery.multiSelect.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

			$(document).ready( function() {

				
				$("#sexual_interests, #general_interests, #sexual_preferences, #fetish_interests").multiSelect({
                                  selectAll: false
                                });


				

			});

		</script>

</head>
<body>
<h1>Edit my profile</h1>


<?php echo anchor(base_url().'members/members_area', 'Home'); ?>
<br/>
<?php echo anchor(base_url().'user/logout', 'Logout'); ?>
<br/>
<div id="edit_profile">
<h3>My personal info</h3>
<hr>

<?php
    echo form_open(base_url().'members/edit_profile');

    $col_width = 'style="width: 200px; position: absolute; left:200px;"';

    //relationship status
    $rel_status = array(
                  'Ask Me'    => 'Ask Me',
                  'Single'   => 'Single',
                  'Married'   => 'Married',
                  'Divorced'  => 'Divorced',
                  'Separated' => 'Separated',
                  'Attached'  => 'Attached'
                
                );

    //participation level
    $part_level = array(
                  'Open for anything'    => 'Open for anything',
                  'Meet first,play later'   => 'Meet first,play later',
                  'Play meets only'   => 'Play meets only',
                  'Social meets only'  => 'Social meets only',
                  'Taking things slow' => 'Taking things slow'
                );

    //sexual orientation
    $sex_orienatation = array(
                  'Not Sure'    => 'Not sure',
                  'I just like to watch'   => 'I just like to watch',
                  'Straight'   => 'Straight',
                  'Experimental'  => 'Experimental',
                  'Bi-Curios' => 'Bi-Curios',
                  'Bisexual' => 'Bisexual',
                  'Gay' => 'Gay',
                  'Anything' => 'Anything',
                );

    //race
    $race = array(
                  'Ask Me'    => 'Ask Me',
                  'American Indian'   => 'American Indian',
                  'Black'   => 'Black',
                  'Caucasian'  => 'Caucasian',
                  'East Indian' => 'East Indian',
                  'Hispanic' => 'Hispanic',
                  'Middle Eastern' => 'Middle Eastern',
                  'Various' => 'Various',
                  'Other' => 'Other'
                );

    //personality
    $personality_ar = array(
                  'Ask Me'                  => 'Ask Me',
                  'Friendly'                => 'Friendly',
                  'Shy'                     => 'Shy',
                  'Shy at first but'        => 'Shy at first but',
                  'Private'                 => 'Private',
                  'Reserved'                => 'Reserved',
                  'Flirtatious'             => 'Flirtatious',
                  'Confident'               => 'Confident',
                  'Outgoing'                => 'Outgoing',
                  'The life of the party'   => 'The life of the party'
                );

      //my looks
    $my_looks_ar = array(
                  'Do Not Display'                  => 'Do Not Display',
                  'Ask Me'                          => 'Ask Me',
                  'See My Photo'                    => 'See My Photo',
                  'Very Attractive'                 => 'Very Attractive',
                  'Attractive'                      => 'Attractive',
                  'Average'                         => 'Average'
                );

      //body type
    $body_type_ar = array(
                  'Ask Me'                          => 'Ask Me',
                  'Thin'                            => 'Thin',
                  'Slim'                            => 'Slim',
                  'Athletic'                        => 'Athletic',
                  'Average'                         => 'Average',
                  'Ample'                           => 'Ample',
                  'A little extra padding'          => 'A little extra padding',
                  'Large'                           => 'Large'


                );

    $height_ar = array(
                 'Ask Me'=> 'Ask Me',
                 '<5 ft 0 in'=> '<5 ft 0 in',
         '5 ft 0 in'=> '5 ft 0 in',
         '5 ft 1 in'=> '5 ft 1 in',
         '5 ft 2 in'=> '5 ft 2 in',
         '5 ft 3 in'=> '5 ft 3 in',
         '5 ft 4 in'=> '5 ft 4 in',
         '5 ft 5 in'=> '5 ft 5 in',
         '5 ft 6 in'=> '5 ft 6 in',
         '5 ft 7 in'=> '5 ft 7 in',
         '5 ft 8 in'=> '5 ft 8 in',
         '5 ft 9 in'=> '5 ft 9 in',
         '5 ft 10 in'=> '5 ft 10 in',
         '5 ft 11 in'=> '5 ft 11 in',
         '6 ft 0 in'=> '6 ft 0 in',
         '6 ft 1 in'=> '6 ft 1 in',
         '6 ft 2 in'=> '6 ft 2 in',
         '6 ft 3 in'=> '6 ft 3 in',
         '6 ft 3 in'=> '6 ft 3 in',
         '6 ft 4 in'=> '6 ft 4 in',
         '>6 ft 4 in'=> '>6 ft 4 in'


    );

    //eye colour
    $eye_colour_ar = array(
                  'Ask Me'                            => 'Ask Me',
                  'Blue'                              => 'Blue',
                  'Green'                             => 'Green',
                  'Brown'                             => 'Brown',
                  'Grey'                              => 'Grey',
                  'Hazel'                             => 'Hazel',
                  'Black'                             => 'Black'
                );

    //hair colour
            $hair_colour_ar = array(
                'Ask Me' => 'Ask Me',
                'Dark Brown' => 'Dark Brown',
                'Light Brown' => 'Light Brown',
                'Black' => 'Black',
                'Red/Strawberry Blonde' => 'Red/Strawberry Blonde',
                'Grey/Silver' => 'Grey/Silver',
                'Other' => 'Other',
                'Other' => 'Other',
                'Changes often' => 'Changes often',
                'Shaved/Bald' => 'Shaved/Bald',
                );

    //hair length
            $hair_length_ar = array(
                'Ask Me' => 'Ask Me',
                'Bald' => 'Dark Brown',
                'Clean shaven' => 'Light Brown',
                'Short/Shaven' => 'Short/Shaven',
                'Short' => 'Short',
                'Medium(Ear length)' => 'Medium(Ear length)',
                'Shoulder length' => 'Shoulder length',
                'Past shoulders' => 'Past shoulders',
                'Other/Irregular' => 'Other/Irregular'
                
                );

     //Drinking
             $drinking_ar = array(
                'Ask Me' => 'Ask Me',
                'I dont drink at all' => 'I dont drink at all',
                'I am a light/social drinker' => 'I am a light/social drinker',
                'I am regular drinker' => 'I am regular drinker'
                

                );
     //Smoking
              $smoking_ar = array(
                'Ask Me' => 'Ask Me',
                'I am non-smoker' => 'I am non-smoker',
                'I am a light smoker' => 'I am a light smokern',
                'I am a regular smoker' => 'I am a regular smoker',
                'I am a cigar/pipe smoker' => 'I am a cigar/pipe smoker'
                

                );
     //General interests

              $general_interests_ar = array(
                'Friends'  => 'Friends',
                'Travel'  => 'Travel',
                  'Music'  => 'Music',
                  'Shopping'  => 'Shopping',
                  'Dancing/Clubbing'  => 'Dancing/Clubbing',
                  'Entertaining/Parties'  => 'Entertaining/Parties',
                  'Wining and Dining'  => 'Wining and Dining',
                  'Movies'  => 'Movies',
                  'Fitness'  => 'Fitness',
                  'Quiet Times'  => 'Quiet Times',
                  'Outdoor Activities'  => 'Outdoor Activities',
                  'Work'  => 'Work',
                  'Cars'  => 'Cars',
                  'Motorcycles'  => 'Motorcycles',
                  'Boats'  => 'Boats',

              );
 ?>


    
    <strong>Relationship status:</strong>
   <?php echo form_dropdown('rel_status', $rel_status, $relationship_status,$col_width); ?>
    <br/>
    <strong>Participation level:</strong>
   <?php echo form_dropdown('part_level', $part_level, $participation_level, $col_width); ?>
    </br>
    <strong>Sexual orientation:</strong>
   <?php echo form_dropdown('sex_orienatation', $sex_orienatation, $sexual_orientation, $col_width); ?>
    </br>
    <strong>Race/Ethnic Background:</strong>
   <?php echo form_dropdown('race', $race, $race_background, $col_width); ?>
    </br>
    <strong>Personality:</strong>
    <?php echo form_dropdown('personality', $personality_ar, $personality, $col_width); ?>
    </br>
    <strong>My looks:</strong>
    <?php echo form_dropdown('my_looks', $my_looks_ar, $my_looks, $col_width); ?>
     </br>
    <strong>Body type:</strong>
    <?php echo form_dropdown('body_type', $body_type_ar,$body_type, $col_width); ?>
     </br>
    <strong>Height:</strong>
    <?php echo form_dropdown('height', $height_ar, $height,$col_width); ?>
    </br>
    <strong>Eye colour:</strong>
    <?php echo form_dropdown('eye_colour', $eye_colour_ar, $eye_colour, $col_width); ?>
    </br>
    <strong>Hair colour:</strong>
    <?php echo form_dropdown('hair_colour', $hair_colour_ar, $hair_colour, $col_width); ?>
    </br>
    <strong>Hair length:</strong>
    <?php echo form_dropdown('hair_length', $hair_length_ar, $hair_length, $col_width); ?>
    </br>
    <strong>Drinking:</strong>
    <?php echo form_dropdown('drinking', $drinking_ar, $drinking, $col_width); ?>
    </br>
    <strong>Smoking:</strong>
    <?php echo form_dropdown('smoking', $smoking_ar,$smoking, $col_width); ?>
     </br>
    <strong>General interests:</strong>
   <?php echo form_multiselect('general_interests[]', $general_interests_ar,  unserialize($general_interests), 'id="general_interests"'); ?>
     </br>

<h3>My Sexual Info(optional)</h3>
<hr>
<?php

//Safe sex
$safe_sex_ar = array(
                'Always for all activities' => 'Always for all activities',
                'Always for all intercourse' => 'Always for all intercourse',
                'Sometimes' => 'Sometimes',
                'If required' => 'If required',
                'Never' => 'Never'


                );

//Body Hair
$body_hair_ar = array(
                'Do not display' => 'Do not display',
                'None' => 'None',
                'Light' => 'Light',
                'Hairly' => 'Hairly',
                'Manicured' => 'Manicured',
                'shaved/Waxed' => 'shaved/Waxed'


                );
//Pubic Hair
$public_hair_ar = array(
                'Do not display' => 'Do not display',
                'Light' => 'Light',
                'Hairly' => 'Hairly',
                'Manicured' => 'Manicured',
                'shaved/Waxed' => 'shaved/Waxed'


                );

//Endowment Length:
$endowment_length_ar = array(
                'Do not display' => 'Do not display',
                'Under 3"' => 'Under 3"',
                '3"-4"' => '3"-4"',
                '4"-5"' => '4"-5"',
                '5"-6"' => '5"-6"',
                '6"-7"' => '6"-7"',
                '7"-8"' => '7"-8"',
                '8"-9"' => '8"-9"',
                'Over 9"' => 'Over 9"'
                );

//Endowment thickness:

$endowment_thickness_ar = array(
                'Do not display' => 'Do not display',
                'Thin' => 'Thin',
                'Average' => 'Average',
                'Thick' => 'Thick',
                'Very Thick' => 'Very Thick'


                );

//Intimate Piercings
$intimate_piercings_ar = array(
                'Do not display' => 'Do not display',
                'Yes' => 'Yes',
                'No' => 'No'
                );

//Sexual Personality:
$sexual_personality_ar = array(
                'Naturally sexy' => 'Naturally sexy',
                'Not naturally sexy but' => 'Not naturally sexy but',
                'Conservative' => 'Conservative',
                'Gentle Lover' => 'Gentle Lover',
                'Considerate Lover' => 'Considerate Lover',
                'Passionate' => 'Passionate',
                'Sexual Deviate' => 'Sexual Deviate',
                'Sex Maniac' => 'Sex Maniac',
                'Dominant' => 'Dominant',
                'Submissive' => 'Submissive',
                'Do it hard, do it now' => 'Do it hard, do it now'
                



                );

//Sexual interests:
$sexual_interests_ar = array(
                'Sexy Chat/Email/Photo Swap' => 'Sexy Chat/Email/Photo Swap',
                'Phone/Webcam Sex' => 'Phone/Webcam Sex',
                'Foreplay Only' => 'Foreplay Only',
                '1 on 1 sex' => '1 on 1 sex',
                'Group sex' => 'Group sex',
                'MMF' => 'MMF',
                'FFM' => 'FFM',
                'MMF (bi)' => 'MMF (bi)',
                'FFM (bi)' => 'FFM (bi)'
                );
//Sexual preferences:
$sexual_preferences_ar = array(
                'Kissing' => 'Kissing',
                'Regular sex' => 'Regular sex',
                'Oral sex - giving' => 'Oral sex - giving',
                'Oral sex - receiving' => 'Oral sex - receiving',
                'Anal sex/play - giving' => 'Anal sex/play - giving',
                'Anal sex/play - receiving' => 'Anal sex/play - receiving',
                'Sex outdoors - secluded' => 'Sex outdoors - secluded',
                'Dogging (public sex)' => 'Dogging (public sex)',
                'Naturism/Nudity' => 'Naturism/Nudity',
                'Erotic Photography' => 'Erotic Photography',
                'Masturbation' => 'Masturbation'
               


                );

//Fetish interests:
$fetish_interests_ar = array(
                'Bondage & Discipline - soft' => 'Bondage & Discipline - soft',
                'Bondage & Discipline - hard' => 'Bondage & Discipline - hard',
                'Role Play' => 'Role Play',
                'Gang Bang' => 'Gang Bang',
                'Exhibitionism & Voyeurism' => 'Exhibitionism & Voyeurism',
                'Feet' => 'Feet',
                'Toys (Vibrator, strap-on etc.)' => 'Toys (Vibrator, strap-on etc.)',
                'Hair pulling' => 'Hair pulling',
                'Spanking/Whipping' => 'Spanking/Whipping',
                'Breast/Nipple Play' => 'Breast/Nipple Play',
                'Genital Play' => 'Genital Play',
                'Breath Play' => 'Breath Play',
                'Candle wax' => 'Candle wax',
                'Water sports' => 'Water sports',
                'Fisting' => 'Fisting',
                'Rimming' => 'Rimming',
                'Bukkake' => 'Bukkake',
                'CD/TV/TG/TS' => 'CD/TV/TG/TS'
              


                );


?>

<br/>
<strong>Safe sex:</strong>
<?php echo form_dropdown('safe_sex', $safe_sex_ar, $safe_sex, $col_width); ?>
<br/>
<strong>Body Hair:</strong>
<?php echo form_dropdown('body_hair', $body_hair_ar, $body_hair, $col_width); ?>
<br/>
<strong>Pubic Hair:</strong>
<?php echo form_dropdown('public_hair', $public_hair_ar, $public_hair, $col_width); ?>
<br/>
<strong>Endowment Length:</strong>
<?php echo form_dropdown('endowment_length', $endowment_length_ar, $endowment_length, $col_width); ?>
<br/>
<strong>Endowment Thickness:</strong>
<?php echo form_dropdown('endowment_thickness', $endowment_thickness_ar,$endowment_thickness, $col_width); ?>
<br/>
<strong>Intimate Piercings:</strong>
<?php echo form_dropdown('intimate_piercings', $intimate_piercings_ar,$intimate_piercings, $col_width); ?>
<br/>
<strong>Sexual Personality:</strong>
<?php echo form_dropdown('sexual_personality', $sexual_personality_ar,$sexual_personality, $col_width); ?>
<br/>
<strong>Sexual interests:</strong>

<?php echo form_multiselect('sexual_interests[]', $sexual_interests_ar,  unserialize($sexual_interests), 'id="sexual_interests"'); ?>
<br/>
<strong>Sexual preferences:</strong>
<?php echo form_multiselect('sexual_preferences[]', $sexual_preferences_ar,unserialize($sexual_preferences), 'id="sexual_preferences"'); ?>
<br/>
<strong>Fetish interests:</strong>
<?php echo form_multiselect('fetish_interests[]', $fetish_interests_ar,  unserialize($fetish_interests), 'id="fetish_interests"'); ?>
<br/>
<h3>My RedHot Fantasy</h3>
<?php
$ideal_sexy_location_ar = array(
 'in a private bedroom with silk sheets on the bed'=>'in a private bedroom with silk sheets on the bed',
 'on a secluded beach of a lush tropical island'   =>'on a secluded beach of a lush tropical island',
 'in a warm, inviting spa of a luxury penthouse'   =>'in a warm, inviting spa of a luxury penthouse',
 'at a romantic bed &amp; breakfast in the country'   =>'at a romantic bed &amp; breakfast in the country',
 'in a secret, underground dungeon'   =>'in a secret, underground dungeon',
 'at a quiet part of a park on a warm sunny day'   =>'at a quiet part of a park on a warm sunny day',
 'in a dark corner of a hyped-up nightclub'   =>'in a dark corner of a hyped-up nightclub',
 'at a mysterious, erotic masquerade party'   =>'at a mysterious, erotic masquerade party'
);

$must_have_ar = array(
 'a lustful, imaginative lover'=>'a lustful, imaginative lover',
 'whipped cream and chocolate body paint'   =>'whipped cream and chocolate body paint',
 'champagne and scented massage oils'   =>'champagne and scented massage oils',
 'fluffy handcuffs and other naughty toys'   =>'fluffy handcuffs and other naughty toys',
 'whips, chains and a leather collar'   =>'whips, chains and a leather collar',
 'a blindfold and ice cubes'   =>'a blindfold and ice cubes',
 'candles and sensual background music'   =>'candles and sensual background music',
 'someone watching from afar'   =>'someone watching from afar'
);
        
$favourite_activity_ar = array(
 'hours of hot, unbridled sex'=>'hours of hot, unbridled sex',
 'a night of sensual, passionate love-making'   =>'a night of sensual, passionate love-making',
 'a session of lingering kisses and touches'   =>'a session of lingering kisses and touches',
 'naughty, porno sex while being filmed'   =>'naughty, porno sex while being filmed',
 'finding the fine line between pleasure and pain'   =>'finding the fine line between pleasure and pain',
 'a slow, sexy dance that works as foreplay'   =>'a slow, sexy dance that works as foreplay',
 'a hard and fast encounter that leaves you breathless'   =>'a hard and fast encounter that leaves you breathless',
 'a quiet moment to get to know my partne'   =>'a quiet moment to get to know my partne'
)

?>
<hr>
<strong>My ideal sexy location:</strong>
<?php echo form_dropdown('ideal_sexy_location', $ideal_sexy_location_ar,$ideal_sexy_location, $col_width); ?>
<br/>
<strong> My sexy must-haves::</strong>
<?php echo form_dropdown('must_have', $must_have_ar,$sexy_must_haves, $col_width); ?>
<br/>
<strong>My favourite activity:</strong>
<?php echo form_dropdown('favourite_activity', $favourite_activity_ar,$favourite_activity, $col_width); ?>
<br/>



<h3>In my own words</h3>
<hr>
<strong>Profile title:</strong>
<?php echo form_input('profile_title', $profile_title, $col_width); ?>
<br/>

<strong>About me:</strong>
<?php echo form_textarea('about_me',$about_me); ?>
<br/>
<strong>I am looking to meet:</strong>
<?php echo form_textarea('looking_to_meet',$looking_to_meet); ?>
<br/>

<?php echo form_submit(array('name'=>'edit'),'Submit'); ?>
<?php
echo form_close();
?>
</div>
</body>
</html>