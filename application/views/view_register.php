<html>
<head>
<title>User registration</title>
<style type="text/css">
    
form li {
  list-style: none;        
}


</style>
</head>
<body>
<h1>User registration</h1>
<p>Please fill in details  below</p>
<?php
    echo form_open(base_url().'user/register');

    $username = array(
          'name'   => 'username',
          'id'     => 'username',
          'value'  => set_value('username'),
    );

    $firstname = array(
          'name'   => 'firstname',
          'id'     => 'firstname',
          'value'  => set_value('firstname'),
    );

    $lastname = array(
          'name'   => 'lastname',
          'id'     => 'lastname',
          'value'  => set_value('lastname'),
    );

    $email = array(
          'name'   => 'email',
          'id'     => 'email',
          'value'  => set_value('email'),
    );
    
    $password = array(
          'name'   => 'password',
          'id'     => 'password',
          'value'  => '',
    );
    
    $password_confirm = array(
          'name'   => 'password_confirm',
          'id'     => 'password_confirm',
          'value'  => '',
    );

  
    $location_options = array(
                  'Australian Capital Territory'  => '- Australian Capital Territory',
                  'New South Wales'               => '- New South Wales',
                  'Northern Territory'            => '- Northern Territory',
                  'Queensland'                    => '- Queensland',
                  'South Australia'               => '- South Australia',
                  'Tasmania'                      => '- Tasmania',
                  'Victoria'                      => '- Victoria',
                  'Western Australia'             => '- Western Australia'
                );
    
    $sex_options = array(
                  'm'  => 'Male',
                  'f'  => 'Female',
                );

    

    $age = array(
          'name'   => 'age',
          'id'     => '',
          'value'  => set_value('age'),
    );

?>
<ul>
    <li>
    <label>Username</label>
    <div>
    <?php echo form_input($username); ?>
    </div>
    </li>

    <li>
    <label>First Name</label>
    <div>
    <?php echo form_input($firstname); ?>
    </div>
    </li>

    <li>
    <label>Last Name</label>
    <div>
    <?php echo form_input($lastname); ?>
    </div>
    </li>

    <li>
    <label>E-mail</label>
    <div>
    <?php echo form_input($email); ?>
    </div>
    </li>
    
    <li>
    <label>Location</label>
    <div>
    <?php echo form_dropdown('location', $location_options); ?>
    </div>
    </li>
    
    <li>
    <label>Sex</label>
    <div>
    <?php echo form_dropdown('sex', $sex_options); ?>
    </div>
    </li>

    <li>
    <label>Age</label>
    <div>
    <?php echo form_input($age); ?>
    </div>
    </li>



    <li>
    <label>Password</label>
    <div>
    <?php echo form_password($password); ?>
    </div>
    </li>

    <li>
    <label>Password confirm</label>
    <div>
    <?php echo form_password($password_confirm); ?>
    </div>
    </li>


    <li>
    <?php
    echo validation_errors();
    ?>
    </li>

    <li>
    <div>
    <?php echo form_submit(array('name'=>'register'),'Register'); ?>    
    </div>
    </li>

</ul>
<?php
echo form_close();
?>
</body>
</html>