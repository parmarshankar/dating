<h2>Profile Statistics</h2>


<div id="edit_profile">
    <div class="message">
        <?php if(isset($message['success'])): ?>
            <div class="success">
                <?php echo $message['success'];?>
            </div>
        <?php endif; ?>
        <?php if(isset($message['warn'])): ?>
            <div class="warn">
                <?php echo $message['warn'];?>
            </div>
        <?php endif; ?>
        <?php if(isset($message['error'])): ?>
            <div class="error">
                <?php echo $message['error'];?>
            </div>
        <?php endif; ?>
    </div>
     <div class="ui-widget-content">
          <div class="ui-state-default hedding">General Stats</div> 
          <table>
              <tr>
                  <td width="200px">I have logged in</td>
                  <td width="170px"><?=$logged_count?> times</td>
                  <td width="100px"></td>
              </tr>
              <tr>
                  <td>My profile has been viewed</td>
                  <td><?=$view_count_last?> times since last login</td>
                  <td><a href="../lists/profileViewList">View List</a></td>
              </tr>
               <tr>
                  <td></td>
                  <td><?=$view_count_this?>  times in the last month</td>
                  <td><a href="../lists/profileViewList">View List</a></td>
              </tr>
              <tr>
                  <td></td>
                  <td><?=$view_count_all?>  times for life time</td>
                  <td><a href="../lists/profileViewList">View List</a></td>
              </tr>
              <tr>
                  <td>This month I have viewed</td>
                  <td><?=$i_view_count?> profiles</td>
                  <td><a href="../lists/profileViewList?url=2">View List</a></td>
              </tr>
              <tr>
                  <td>I have</td>
                  <td><a href="../lists/friends?url=2"><?=count($friends)?> friends</a> in total</td>
              </tr>
              <tr>
                  <td>I have posted</td>
                  <td><?=$event_post_count?> events</td>
                  <td><a href="../event/myevents">View</a></td>
              </tr>
              <tr>
                  <td>I am interested in</td>
                   <td><?=$event_interest_count?> events</td>
                  <td><a href="../event/myevents">View</a></td>
              </tr>
          </table>
     </div>
    <div class="ui-widget-content">
          <div class="ui-state-default hedding">Mailbox Stats</div> 
          <table>
              <tr>
                  <td width="200px">I have sent</td>
                  <td width="100px"><a href="../message?view=sent"><?=$messages['from']?> messages</a> </td>
                  <td width="140px">Reply Rate: 
                      <?php if ($messages['from'] > 0){?>
                        <?=round(($messages['reply']/$messages['from'] * 100),2)?> %
                        <?php }else {echo '0%';}?>   
                  </td>
              </tr>
              <tr>
                  <td>I have received</td>
                  <td><a href="../message"><?=$messages['to']?> messages</a></td>
                  <td></td>
              </tr>
               <tr>
                  <td >I have sent</td>
                  <td><a href="../flirter?view=sent"><?=$flirter['to']?> flirts</a> </td>
                  <td>Reply Rate: 
                             <?php if ($flirter['from'] > 0) {?>   
                              <?=round(($flirter['reply']/$flirter['from'] * 100),2)?> %</td>
                              <?php }else {echo '0%';}?>   
              </tr>
              <tr>
                  <td>I have received</td>
                  <td><a href="../flirter"><?=$flirter['to']?> flirts</a></td>
                  <td></td>
              </tr>
          </table>
     </div>
    <div class="ui-widget-content">
          <div class="ui-state-default hedding">Lists Stats</div> 
          <table>
              <tr>
                  <td width="200px">My profile is on</td>
                  <td width="170px"><?=$hot_count?> Hot Lists </td>
                  <td width="100px"> <a href="../lists/hotList?url=2">View List</a></td>
              </tr>
              <tr>
                  <td></td>
                  <td><?=$short_count?>Short Lists</td>
                  <td><a href="../lists/shortList?url=2">View List</a></td>
              </tr>
               
          </table>
     </div>
</div>

