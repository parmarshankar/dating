

<script type="text/javascript">
    <?php if(isset($_GET['url']) && $_GET['url']=='2'):?>
        window.onload=function(){tabSwitch_2(2, 2, 'tabm_', 'contentm_');}
    <?php endif;?>
</script>
<div class="breadcrumb">
<breadcrumb><a  href="<?= site_url('lists'); ?>">My Lists</a> >> Short List</breadcrumb>
</div>

<h2>Short List</h2>

<div class="my_lists">
 <div id="tablule-l1" class="">
    <div class="tabbed_area">
        <ul class="tabs">
            <li><a href="javascript:tabSwitch_2(1, 2, 'tabm_', 'contentm_');" id="tabm_1" class="active">My Short List</a></li>
            <li><a href="javascript:tabSwitch_2(2, 2, 'tabm_', 'contentm_');" id="tabm_2">Short List's I'm In</a></li>

        </ul>

        <div id="contentm_1" class="content">
             <ul>  
                 <li>
                    <div id="frends-add">
                    <button id="add-short">Add New</button>  
                    <button id="delete-short">Delete Selected</button> 
                    </div>
                </li>
                   <?php 
                   $user =0;
                   foreach($my_short_lists as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="hot_user" value="<?=$pf->username?>" />
                                        </td>
                                        <td>
                                            <div class="thumbnails">
                                                <a href="<?php echo base_url().'profile?id='.$pf->id?>">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                                </a>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                        <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                    <td  width="300px">
                                        <?php 
                                        if(!isset($friend[$pf->id]))                                        
                                        echo anchor('lists/sendRequest?to='.$pf->id, ' Add As Friend',array('class' => 'btn small')); 
                                        ?>                                        
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
             </ul>                     
        </div>
        <div id="contentm_2" class="content">

             <ul>
                <?php 
                   $user =0;
                   foreach($i_in_short_lists as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="thumbnails">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                        <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>               
            </ul> 
        </div>                  

    </div>
</div>
    
    
</div>

<div id="dialog-form" title="Add selected user to short list" style="display: none;">
    <form>
    <fieldset>
        <label for="title">Username</label>
        <br>
        <input type="text" name="uname" id="uname" class="text ui-widget-content ui-corner-all" />                   
    </fieldset>
    </form>
</div>

<div id="dialog-success" title="Confirm !" style="display: none;">
    <p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
	<div id="success-title"></div>
    </p>
</div>

<div id="dialog-form-delete" title="Do you want to delete" style="display: none;">
    Do you want to delete <br/>
    <span id="results"></span>
    <br/>
    from your short list
</div>

<script type="text/javascript">

    $(function(){
        
        $( "#add-short" )
        .button()
        .click(function() {
            $( "#dialog-form" ).dialog( "open" );
        });
        
        $( "#delete-short" )
        .button()
        .click(function() {            
            $( "#dialog-form-delete" ).dialog( "open" );
        });
        
        
        $( "#dialog-form-delete" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Yes Delete": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/deleteFrom') ?>",
                        data: "results="+document.getElementById('results').innerHTML  +"&code=SHORT",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Add": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/sendKnownRequest') ?>",
                        data: "uname="+$('#uname').val()+"&code=SHORT",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-success" ).dialog({
	    autoOpen: false,
	    resizable: false,
	    height:180,
	    buttons: {
		Ok: function() {
                     window.location='<?= site_url('lists/shortList'); ?>';
		    $( this ).dialog( "close" );
		}
	    },

	    close: function() {
		 window.location='<?= site_url('lists/shortList'); ?>';
	    }
	});
        
        
        
       });

</script>

<script>

    function showValues() {
      var fields = $(":input").serializeArray();
      $("#results").empty();
      jQuery.each(fields, function(i, field){
        $("#results").append(field.value + " : ");
      });
    }

    $(":checkbox").click(showValues);    
    showValues();
</script>
