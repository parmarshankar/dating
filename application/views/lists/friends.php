<script type="text/javascript">
    <?php if(isset($_GET['url']) && $_GET['url']=='2'):?>
        window.onload=function(){tabSwitch_2(2, 2, 'tabm_', 'contentm_');}
    <?php endif;?>
</script>
<div class="breadcrumb">
<breadcrumb>My Friends</breadcrumb>
</div>
<h2>My Friends</h2>

<div class="my_lists">
    
    
<div class="message">
    <?php if(isset($message['success'])): ?>
        <div class="success">
            <?php echo $message['success'];?>
        </div>
    <?php endif; ?>
 </div>
    
 <div id="tablule-l1" class="">
    <div class="tabbed_area">
        <ul class="tabs">
            <li><a href="javascript:tabSwitch_2(1, 2, 'tabm_', 'contentm_');" id="tabm_1" class="active">Pending Friend Requests</a></li>
            <li><a href="javascript:tabSwitch_2(2, 2, 'tabm_', 'contentm_');" id="tabm_2">Current Friends</a></li>

        </ul>

        <div id="contentm_1" class="content">
             <ul>                    
                   <?php 
                   $user =0;
                   foreach($pending_friend as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="thumbnails">
                                                <a href="<?php echo base_url().'profile?id='.$pf->id?>"> 
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                                </a>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>                                        
                                        <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                    <td  width="300px">
                                        <?php echo anchor('lists/acceptFriends?userid='.$pf->id, ' Accept',array('class' => 'btn small')); ?>
                                        <?php echo anchor('lists/ignoreFriends?userid='.$pf->id, ' Ignore',array('class' => 'btn small')); ?>
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
                            
                            
                            
                   <?php 
                   $user =0;
                   foreach($pending_friend_byme as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="thumbnails">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                       <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                    <td  width="300px">                                       
                                        Send by me<br/>
                                        At <?=$pf->date?>
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
             </ul>                     
        </div>
        <div id="contentm_2" class="content">
            
             <ul>
                <li>
                    <p>You decide which of your friends are displayed on your profile!
To select the top 6 friends to be displayed on your profile, please use the Rank box to order from 1 to 6.</p>
                </li>
                <li>
                    <div id="frends-add">
                    <button id="add-afriend">Add Friend</button>  
                    <button id="delete-friend">Delete Selected</button> 
                    </div>                    
                </li>
                <?php  echo form_open('lists/saveOrder');?>
                <li>
                    <table width="100%">
                        <tr>                           
                            <td style="text-align: right;width: 500px;">Order
                            <?php echo form_submit(array('name' => 'save','class' => 'button','style'=>'margin-left:10px;float:right'), 'Save'); ?>
                            </td>                            
                        </tr>
                    </table>                  
                </li>
                <?php 
                   $user =0;
                   foreach($friend as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table width="100%">
                                    <tr>
                                        <td width="20px">
                                            <input type="checkbox" name="friend_user" value="<?=$pf->username?>" />
                                        </td>
                                        <td>
                                            <div class="thumbnails">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                            </div>
                                        </td>
                                        <td  width="300px">
                                            <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                            <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                            <?=$pf->location; ?>
                                        </td>
                                         <td style="text-align: right">
                                             <?php
                                             $value ='';
                                             if(isset($friend_order[$pf->id]))
                                                     $value = $friend_order[$pf->id]->order;
                                             ?>
                                            <input type="text" name="friend_order[]" value="<?=$value?>" size="2" style="width:20px"/>
                                            <input type="hidden" name="friend_id[]" value="<?=$pf->id?>" />
                                        </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>  
                      <?php  echo form_close();?>       
            </ul> 
           
        </div>                  

    </div>
</div>
    
    
</div>

<div id="dialog-form" title="Add Selected User to list">
    <form>
    <fieldset>
        <label for="title">Username</label>
        <br>
        <input type="text" name="uname" id="uname" class="text ui-widget-content ui-corner-all" />                   
    </fieldset>
    </form>
</div>

<div id="dialog-success" title="Confirm !" style="display: none;">
    <p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
	<div id="success-title"></div>
    </p>
</div>

<div id="dialog-form-delete" title="Do you want to delete" style="display: none;">
    Do you want to delete <br/>
    <span id="results"></span>
    <br/>
    from your friends
</div>

<script type="text/javascript">

    $(function(){
        
        $( "#add-afriend" )
        .button()
        .click(function() {
            $( "#dialog-form" ).dialog( "open" );
        });
        
        
        $( "#delete-friend" )
        .button()
        .click(function() {
            if(document.getElementById('results').innerHTML.trim() != ':')
                $( "#dialog-form-delete" ).dialog( "open" );
        });
        
        $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Send Request": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/sendKnownRequest') ?>",
                        data: "uname="+$('#uname').val()+"&code=FRIEND",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-success" ).dialog({
	    autoOpen: false,
	    resizable: false,
	    height:180,
	    buttons: {
		Ok: function() {
                     window.location='<?= site_url('lists/friends'); ?>';
		    $( this ).dialog( "close" );
		}
	    },

	    close: function() {
		 window.location='<?= site_url('lists/friends'); ?>';
	    }
	});
        
        
        $( "#dialog-form-delete" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Yes Delete": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/deleteFriends') ?>",
                        data: "results="+document.getElementById('results').innerHTML,
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        
        
       });

</script>

<script>

    function showValues() {
      var fields = $(":input").serializeArray();
      $("#results").empty();
      jQuery.each(fields, function(i, field){
        $("#results").append(field.value + " : ");
      });
    }

    $(":checkbox").click(showValues);    
    showValues();
</script>