<div class="breadcrumb">
<breadcrumb><a  href="<?= site_url('lists'); ?>">My Lists</a> >> Block List</breadcrumb>
</div>
<h2>Block List</h2>

<div class="my_lists">
 <div id="tablule-l1" class="">
    <div class="tabbed_area">
        <ul class="tabs">
            <li><a href="javascript:tabSwitch_2(1, 2, 'tabm_', 'contentm_');" id="tabm_1" class="active">My Block List</a></li>
           

        </ul>

        <div id="contentm_1" class="content">
             <ul>  
                 <li>
                    <div id="frends-add">
                    <button id="add-block">Block a User</button>  
                    <button id="delete-block">Unblock Selected</button> 
                    </div>
                </li>
                   <?php 
                   $user =0;
                   foreach($my_block_lists as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="hot_user" value="<?=$pf->username?>" />
                                        </td>
                                        <td>
                                            <div class="thumbnails">
                                                <a href="<?php echo base_url().'profile?id='.$pf->id?>">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                                </a>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                        <?php  if ($pf->sex == 1) echo "Man "; else echo "Woman "; echo $pf->age."yrs"?><br/>
                                        <?=$pf->location; ?>
                                    </td>                                    
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
             </ul>                     
        </div>
                         

    </div>
</div>
    
    
</div>

<div id="dialog-form" title="Block a user" style="display: none;">
    <form>
    <fieldset>
        <label for="title">Username</label>
        <br>
        <input type="text" name="uname" id="uname" class="text ui-widget-content ui-corner-all" />                   
    </fieldset>
    </form>
</div>

<div id="dialog-success" title="Confirm !" style="display: none;">
    <p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
	<div id="success-title"></div>
    </p>
</div>

<div id="dialog-form-delete" title="Unblock users" style="display: none;">
    Do you want to Unblock <br/>
    <span id="results"></span>   
</div>

<script type="text/javascript">

    $(function(){
        
        $( "#add-block" )
        .button()
        .click(function() {
            $( "#dialog-form" ).dialog( "open" );
        });
        
        $( "#delete-block" )
        .button()
        .click(function() {            
            $( "#dialog-form-delete" ).dialog( "open" );
        });
        
        
        $( "#dialog-form-delete" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Yes Block": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/deleteFrom') ?>",
                        data: "results="+document.getElementById('results').innerHTML  +"&code=BLOCK",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Block": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/sendKnownRequest') ?>",
                        data: "uname="+$('#uname').val()+"&code=BLOCK",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-success" ).dialog({
	    autoOpen: false,
	    resizable: false,
	    height:180,
	    buttons: {
		Ok: function() {
                     window.location='<?= site_url('lists/blockList'); ?>';
		    $( this ).dialog( "close" );
		}
	    },

	    close: function() {
		 window.location='<?= site_url('lists/blockList'); ?>';
	    }
	});
        
        
        
       });

</script>

<script>

    function showValues() {
      var fields = $(":input").serializeArray();
      $("#results").empty();
      jQuery.each(fields, function(i, field){
        $("#results").append(field.value + " : ");
      });
    }

    $(":checkbox").click(showValues);    
    showValues();
</script>
