<script type="text/javascript">
    <?php if(isset($_GET['url']) && $_GET['url']=='2'):?>
        window.onload=function(){tabSwitch_2(2, 2, 'tabm_', 'contentm_');}
    <?php endif;?>
</script>
<div class="breadcrumb">
<breadcrumb><a  href="<?= site_url('lists'); ?>">My Lists</a> >>  Profile Viewed</breadcrumb>
</div>
<h2>Profile Viewed List</h2>

<div class="my_lists">
 <div id="tablule-l1" class="">
    <div class="tabbed_area">
        <ul class="tabs">
            <li><a href="javascript:tabSwitch_2(1, 2, 'tabm_', 'contentm_');" id="tabm_1" class="active">Who Viewed You</a></li>
            <li><a href="javascript:tabSwitch_2(2, 2, 'tabm_', 'contentm_');" id="tabm_2">Who You Viewed</a></li>

        </ul>

        <div id="contentm_1" class="content">
             <ul>  
                 
                   <?php 
                   $user =0;
                   foreach($my_pfv_lists as $pf): 
                   
                        if($user!=$pf->id && $pf->id != $this->session->userdata('id')): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="hot_user" value="<?=$pf->username?>" />
                                        </td>
                                        <td>
                                            <div class="thumbnails">
                                                <a href="<?php echo base_url().'profile?id='.$pf->id?>">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                                </a>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                        <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                    <td  width="300px">
                                        Last Viewed : <?=$pf->last_viewed?></br>
                                        <?php 
                                        if(!isset($friend[$pf->id]))                                        
                                        echo anchor('lists/sendRequest?to='.$pf->id, ' Add As Friend',array('class' => 'btn small')); 
                                        ?>                                        
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
             </ul>                     
        </div>
        <div id="contentm_2" class="content">

             <ul>
                <?php 
                   $user =0;
                   foreach($i_in_pfv_lists as $pf): 
                   
                        if($user!=$pf->id && $pf->id != $this->session->userdata('id')): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="thumbnails">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                        <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                    <td  width="300px">
                                        Last Viewed : <?=$pf->last_viewed?></br>
                                        <?php 
                                        if(!isset($friend[$pf->id]))                                        
                                        echo anchor('lists/sendRequest?to='.$pf->id, ' Add As Friend',array('class' => 'btn small')); 
                                        ?>                                        
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
                
            </ul> 
        </div>                  

    </div>
</div>
    
    
</div>







<script>

    function showValues() {
      var fields = $(":input").serializeArray();
      $("#results").empty();
      jQuery.each(fields, function(i, field){
        $("#results").append(field.value + " : ");
      });
    }

    $(":checkbox").click(showValues);    
    showValues();
</script>
