<div class="breadcrumb">
<breadcrumb>My Lists</breadcrumb>
</div>
<h2>My Lists</h2>

<div class="my_lists">
    <div class="ui-widget-content">
    <table>
        <tr>
            <td>
              <b>Short List</b> 
              <table>
                  <tr>
                      <td><?php echo anchor('lists/shortList', 'My Short List',array('class' => 'btn small')); ?></td>
                      <td>A list of members who you have added to your Short List.</td>
                   </tr>
                   <tr>
                      <td><?php echo anchor('lists/shortList?url=2', "Short List's I'm In",array('class' => 'btn small')); ?></td>
                      <td>A list of members who have added you to their Short List.</td>
                   </tr>
              </table>
            </td>
        </tr>
        <tr>
            <td>
              <b>Hot List</b> 
              <table>
                  <tr>
                      <td><?php echo anchor('lists/hotList', 'Who You Think Is Hot',array('class' => 'btn small')); ?></td>
                      <td>A list of members who you have added to your Hot List.</td>
                   </tr>
                   <tr>
                      <td><?php echo anchor('lists/hotList?url=2', "Hot Lists I'm On",array('class' => 'btn small')); ?></td>
                      <td>A list of members who have added you to their Hot List.</td>
                   </tr>
              </table>
            </td>
        </tr>
        <tr>
            <td>
              <b>Block List</b> 
              <table>
                  <tr>
                      <td><?php echo anchor('lists/blockList', 'My Block List',array('class' => 'btn small')); ?></td>
                      <td>A list of members you have blocked.</td>
                   </tr>                  
              </table>
            </td>
        </tr>
        <tr>
            <td>
              <b>Profile Views</b> 
              <table>
                  <tr>
                      <td><?php echo anchor('lists/profileViewList', 'Who Viewed You',array('class' => 'btn small')); ?></td>
                      <td>A list of members who have viewed your profile.</td>
                   </tr>
                   <tr>
                      <td><?php echo anchor('lists/profileViewList?url=2', "Who You Viewed",array('class' => 'btn small')); ?></td>
                      <td>A list of members whose profile you have viewed.</td>
                   </tr>
              </table>
            </td>
        </tr>
        <tr>
            <td>
              <b>Private Access List</b> 
              <table>
                  <tr>
                      <td><?php echo anchor('lists/privateAccessList', 'Private Gallery List',array('class' => 'btn small')); ?></td>
                      <td>A list of members you have given access to your private pictures.</td>
                   </tr>
                   <tr>
                      <td><?php echo anchor('lists/privateAccessList?url=2', "Private Video List",array('class' => 'btn small')); ?></td>
                      <td>A list of members you have given access to your private video.</td>
                   </tr>
              </table>
            </td>
        </tr>
    </table>
    </div>
</div>