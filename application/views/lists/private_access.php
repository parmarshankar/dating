<script type="text/javascript">
    <?php if(isset($_GET['url']) && $_GET['url']=='2'):?>
        window.onload=function(){tabSwitch_2(2, 2, 'tabm_', 'contentm_');}
    <?php endif;?>
</script>
<div class="breadcrumb">
<breadcrumb><a  href="<?= site_url('lists'); ?>">My Lists</a> >> Private Access</breadcrumb>
</div>
<h2>Private Access List</h2>

<div class="my_lists">
 <div id="tablule-l1" class="">
    <div class="tabbed_area">
        <ul class="tabs">
            <li><a href="javascript:tabSwitch_2(1, 2, 'tabm_', 'contentm_');" id="tabm_1" class="active">Private Gallery List</a></li>
            <li><a href="javascript:tabSwitch_2(2, 2, 'tabm_', 'contentm_');" id="tabm_2">Private Video List</a></li>

        </ul>

        <div id="contentm_1" class="content">
             <ul>  
                 <li>
                    <div id="frends-add">
                    <button id="add-pvtphoto">Add New</button>  
                    <button id="delete-pvtphoto">Delete Selected</button> 
                    </div>
                </li>
                   <?php 
                   $user =0;
                   foreach($my_private_photo_lists as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="p_user[]"  id="p_user"  value="<?=$pf->username?>" />
                                        </td>
                                        <td>
                                            <div class="thumbnails">
                                                <a href="<?php echo base_url().'profile?id='.$pf->id?>">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                                </a>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                        <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                    <td  width="300px">
                                        <?php 
                                        if(!isset($friend[$pf->id]))                                        
                                        echo anchor('lists/sendRequest?to='.$pf->id, ' Add As Friend',array('class' => 'btn small')); 
                                        ?>                                        
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
             </ul>                     
        </div>
        <div id="contentm_2" class="content">

             <ul>
                  <li>
                    <div id="frends-add">
                    <button id="add-pvtvideo">Add New</button>  
                    <button id="delete-pvtvideo">Delete Selected</button> 
                    </div>
                    </li>
                <?php 
                   $user =0;
                   foreach($my_private_video_lists as $pf): 
                   
                        if($user!=$pf->id): 
                        $user = $pf->id ;
                        ?>
                 
                            <li>
                                <table>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="v_user[]" id="v_user" value="<?=$pf->username?>" />
                                        </td>
                                        <td>
                                            <div class="thumbnails">
                                                <?php if (!is_null($pf->photo)){ ?>
                                                    <img onclick="" src="<?php echo base_url().'uploads/'.$pf->photo; ?>" alt="Image Not Loaded"/>
                                                <?php } else echo '<img src="../assets/images/no_avatar.jpg">'; ?>
                                            </div>
                                        </td>
                                    <td  width="300px">
                                        <a href="<?php echo base_url().'profile?id='.$pf->id?>"> <?php echo $pf->username; ?> </a><br/>
                                        <?php
                                        $user_age = (int) substr($pf->age, 2, -6);
                                         echo $pf->cat.' '; 
                                        echo $user_age."yrs"
                                        ?>
                                        <br/>
                                        <?=$pf->location; ?>
                                    </td>
                                </tr>
                             </table>               

                            </li>   
                        <?php 
                        endif;
                    endforeach;?>
                
            </ul> 
        </div>                  

    </div>
</div>
    
    
</div>

<div id="dialog-form-photo" title="Add selected user to private gallery list" style="display: none;">
    <form>
    <fieldset>
        <label for="title">Username</label>
        <br>
        <input type="text" name="unamep" id="unamep" class="text ui-widget-content ui-corner-all" />                   
    </fieldset>
    </form>
</div>

<div id="dialog-form-video" title="Add selected user to private video list" style="display: none;">
    <form>
    <fieldset>
        <label for="title">Username</label>
        <br>
        <input type="text" name="unamev" id="unamev" class="text ui-widget-content ui-corner-all" />                   
    </fieldset>
    </form>
</div>

<div id="dialog-success" title="Confirm !" style="display: none;">
    <p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
	<div id="success-title"></div>
    </p>
</div>

<div id="dialog-form-delete-video" title="Do you want to delete" style="display: none;">
    Do you want to delete <br/>
    <span id="results1"></span>
    <br/>
    From your private video access List
</div>

<div id="dialog-form-delete-photo" title="Do you want to delete" style="display: none;">
    Do you want to delete <br/>
    <span id="results2"></span>
    <br/>
    From your private photo access List
</div>

<script type="text/javascript">

    $(function(){
        
        $( "#add-pvtvideo" )
        .button()
        .click(function() {
            $( "#dialog-form-video" ).dialog( "open" );
        });
        
        $( "#add-pvtphoto" )
        .button()
        .click(function() {
            $( "#dialog-form-photo" ).dialog( "open" );
        });
        
        $( "#delete-pvtvideo" )
        .button()
        .click(function() {            
            $( "#dialog-form-delete-video" ).dialog( "open" );
        });
        
        
        $( "#dialog-form-delete-video" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Yes Delete": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/deleteFrom') ?>",
                        data: "results="+document.getElementById('results1').innerHTML  +"&code=PVTVIDEO",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#delete-pvtphoto" )
        .button()
        .click(function() {            
            $( "#dialog-form-delete-photo" ).dialog( "open" );
        });
        
        
        $( "#dialog-form-delete-photo" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Yes Delete": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/deleteFrom') ?>",
                        data: "results="+document.getElementById('results2').innerHTML  +"&code=PVTPHOTO",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-form-photo" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Add": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/sendKnownRequest') ?>",
                        data: "uname="+$('#unamep').val()+"&code=PVTPHOTO",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-form-video" ).dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            modal: true,
            buttons: {
                "Add": function() {
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('lists/sendKnownRequest') ?>",
                        data: "uname="+$('#unamev').val()+"&code=PVTVIDEO",
                        success: function(msg){
                            document.getElementById('success-title').innerHTML = msg;                           
                            $( "#dialog-success" ).dialog('open');
                           
                        }
                    });
                    $( this ).dialog( "close" );
                },

                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        
        $( "#dialog-success" ).dialog({
	    autoOpen: false,
	    resizable: false,
	    height:200,
	    buttons: {
		Ok: function() {
                     window.location='<?= site_url('lists/privateAccessList'); ?>';
		    $( this ).dialog( "close" );
		}
	    },

	    close: function() {
		 window.location='<?= site_url('lists/privateAccessList'); ?>';
	    }
	});
        
        
        
       });

</script>

<script>

    function showValues() {
      var fieldsv = $("#v_user").serializeArray();
      var fieldsp = $("#p_user").serializeArray();
      $("#results1").empty();
      $("#results2").empty();
      jQuery.each(fieldsv, function(i, field){
        $("#results1").append(field.value + " : ");       
      });
       jQuery.each(fieldsp, function(i, field){        
        $("#results2").append(field.value + " : ");
      });
    }

    $(":checkbox").click(showValues);    
    showValues();
</script>
