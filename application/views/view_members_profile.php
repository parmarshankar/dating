<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title></title>
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
        

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.8.15.custom.min.js"></script>

        <script type="text/javascript">
	 $(document).ready(function() {
		$( "#tabs1" ).tabs();
                $( "#tabs2" ).tabs();
	});
	</script>

        <link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>css/ui-lightness/jquery-ui-1.8.15.custom.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">



.thumbnails img {
height: 50px;
border: 1px solid #555;
padding: 1px;

}

.thumbnails img:hover {
border: 1px solid #00ccff;
cursor:pointer;
}


</style>
</head>

<body>

<div id="wrapper">

	<div id="header">


        <?php echo anchor(base_url().'members/members_area', 'Home'); ?>
        <br/>
        <?php echo anchor(base_url().'user/logout', 'Logout'); ?>
        <br/>
	</div><!-- #header-->

	<div id="middle">

		<div id="container">
			<div id="content">
			<div id="accountinfo">
                                    <p><b><?php if ($sex == 1) echo "Man "; else echo "Woman "; echo $age."yrs" ?></b></p>
                                    <p><b>Town/City:</b> ?</p>
                                    <p><b>Location: </b><?php echo $location; ?></p>
                                    <p><b>Phone No.:</b>n/a</p>                                   
                                    <p><b>Seeking:</b>?</p>
                                    <p><b>Who Are:</b>?</p>

                                           <div id="tabs1">
                                            <ul>
                                                    <li><a href="#tabs-1">Physical</a></li>
                                                    <li><a href="#tabs-2">Intimate</a></li>
                                                    <li><a href="#tabs-3">Life Style</a></li>
                                            </ul>
                                            <div id="tabs-1">
                                                    <li><b>Body type:</b><?php echo $body_type;?></li>
                                                    <li><b>Height:</b><?php echo $height;?></li>
                                                    <li><b>Race:</b><?php echo $race_background;?></li>
                                                    <li><b>Eye colour:</b><?php echo $eye_colour;?></li>
                                                    <li><b>Hair colour:</b><?php echo $hair_colour;?></li>
                                                    <li><b>Hair length:</b><?php echo $hair_length;?></li>
                                                    
                                                    
                                                    
                                                    
                                            </div>
                                            <div id="tabs-2">
                                                     <li><b>Participation:</b><?php echo $participation_level;?></li>
                                                    <li><b>Orientation:</b><?php echo $sexual_orientation;?></li>
                                                    <li><b>Safe Sex:</b><?php echo $safe_sex;?></li>
                                                    <li><b>Personality:</b><?php echo $personality;?></li>
                                            </div>
                                            <div id="tabs-3">
                                                    <li><b>Relationship:</b><?php echo $relationship_status;?></li>
                                                    <li><b>Star sign:</b><?php echo $sexual_orientation;?></li>
                                                    <li><b>Drinking:</b><?php echo $drinking;?></li>
                                                    <li><b>Smoking:</b><?php echo $smoking;?></li>
                                            </div>
                                            </div>

                                            <div id="tabs2">
                                            <ul>
                                                    <li><a href="#tabs-1">General Interest</a></li>
                                                    <li><a href="#tabs-2">Sexual Interest</a></li>
                                                    <li><a href="#tabs-3">Fetish Interest</a></li>
                                            </ul>
                                            <div id="tabs-1">
                                                <?php
                                                if (is_array(unserialize($general_interests)))
                                                foreach (unserialize($general_interests) as $value) {
                                                ?>
                                                <li><?php echo $value;?></li>
                                                <?php
                                                }
                                                ?>


                                            </div>
                                            <div id="tabs-2">
                                                   <?php
                                                if (is_array(unserialize($sexual_interests)))
                                                foreach (unserialize($sexual_interests) as $value) {

                                                ?>
                                                <li><?php echo $value;?></li>
                                                <?php
                                                }

                                                ?>

                                            </div>
                                            <div id="tabs-3">
                                                <?php
                                                if (is_array(unserialize($fetish_interests)))
                                                foreach (unserialize($fetish_interests) as $value) {
                                                ?>
                                                <li><?php echo $value;?></li>
                                                <?php
                                                }
                                                ?>

                                                    
                                            </div>
                                            </div>
                        </div>
			</div><!-- #content-->
		</div><!-- #container-->

		<div class="sidebar" id="sideLeft">
                    
                                    <?php if($photos) {?>
                                    <div id="photoarea">
                                    <img id="preview"  src="<?php echo base_url().'uploads/'.$photos[0]->photo; ?>" alt="No Image Loaded"/>
                                    </div>

                                    <?php
                                    $i = 1;
                                    foreach ($photos as $photo):?>
                                    <div class="thumbnails">
                                            <img onclick="<?php echo "document.getElementById('preview').src=document.getElementById('img".$i."').src"; ?>" id="<?php echo "img".$i; ?>" src="<?php echo base_url().'uploads/'.$photo->photo; ?>" alt="Image Not Loaded"/>
                                    </div>

                                    <?php
                                    $i++;
                                    endforeach;?>
                                        <br/>
                                    
                                    

                                    <br/>
                                    
                                    <?php };?>
                    
		</div><!-- .sidebar#sideLeft -->

		<div class="sidebar" id="sideRight">
                      Messages, interaction, etc
		</div><!-- .sidebar#sideRight -->

	</div><!-- #middle-->

</div><!-- #wrapper -->

<div id="footer">
	<strong>Footer will be here</strong>
</div><!-- #footer -->

</body>
</html>