<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Edit my photos</title>

<style type="text/css">



.thumbnails img {
height: 80px;
border: 2px solid #555;
padding: 1px;
margin: 0 10px 10px 0;
}

.thumbnails img:hover {
border: 2px solid #00ccff;
cursor:pointer;
}

.preview img {
border: 2px solid #444;
padding: 1px;
height: 500px;
}
</style>

</head>
<body>
<?php echo $error;?>

<?php echo anchor(base_url().'members/view_profile', 'View my profile'); ?>
<br/>
<?php echo anchor(base_url().'members/edit_profile', 'Edit my profile'); ?>
<br/>
<?php echo anchor(base_url().'user/logout', 'Logout'); ?>
<br/>


<?php if($photos) {?>
<div class="gallery">

<?php
$i = 1;
foreach ($photos as $photo):?>
<div class="thumbnails">
	<img onclick="<?php echo "document.getElementById('preview').src=document.getElementById('img".$i."').src"; ?>" id="<?php echo "img".$i; ?>" src="<?php echo base_url().'uploads/'.$photo->photo; ?>" alt="Image Not Loaded"/>
</div>

<?php
$i++;
endforeach;?>
    <br/>

<div class="preview">
	<img id="preview" name="preview" src="<?php echo base_url().'uploads/'.$photo->photo; ?>" alt="No Image Loaded"/>
</div>

<br/>
</div>
<?php };?>

<?php echo form_open_multipart(base_url().'members/upload_photo');?>

<input type="file" name="userfile" size="20" />

<br /><br />

<input type="submit" value="upload" />

<?php
echo form_close();
?>
</body>
</html>