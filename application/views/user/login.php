<div id="members_login">
    <table>
        <tr>
            <td>
<div id="login-reg">
<div id="home-registration" class="home-top-form">
           <div class="main-module-head"> Are you new </div>           
           <div class="main-module-content">
               <?=form_open('user/quickregister',array('name' => 'user_register', 'id' => 'user_register'))?>
                <table>
                    <tbody>
                    <tr> 
                        <td class="home-form-lable"><label>Email</label></td>
                        <td class="home-form-input"><input type="text" id="email" name="email" value="<?=set_value('email');?>" /></td>
                    </tr>
                    <tr> 
                        <td class="home-form-lable"><label>Username</label></td>
                        <td class="home-form-input"><input type="text" id="username" name="username" value="<?=set_value('username');?>" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"><label>Password</label></td>
                        <td class="home-form-input"><input type="password" id="password" name="password" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"><label>Verify Password</label></td>
                        <td class="home-form-input"><input type="password" id="password_confirm" name="password_confirm" /></td>
                    </tr>
                    <tr>
                        <td class="home-form-lable"></td>
                        <td class="home-form-input"><input type="submit" name="Join" value="Join"/></td>
                    </tr>
                    </tbody>
                </table>
                <?=form_close();?>
           </div>
           <div class="main-module-foot"></div>
</div>  
</div>
                </td>
                <td style="padding-left: 60px;">
<h2>User Login</h2>
<div class="title-tab-form">Please enter the following info:</div>
<div class="members_login_form">
<?=form_open('user/login',array('name' => 'user_login', 'id' => 'user_login'))?>
<div class="form_errors">
    <?php echo validation_errors(); ?>
</div>
<table cellspacing="0" cellpadding="0" border="0">
    <tr>
	<td class="heading" width="20%">Username</td>
	<td><input type="text" id="username" name="username" value="<?=set_value('username');?>" /></td>
    </tr>   
    <tr>
	<td class="heading" >Password</td>
	<td><input type="password" id="password" name="password" /></td>
    </tr>
    <tr>
	<td style="font-size: 10px;">All fields are compulsory</td>
	<td>
            <input type="hidden" name="location" value="user/login"/>
	    <input name="button" type="submit" class="btn-contact" id="button" value="Submit" /> 
	    <input name="button" type="button" class="btn-contact" id="button" value="Cancel" onclick="window.location = '/'"/> 
	</td>
    </tr>
    
</table>
    <?=form_close();?>
</div>
</td></tr></table>
</div>