

<h2>User Registration</h2>
<?php
$col_width = 'style="width: 200px; left:200px;"';
$location_options['']= 'Select the location';
$location_options=array_merge ($location_options,$locations);
$user_types_options = $user_types;
$maxday = (date('Y')-10).'-'.date('m-d');
$minday = (date('Y')-90).'-'.date('m-d');
$maxy = (date('Y')-10);
$miny = (date('Y')-90);
?>
<script>
        jQuery.noConflict();
	jQuery(function() {                 
		jQuery( "#bday" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: '<?=$maxday?>',
                    minDate: '<?=$minday?>',
                    yearRange: "<?=$miny?>:<?=$maxy?>",
                    changeMonth: true,
                    changeYear: true
                });
               
	});
</script>
<div id="register">
<div class="title-tab-form">Please enter the following info:</div>
<div class="ui-widget-content">
<?=form_open('user/register',array('name' => 'user_register', 'id' => 'user_register'))?>
<div class="form_errors">
    <?php echo validation_errors(); ?>
</div>
<table width="600px" cellspacing="0" cellpadding="0" border="0">
    <tr>
	<td class="heading" width="50%">Username *</td>
	<td><input type="text" id="username" name="username" value="<?=set_value('username');?>" <?=$col_width?>/></td>
    </tr>   
    <tr>
	<td class="heading" >First Name *</td>
	<td><input type="text" id="firstname" name="firstname" value="<?=set_value('firstname');?>"  <?=$col_width?> /></td>
    </tr>
    <tr>
	<td class="heading" >Last Name *</td>
	<td><input type="text" id="lastname" name="lastname" value="<?=set_value('lastname');?>" <?=$col_width?> /></td>
    </tr>
    <tr>
	<td class="heading" >E-mail *</td>
	<td><input type="text" id="email" name="email" value="<?=set_value('email');?>" <?=$col_width?> /></td>
    </tr>
    <tr>
	<td class="heading" >Password *</td>
	<td><input type="password" id="password" name="password" <?=$col_width?> /></td>
    </tr>
    <tr>
	<td class="heading" >Password Confirm *</td>
	<td><input type="password" id="password_confirm" name="password_confirm" <?=$col_width?> /></td>
    </tr>
    <tr>
	<td class="heading" >Location *</td>
	<td>
	    <?php echo form_dropdown('location', $location_options, set_value('location') ,$col_width); ?>
	</td>
    </tr>
    <tr>
	<td class="heading" >City or suburb</td>
	<td>
	    <input type="text" id="city_suburb" name="city_suburb" value="<?=set_value('city_suburb');?>" <?=$col_width?> />
	</td>
    </tr>
    <tr>
	<td class="heading" >Postcode</td>
	<td>
	    <input type="text" id="postcode" name="postcode" value="<?=set_value('postcode');?>" <?=$col_width?> />
	</td>
    </tr>
    <tr>
	<td class="heading">I am / We are *</td>
	<td>
            <?php foreach($user_types_options as $key=>$user_types_option):?>
	    <input <?php echo set_radio('sex', $key); ?> value="<?=$key?>"  name="sex" class="txt-register_tutor_input_radio" type="radio" /> <span style="font-size: 14px;margin-right: 5px;"><?=$user_types_option?></span>
            <br/>
            <?php endforeach;?>
	</td>
        
    </tr>
<!--    <tr>
	<td class="heading" width="20%">Age</td>
	<td><select name="age" id="age" style="width: 145px;">
	<?php for ($index = 10;$index <= 80;$index++) {?>
		<option value="<?=$index;?>"><?=$index;?></option>
	<?php } ?>
	    </select>
	</td>
    </tr>-->
    <tr>
	<td class="heading" >My birthday *</td>
	<td>            
            <?php
//            $cutoff = date('Y')-100;
//            // current year
//            $now = date('Y')-10;
//
//            // build years menu
//            echo '<select name="year" style="width: 55px;>' . PHP_EOL;
//             echo '  <option value="">year</option>' . PHP_EOL;
//            for ($y=$now; $y>=$cutoff; $y--) {
//                echo '  <option value="' . $y . '">' . $y . '</option>' . PHP_EOL;
//            }
//            echo '</select>' . PHP_EOL;
//
//            // build months menu
//            echo '<select name="month" style="width: 45px;>' . PHP_EOL;
//             echo '  <option value="">month</option>' . PHP_EOL;
//            for ($m=0; $m<=12; $m++) {
//                echo '  <option value="' . $m . '">' . date('M', mktime(0,0,0,$m,1,2000)) . '</option>';
//            }
//            echo '</select>';
//
//            // build days menu
//            echo '<select name="day" style="width: 35px;>' . PHP_EOL;
//             echo '  <option value="">day</option>' . PHP_EOL;
//            for ($d=0; $d<=31; $d++) {
//                echo '  <option value="' . $d . '">' . $d . '</option>' . PHP_EOL;
//            }
//            echo '</select>' . PHP_EOL;

            ?>
            <input id="bday" name="bday" type="text" value="" <?=$col_width?>>
        </td>
    </tr>
    <tr>
	<td style="font-size: 10px;">fields with * are compulsory</td>
	<td>
	    <input  type="submit" class="btn-contact" id="button" value="Submit" /> 
	    <input  type="button" class="btn-contact" id="button" value="Cancel" onclick="window.location = '/'"/>
	</td>
    </tr>    
</table>
    <?=form_close();?>
</div>
</div>

