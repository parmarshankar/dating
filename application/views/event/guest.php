<div class="breadcrumb">
<breadcrumb><a href="<?=site_url('publicevent')?>">Events & Parties</a>
   
    >>  <a href="<?=site_url('publicevent/view/'.$event->id)?>"><?=$event->event_title?></a>
    >> Guests
</breadcrumb>
</div>

<h2>Guests of <?=$event->event_title?></h2>
<div id="event_list">
<?php
$user =0;

foreach ($guests as $result):
   $warining =''; 
    $user = $result->id ;  
    ?>
           
                <div class="ui-widget-content"> 
                <table>
                    <tr>
                    <td>
                    <div class="thumbnails">
                    <?php
                    if (!is_null($result->photo))
                    {
                    ?>
                    <img onclick="" src="<?php echo base_url().'uploads/'.$result->photo; ?>" alt="Image Not Loaded"/>

                    <?php
                    }
                    else echo '<img src="../assets/images/no_avatar.jpg">';

                    ?>
                    </div>
                    </td>
                    <td  width="300px">
                    <a href="<?php echo base_url().'profile?id='.$result->id?>"> <?php echo $result->username; ?> </a>

                    <br/>
                    <?php
                    $user_age = (int) substr($result->age, 2, -6);
                    echo $result->cat.' '; 
                    echo $user_age."yrs"
                    ?>
                    <br/>
                    <?php
                    echo $result->location;
                    ?>
                    <br/>
                    <?php if($result->comment!=''){
                        ?>
                    <div class="search_warn">
                    <?php echo $result->comment;?>
                    </div>

                    <?php
                    }?>
                    </td>
       
                </tr>
            </table>             
            </div>


    <?php

endforeach;
?>
</div>

<div id="EventSearch">    
<?php echo $template['partials']['event_current']; ?>
</div>
