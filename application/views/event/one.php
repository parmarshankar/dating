<link href="../../assets/css/tabular.css" rel="stylesheet" type="text/css"/>
<div class="breadcrumb">
<breadcrumb><a href="<?=site_url($from)?>">Events & Parties</a> >> <?=$event->event_title?></breadcrumb>
</div>

<h2><?=$event->event_title?></h2>

<div id="event_list">
<div class="message">
    <?php if(isset($message['success'])): ?>
        <div class="success">
            <?php echo $message['success'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['warn'])): ?>
        <div class="warn">
            <?php echo $message['warn'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['error'])): ?>
        <div class="error">
            <?php echo $message['error'];?>
        </div>
    <?php endif; ?>
</div>
<a href="javascript: history.go(-1)" style="float:right"><back><< Back </back></a>
<?php if($event->banner_name=='')
{
    $event->banner_name = 'noImageAvailable.jpg';
}
?>  
<div class="ui-widget-content">	
    <div class="imageContainer">
       <img  src="<?=site_url()?>/uploads/events/<?=$event->banner_name?>" galleryimg="no" >
    </div>
    <div class="clear"></div>
    
    <div class="EventBreakdown"> 
        <div class="EventInformation">            
            <div class="postImg">            
                <img style="height:90px;width:90px;border-width:0px;" alt="<?=$event->addedName?>" src="<?=site_url()?>/uploads/<?=$event_added_by->photo?>" title="<?=$event->addedName?>" >

            </div>
            <div class="postInfo">
                <strong>Posted By:</strong><br>
                <a target="_blank" href="<?=site_url('profile')?>?id=<?=$event->event_added_by?>" ><?=$event->addedName?></a>
                <br>
                <br>
                <?php 
                            $weekday = date('l', strtotime($event->held_date));
                            $year = date('Y', strtotime($event->held_date));
                            $month = date('F', strtotime($event->held_date));
                            $day = date('d', strtotime($event->held_date));
                 ?>     
                <strong>Event Date:</strong><br>
                <?=$weekday?>, <?=$day?> <?=$month?> <?=$year?> <?=$event->time?>  
            </div>
        </div>
          
        <div class="EventDescription">
            <?=$event->event_description?>        
            <br/><br/>
            <div  class="Categories">			
                Categories: 
                <?php foreach ($event->categories as $category):?>
                <a  href="<?=site_url('publicevent/category/'.$category->id)?>"><?=$category->name?></a>,
                <?php endforeach;?>   	        
            </div>        
        </div>
    </div>
    <div class="clear"></div>
    <div class="EventExplanation"> 
        <table width="100%">
            <tr>
                <td width="50%" valign="top">
                    <div class="content">
                        <ul>
                        <li> <b>Dress Code:</b> <?=$event->dress_code?> </li>  
                        <li> <b>Rules:</b> <?=$event->event_rules?> </li>
                        <li> <b>Guest Age:</b> <?=$event->aged1?> to <?=$event->aged2?></li>
                        <li> <b>Cost:</b> <?=$event->entry_fee?></li>
                                                
                        </ul>
                    </div>
                </td>
                <td width="50%" valign="top"> 
                     <div class="content">
                        <ul>
                        <li> <b>Location:</b> <?=$event->location?> </li>
                        <li> <b>Guest Type:</b> <?php echo implode(', ', unserialize($event->guest_types))?> </li>
                        <?php if($event->show_email == 1):?>
                        <li> <b>Email:</b> <?=$event_added_by->email?> </li>
                         <?php endif;?>
                        <?php if($event->show_contact_number == 1):?>
                        <li> <b>Contact Number:</b> <?=$event_added_by->telephone_no?> </li>
                         <?php endif;?>
                        <li> <b>Website:</b> <?=$event->website_url?></li>                        
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
   <!--has to function-->
   <?php if($event->accept_interest == 1 && $this->session->userdata('logged')):?>
   <?php if($matched==1):?>
   <?php echo form_open('publicevent/register'); ?>
    <?php endif;?> 
  
   
   <?php if($ispast != 1  ):?>
    <div class="EventInterest"> 
        <h3>Register Your Interest</h3>
        <p>Put your comment [optional] and register your interest:</p>
        <?php        
            $data_comment = array(
              'name'        => 'comment',
              'id'          => 'comment',
              'value'       => '',
              'cols'   => '75',
              'rows'        => '2',              
            );            
            echo form_textarea($data_comment); 
        ?>
        <input type="hidden" name="event_id" id="event_id" value="<?=$event->id?>">
         <?php if($matched==1):?>
        <?php echo form_submit(array('name' => 'edit','class' => 'small','style'=>'float:right'), 'Register Your Interest'); ?>
        <?php endif;?> 
        <?php if($matched==0):?>
        <input type="button" id="request" style="padding: 3px 7px;line-height: 12px;float:right" class="small" value="Register Your Interest"/>
        <?php endif;?>
        <div class="clear"></div>
    </div> 
   <?php endif;?>  
   <?php if($matched==1):?>
   <?php echo form_close();?> 
   <?php endif;?>  
     
   <?php endif;?>  
   <div class="clear"></div>
   <?php if($event->accept_interest == 1 ):?>
    <div class="EventGuest"> 
        <span style="float:left;"><?=$guestCount?> user interested in this event.</span>
        <span style="float:right;"><a href="<?=site_url('publicevent/viewGuests/'.$event->id)?>">View interested users</a></span>
        <div class="clear"></div>
    </div>
   <?php endif;?>
</div>



</div>

<div id="EventSearch">    
<?php echo $template['partials']['event_current']; ?>
<?php echo $template['partials']['event_past']; ?>    
</div>


<?php if($event->accept_interest == 1 && $this->session->userdata('logged') && $matched==0):?>
<?php
$user_sex = '';
if($view_by->sex == 1)
$user_sex ='Man';
if($view_by->sex == 0)
$user_sex ='Woman';        
?>
<div id="not-match" title="Not Match" style="display: none;">
    <p>	Hey there. This is a friendly note to alert you that "<?=$event->event_title?>" is not looking for:</p>
    
    <p>
        <span class="ui-icon ui-icon-circle-arrow-e" style="float:left; margin:0 7px 20px 0;">Your Gender Group</span>        
    </p>
    
     <p>
        <table>
            <tr>
                <td></td>
                <td>They are looking for...</td>
                <td>You are...</td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td class="<?=$matched_type?>"><?php echo implode(', ', unserialize($event->guest_types))?></td>
                <td class="<?=$matched_type?>"><?=$user_sex?></td>
            </tr>
            <tr>
                <td>Age Between</td>
                <td class="<?=$matched_age?>"><?=$event->aged1?> to <?=$event->aged2?></td>
                <td class="<?=$matched_age?>"><?=$view_by->age?></td>
            </tr>
        </table>       
    </p>
</div>
 <?php endif;?> 


<script type="text/javascript">

    $(function(){
	
        <?php if($event->accept_interest == 1 && $this->session->userdata('logged') && $matched==0):?>
        $('#request').click(function(){
	    $( "#not-match" ).dialog("open");
	});
        <?php endif;?> 

	$( "#not-match" ).dialog({
	    autoOpen: false,
	    resizable: false,
	    height:300,
            width:500,
	    modal: true,
	    buttons: {
		"OK Cancle": function() {
		    $( this ).dialog( "close" );
		}		
	    }

	});

    });

</script>
