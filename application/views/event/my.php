<div class="breadcrumb">
<breadcrumb><a href="<?=site_url('publicevent')?>">Events & Parties</a> >> My events</breadcrumb>
</div>

<h2>My events</h2>


<div id="event_list">

    <div class="message">
        <?php if(isset($message['success'])): ?>
            <div class="success">
                <?php echo $message['success'];?>
            </div>
        <?php endif; ?>
        <?php if(isset($message['warn'])): ?>
            <div class="warn">
                <?php echo $message['warn'];?>
            </div>
        <?php endif; ?>
        <?php if(validation_errors()!='' || isset($message['error'])): ?>
            <div class="error">
                <?php if(isset($message['error']))echo $message['error'];?>
                <div class="form_errors">
                <?php echo validation_errors(); ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
    
<?php foreach($events as $event):?>
    
<?php if($event->banner_name=='')
{
    $event->banner_name = 'noImageAvailable.jpg';
}
?>   
<div class="ui-widget-content">	
            <div class="event">
            <div class="thumbnail">
                <img  title="<?=$event->event_title?>" src="<?=site_url()?>/uploads/events/<?=$event->banner_name?>" alt="<?=$event->event_title?> small banner" style="height:70px;width:110px;border-width:0px;">                
            </div>
           <div class="info">
                <a  class="UserName" href="<?=site_url('publicevent/view/'.$event->id)?>"><?=$event->event_title?></a><br>
		        Posted by: <a href="<?=site_url('profile')?>?id=<?=$event->event_added_by?>" target="_blank"><?=$event->addedName?></a>		        
                        
            </div> 
            <?php if($event->accept_interest == 1 ):?>
            <a  class="GuestList" href="#">
                <div class="Padded">
                    <div class="guestRow">
                        Guestlist</div>
                    <div class="Guest">
                        <?=$event->guestCount ?>
                    </div>
                </div>
            </a> 
            <?php endif;?>
        <div class="CalendarDate">
                            <?php 
                            $weekday = date('D', strtotime($event->held_date));
                            $year = date('Y', strtotime($event->held_date));
                            $month = date('M', strtotime($event->held_date));
                            $day = date('d', strtotime($event->held_date));
                            ?>                            
            <div class="Date"><?=$day?></div>
            <div class="Month"><?=$month?></div>
            <div class="Day"><?=$weekday?></div>
            <div class="Year"><?=$year?></div>
        </div> 
         <div class="clear"></div>
        <div  class="Categories">			
		            Categories: 
                            <?php foreach ($event->categories as $category):?>
                            <a  href="<?=site_url('publicevent/category/'.$category->id)?>"><?=$category->name?></a>,
                            <?php endforeach;?>   	        
        </div>
         <?php if($event->event_added_by == $this->session->userdata('id')):?>
        <div class="action">
            <?php echo anchor('event/edit/'.$event->id, 'Edit',array('class' => 'btn small')); ?>
            <?php echo anchor('event/delete/'.$event->id, 'Delete',array('class' => 'btn small')); ?>
        </div>
         <?php endif;?>
         <div class="clear"></div>
        </div>
</div>

<?php endforeach;?>
 
 <?php if($this->pagination->create_links() !=''):?>
 <div class="pagination">     
<?php echo $this->pagination->create_links(); ?>
</div>
    <?php endif;?>
</div>

<div id="EventSearch">    
<?php echo $template['partials']['event_current']; ?>
</div>



