<link href="../../assets/css/tabular.css" rel="stylesheet" type="text/css"/>
<div class="breadcrumb">
<breadcrumb><a href="<?= site_url('event'); ?>">Event</a> >> <a href="<?= site_url('event/myevents'); ?>">My Events</a> >> Delete </breadcrumb>
</div>

<h2>Delete : <?=$event->event_title?></h2>

<div id="event_list">
<div class="message">

        <div class="success">
           Do You want to delete <?=$event->event_title?> ? <br/>
           <?php echo form_open_multipart('event/delete/');?>
             <input type="hidden" name="id" value="<?=$event->id?>">
              <?php echo form_submit(array('name' => 'Update','class' => 'button'), 'Yes Delete'); ?>
             <input  type="button" class="btn-contact" id="button"  value="Cancel" onclick="window.location = '/'"/>
           <?php echo form_close(); ?>
        </div>
    
</div>
<?php if($event->banner_name=='')
{
    $event->banner_name = 'noImageAvailable.jpg';
}
?>  
<div class="ui-widget-content">	
    <div class="imageContainer">
       <img  src="<?=site_url()?>/uploads/events/<?=$event->banner_name?>" galleryimg="no" >
    </div>
    <div class="clear"></div>
    
    <div class="EventBreakdown"> 
        <div class="EventInformation">            
            <div class="postImg">            
                <img style="height:90px;width:90px;border-width:0px;" alt="<?=$event->addedName?>" src="<?=site_url()?>/uploads/<?=$event_added_by->photo?>" title="<?=$event->addedName?>" >

            </div>
            <div class="postInfo">
                <strong>Posted By:</strong><br>
                <a target="_blank" href="<?=site_url('profile')?>?id=<?=$event->event_added_by?>" ><?=$event->addedName?></a>
                <br>
                <br>
                <?php 
                            $weekday = date('l', strtotime($event->held_date));
                            $year = date('Y', strtotime($event->held_date));
                            $month = date('F', strtotime($event->held_date));
                            $day = date('d', strtotime($event->held_date));
                 ?>     
                <strong>Event Date:</strong><br>
                <?=$weekday?>, <?=$day?> <?=$month?> <?=$year?> <?=$event->time?>  
            </div>
        </div>
          
        <div class="EventDescription">
            <?=$event->event_description?>        
            <br/><br/>
            <div  class="Categories">			
                Categories: 
                <?php foreach ($event->categories as $category):?>
                <a  href="<?=site_url('publicevent/category/'.$category->id)?>"><?=$category->name?></a>,
                <?php endforeach;?>   	        
            </div>        
        </div>
    </div>
    <div class="clear"></div>
    <div class="EventExplanation"> 
        <table width="100%">
            <tr>
                <td width="50%" valign="top">
                    <div class="content">
                        <ul>
                        <li> <b>Dress Code:</b> <?=$event->dress_code?> </li>  
                        <li> <b>Rules:</b> <?=$event->event_rules?> </li>
                        <li> <b>Guest Age:</b> <?=$event->aged1?> to <?=$event->aged2?></li>
                        <li> <b>Cost:</b> <?=$event->entry_fee?></li>
                                                
                        </ul>
                    </div>
                </td>
                <td width="50%" valign="top"> 
                     <div class="content">
                        <ul>
                        <li> <b>Location:</b> <?=$event->location?> </li>
                        <li> <b>Guest Type:</b> <?php echo implode(', ', unserialize($event->guest_types))?> </li>
                        <?php if($event->show_email == 1):?>
                        <li> <b>Email:</b> <?=$event_added_by->email?> </li>
                         <?php endif;?>
                        <?php if($event->show_contact_number == 1):?>
                        <li> <b>Contact Number:</b> <?=$event_added_by->telephone_no?> </li>
                         <?php endif;?>
                        <li> <b>Website:</b> <?=$event->website_url?></li>                        
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
      
</div>



</div>

<div id="EventSearch">    
<?php echo $template['partials']['event_current']; ?>
</div>
