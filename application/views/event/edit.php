<?php
 $col_width = 'style="width: 200px;"';
 $col_width_small = 'style="width: 50px;"';
 $today = date('Y-m-d');
?>

        
<link href="<?= asset_url(); ?>css/jquery.multiSelect.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?= asset_url(); ?>js/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="<?= asset_url(); ?>js/jquery.multiSelect.js"></script>

<script>
        jQuery.noConflict();
	jQuery(function() {                 
		jQuery( "#helddate" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: '<?=$today?>'
                });
               
	});
        
       

</script>



<div class="breadcrumb">
<breadcrumb><a href="<?= site_url('event'); ?>">Event</a> >> <a href="<?= site_url('event/myevents'); ?>">My Events</a> >> Edit </breadcrumb>
</div>

<h2>Edit Event : <?=$event->event_title?></h2>
<div id="event_list">
    
    <div class="message">
        <?php if(isset($message['success'])): ?>
            <div class="success">
                <?php echo $message['success'];?>
            </div>
        <?php endif; ?>
        <?php if(isset($message['warn'])): ?>
            <div class="warn">
                <?php echo $message['warn'];?>
            </div>
        <?php endif; ?>
        <?php if(validation_errors()!='' || isset($message['error'])): ?>
            <div class="error">
                <?php if(isset($message['error']))echo $message['error'];?>
                <div class="form_errors">
                <?php echo validation_errors(); ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
    
    <?php echo form_open_multipart('event/edit/');?>
     <div class="ui-widget-content">
         <table width="100%">
            <tr>
                <td width="200px"><lable>* Name / Title</lable></td>                
                <td><?php echo form_input('event_title', $event->event_title, $col_width); ?></td>
            </tr>
            <tr>
                <td><lable>* Date Held</lable></td>                
                <td><input id="helddate" name="helddate" type="text" value="<?=$event->held_date?>" <?=$col_width?>></td>
            </tr>  
            <tr>
                <td><lable>* Type of guests</lable></td>                
         <td><?php echo form_multiselect('guest_types[]', $guest_types, unserialize($event->guest_types), 'id="guest_types"'); ?></td>
            </tr>
             <tr>
                <td><lable>* Event Category</lable></td>                
                <td><?php echo form_multiselect('category_id[]', $category_id, unserialize($event->category_id), 'id="category_id"'); ?></td>
            </tr>
            <tr>
                <td><lable>* Age range</lable></td>                
                <td><?php echo form_input('aged1', $event->aged1, $col_width_small); ?> 
                    &nbsp;&nbsp;to  &nbsp;
                    <?php echo form_input('aged2', $event->aged2, $col_width_small); ?>
                    &nbsp; years of age
                </td>
            </tr>
             <tr>
                <td><lable>* Location</lable></td>                
                <td>                    
                    <?php echo form_dropdown('location', $location, $event->location, $col_width); ?>
                </td>
            </tr>
            <tr>
                <td rowspan="3"><lable>* Description</lable></td>
                <td style="font-size: 10px;">                
                please give a brief description of your event (max 1000 characters) 
               </td>
                
            </tr>
             <tr>                            
                <td> 
                    <?php
                    $dataDescription = array(
                      'name'        => 'event_description',
                      'id'          => 'event_description',
                      'value'       => $event->event_description,
                      'cols'   => '40',
                      'rows'        => '5',   
                      'onKeyDown'   => "limitText(this.form.event_description,'countdown',1000)",
                        '' =>"limitText(this.form.limitedtextarea,'countdown',1000)"
                    );
                    echo form_textarea($dataDescription); ?>
                </td>
            </tr>
            <tr>                
                <td style="font-size: 10px;">
                <span id="countdown"></span>
               </td>
                
            </tr>
            <tr>
                <td><lable>Maximum attendees</lable></td>                
                <td>
                    <table>
                        <tr>
                            <td><?php echo form_input('max_attendess', $event->max_attendess, $col_width_small); ?></td>
                            <td style="font-size: 10px;">Leave blank if there's no maximum limit of attendees, <br/>all members are invited</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><lable>Accept interest</lable></td>                
                <td>
                    <table>
                        <tr>
                            <td>
                                 <?php 
                                    $data_accept_interest = array(
                                    'name'        => 'accept_interest',
                                    'id'          => 'accept_interest',
                                     'checked'     => $event->accept_interest,
                                    'value'       => '1',                        
                                    'style'       => 'margin-right:10px'

                                    );                    
                                    echo  form_checkbox($data_accept_interest);
                                ?>
                            </td>
                            <td style="font-size: 10px;">Tick to enable other members register their interest</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><lable>Ticket Booking URL</lable></td>                
                <td><?php echo form_input('ticket_booking_url', $event->ticket_booking_url, $col_width); ?><br/>
                <span  style="font-size: 10px;">
                    Link this event/party to your website ticket booking page.  <br/>
                    <b>Note:</b> Link will need to be approved by moderator to be displayed.
                </span>
                </td>
            </tr>
            <tr>
                <td><lable>Upload Banner</lable></td>                
                <td><input type="file" name="userfile" /><br/>
                    <span  style="font-size: 10px;">
                        Please enter banner file only if you wish to include banner for your event.<br/>
                        <b>Note:</b> the best size of banner is 630px width and 250px height
                </span>
                </td>
            </tr>
        </table>
     </div>
    <br/><br/>
    <div id="clickme" onclick='jQuery("#divEventDetails").toggle("slow");'>
    <h3>+ Add Other Event Details</h3>
    </div>
    <div id="divEventDetails">
    <div class="ui-widget-content">
        <table width="100%">           
            <tr>
                <td rowspan="3" width="200px"><lable>Rules</lable></td>
                <td style="font-size: 10px;">                
                Rules you may have for your event (max 500 characters)
               </td>                
            </tr>
             <tr>                            
                <td> 
                    <?php
                    $dataRules = array(
                      'name'        => 'event_rules',
                      'id'          => 'event_rules',
                      'value'       => $event->event_rules,
                      'cols'   => '40',
                      'rows'        => '5',   
                      'onKeyDown'   => "limitText(this.form.event_rules,'countdown2',500)",
                        '' =>"limitText(this.form.limitedtextarea,'countdown2',1000)"
                    );
                    echo form_textarea($dataRules); ?>
                </td>
            </tr>
            <tr>                
                <td style="font-size: 10px;">
                <span id="countdown2"></span>
               </td>
                
            </tr>
            <tr>
                <td><lable>Theme</lable></td>                
                <td>
                   <?php echo form_input('theme', $event->theme, $col_width); ?>
                </td>
            </tr>
            <tr>
                <td><lable>State</lable></td>                
                <td>
                    <?php echo form_input('state', $event->state, $col_width); ?>
                </td>
            </tr>
            <tr>
                <td><lable>Time</lable></td>                
                <td>
                    <?php echo form_input('time', $event->time, $col_width); ?>
                </td>
            </tr>
           
             <tr>
                <td><lable>Dress Code</lable></td>                
                <td><?php echo form_input('dress_code', $event->dress_code, $col_width); ?>
                </td>
            </tr>
            <tr>
                <td><lable>Entry Fee</lable></td>                
                <td>
                    <table>
                        <tr>
                            <td><?php echo form_input('entry_fee', $event->entry_fee, $col_width_small); ?></td>
                            <td style="font-size: 10px;">Leave blank <br/>If it's free of charge</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><lable>Contact Number</lable></td>                
                <td>
                    <table>
                        <tr>
                            <td>
                                 <?php 
                                    $data_contact_number = array(
                                    'name'        => 'contact_number',
                                    'id'          => 'contact_number',
                                    'value'       => '1',  
                                     'checked'     => $event->show_contact_number,
                                    'style'       => 'margin-right:10px'

                                    );                    
                                    echo  form_checkbox($data_contact_number);
                                ?>
                            </td>
                            <td style="font-size: 10px;">Tick to show your registered phone number</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><lable>Email</lable></td>                
                <td>
                    <table>
                        <tr>
                            <td>
                                 <?php 
                                    $data_email = array(
                                    'name'        => 'show_email',
                                    'id'          => 'show_email',
                                    'value'       => '1', 
                                    'checked'     => $event->show_email,    
                                    'style'       => 'margin-right:10px'

                                    );                    
                                    echo  form_checkbox($data_email);
                                ?>
                            </td>
                            <td style="font-size: 10px;">Tick to show your registered email address</td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td><lable>Website URL</lable></td>                
                <td>
                   <?php echo form_input('website_url', $event->website_url, $col_width); ?>
                </td>
            </tr>
        </table>
    </div>
    </div>
    <br/><br/>
    <div class="ui-widget-content">
         <table width="100%">
             <tr>
                 <td  width="200px"></td>
                 <td style="text-align: right">
                     <input type="hidden" name="id" value="<?=$event->id?>">
                   <?php echo form_submit(array('name' => 'Update','class' => 'button','style'=>'float:right'), 'Update'); ?></td>
             </tr>
         </table>
    </div>    
    <?php echo form_close(); ?>
</div>

<div id="EventSearch">    
<?php echo $template['partials']['event_current']; ?>
</div>
<script type="text/javascript">
         jQuery.noConflict();
	jQuery(document).ready( function() {
		jQuery("#guest_types,#category_id").multiSelect({
		  selectAll: false
		});
	});

</script>
<script language="javascript" type="text/javascript">
function limitText(limitField, limitcount, limitNum) {
   
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
            document.getElementById(limitcount).innerHTML = 'You have '+ (limitNum - limitField.value.length) + ' characters left';
	}
}
</script>