<div class="breadcrumb">
<breadcrumb>My Event Alerts</breadcrumb>
</div>

<h2>My Event Alerts</h2>

<div id="message">

<div id="msg-body">
<div id="messageLeft">
<?php echo $template['partials']['member_left_menu']; ?>
</div>

<div id="messageRight">

    <table cellspacing="0">

	<thead>

	    <th class="mail_view_header"></th>

	    <th class="mail_view_header">Alert</th>

	    <th class="mail_view_header">Date</th>

	</thead>

	<tbody>

	    <?php

	    if(!empty ($events)){
                $mail_threads = array();
	    foreach($events as $event){?>
                <?php 
                //read or unread
                $class = "unread_msg";
                if($event->read==1) $class ="read_msg";   
                ?>
            
            
	    <tr class="<?=$class?>">
		<td width="5%"></td>
                <?php $message = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $event->message); ;?>
		<td><a href="#" id="view-allert-<?=$event->id;?>"><?=$message;?></a></a>
                <div id="allert-<?=$event->id;?>" title="Event Alert!" style="display: none">
                    <?=$event->message;?>
                </div>
                    <script type="text/javascript">
                        $('#view-allert-<?=$event->id;?>').click(function(){
                            $( "#allert-<?=$event->id;?>" ).dialog("open"),
                            $.ajax({
                                        type: "POST",
                                        dataType:"json",
                                        url: "<?= site_url('event/alertRead') ?>",
                                        data: "id=<?=$event->id;?>",
                                        success: function(msg){
                                            $( "#allert-<?=$event->id;?>" ).dialog('open');                                          
                                        }
                                    });
                        });
        
                        $( "#allert-<?=$event->id;?>" ).dialog({
                            autoOpen: false,
                            resizable: false,
                            height: 200,
                            width: 350,
                            modal: true,
                            buttons: {
                                "Delete": function() {
                                    $.ajax({
                                        type: "POST",
                                        dataType:"json",
                                        url: "<?= site_url('event/delete_alert') ?>",
                                        data: "id=<?=$event->id;?>",
                                        success: function(msg){
                                            window.location='<?=site_url('event/alert'); ?>';
                                        }
                                    });
                                    window.location='<?= site_url('event/alert'); ?>';
                                },


                                Ok: function() {
                                   window.location='<?= site_url('event/alert'); ?>';
                                }
                            }

                        });
                    </script>
                </td>
		<td width="25%"><?=$event->date;?></td>
	    </tr>

	    <?php }            
             if($this->pagination->create_links() !=''):?>
            <tr><td colspan="3" align="center"><div class="pagination">     
            <?php echo $this->pagination->create_links(); ?>
            </div></td> </tr>
            <?php endif;?>
            
            <?php
	    }else{?>

	    <tr>

		<td colspan="3" align="center">

		    Empty

		</td>

	    </tr>

	    <?php } ?>

	</tbody>

    </table>
    <?php //$links ?>

</div>



</div>
</div>