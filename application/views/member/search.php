<h2>Search</h2>
<div class="message">
    <?php if(isset($message['success'])): ?>
        <div class="success">
            <?php echo $message['success'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['warn'])): ?>
        <div class="warn">
            <?php echo $message['warn'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['error'])): ?>
        <div class="error">
            <?php echo $message['error'];?>
        </div>
    <?php endif; ?>
</div>
<?php
    echo form_open('search/searchresults');
?>
<div id="search">
     <div class="ui-widget-content">

<?php
$col_width = 'style="width: 200px; left:200px;"';


$aged = array(
                '18' => '18',
                '20' => '20',
                '25' => '25',
                '30' => '30',
                '35' => '35',
                '40' => '40',
                '45' => '45',
                '50' => '50',
                '55' => '55',
                '60' => '60',
                '65' => '65',
                '70' => '70',
                '80' => '80',
                '90' => '90'
                );

$looking_for_options['']= 'Any';
$looking_for_options=array_merge ($looking_for_options,$looking_for);


?>

    <table>
    <tr>
        <td  width="200px"><label>I am looking for</label></td>
        <td><?php echo form_dropdown('looking_for', $looking_for_options,'',$col_width); ?></td>

    </tr>

    <tr>
        <td><label>Seeking</label></td>
        <td><?php echo form_dropdown('seeking', $looking_for_options,'',$col_width); ?></td>

    </tr>
    <tr>
        <td><label>Aged between</label></td>
         <td>
        <?php echo form_dropdown('aged1', $aged, 18, 'style="width: 50px"'); ?>
             &nbsp;&nbsp;&nbsp;&nbsp;and&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo form_dropdown('aged2', $aged, 50, 'style="width: 50px"'); ?>
        </td>
    </tr>
    <tr>
        <td><label>Location</label></td>
        <td><?php echo form_dropdown('location', $location_options,'New South Wales' ,$col_width); ?></td>
    </tr>
    <tr>
        <td><label>City or suburb</label></td>
        <td> <?php echo form_input('city_suburb', '',$col_width); ?>   </td>
    </tr>
     <tr>
        <td><label>Postcode</label></td>
        <td> <?php echo form_input('postcode', '',$col_width); ?>   </td>
    </tr>
    <tr>
        <td><label>Date Joined</label></td>
         <td>
             <?php
                $date_joined = array(
                                'Any time'    => 'Any time',
                                'Last week'   => 'Last week',
                                'Last month'  => 'Last month'
                );
                echo form_dropdown('date_joined', $date_joined,'Any time',$col_width);
            ?>
         </td>
    </tr>  
    <tr>
        <td colspan="2">
            <h4>Display preferences</h4>            
        </td>
    </tr>
<!--    <tr>
        <td><label>Show me members:</label></td>
        <td>
            <?php
            echo form_checkbox('who_are_online_now');
            ?>
            Who are online now
        </td>
    </tr>-->
    <tr>
        <td></td>
        <td>
            <?php
            echo form_checkbox('with_photos','1');
            ?>
            With photos
        </td>
    </tr>
<!--    <tr>
        <td></td>
        <td>
            <?php
            echo form_checkbox('who_are_verified');
            ?>
            Who are verified
        </td>
    </tr>-->
<!--    <tr>
        <td></td>
        <td>
             <?php
            echo form_checkbox('who_are_looking_for_me');
            ?>
            Who are looking for me
        </td>
    </tr>-->
    <tr>
        <td></td>
        <td></td>
    </tr>
       
</table>

    </div>
     <?php echo form_submit(array('name' => 'edit','class' => 'button','style'=>'margin-left:70px;float:right'), 'Submit'); ?>
</div>
<?php
echo form_close();
?>
