

<div id="photo-gallery">
<?php
$displayphoto['name'] = "";
$displayphoto['cat'] = "";
$displayphoto['desc'] = "";

$currentPhoto = '';

foreach ($photoCat as $pc)
{
  $category_ar[$pc->id] = $pc->name;            
}   
?>
<?php if($photos) {?>
    <div class="gallery">
        <?php
        $i = 1;
        foreach ($photos as $photo):?>
            <div class="thumbnails">
                    <img onclick="changePhotoInfo(<?=$i?>)" id="<?php echo "img".$i; ?>" src="<?php echo base_url().'uploads/'.$photo->photo; ?>" alt="Image Not Loaded"/>
                    <input type="hidden" id="<?php echo "imgid".$i; ?>" value="<?=$photo->pid?>" />
                    <input type="hidden" id="<?php echo "imgname".$i; ?>" value="<?=$photo->name?>" />
                    <input type="hidden" id="<?php echo "imgdesc".$i; ?>" value="<?=$photo->description?>" />
                    <input type="hidden" id="<?php echo "imgcat".$i; ?>" value="<?=$photo->cat_name?>" />
            </div>
            <?php
            $displayphoto['name'] =$photo->name;
            $displayphoto['cat'] = $photo->cat_name;
            $displayphoto['desc'] = $photo->description;
            $currentPhoto = $photo->photo;
            $i++;
        endforeach;?>
        <br/>
        <?php
         if(isset($photo_info))
         {
             $currentPhoto = $photo_info->photo;
             $displayphoto['name'] = $photo_info->name;
             $displayphoto['cat'] = $photo_info->cat_name;
             $displayphoto['desc'] = $photo_info->description;
         }
        ?>
        <div class="preview">
                <img id="preview" name="preview" src="<?php echo base_url().'uploads/'.$currentPhoto; ?>" alt="No Image Loaded"/>
        </div>
        <div class="information">
             <?php echo form_open_multipart('member/managePhotos');?>
            <?php echo form_submit(array('name' => 'delete','class' => 'btn small','style'=>'margin-left:10px;'), 'Delete'); ?>
            <?php echo form_submit(array('name' => 'edit','class' => 'btn small','style'=>'margin-left:10px;'), 'Edit'); ?>
            <input type="hidden" name="id" id="id" value="<?=$photo->pid?>" />
            <?php echo form_close(); ?>
        </div>
        <br/>
    </div>
<?php };?>
</div>

<div id="upload-info">
    <div id="upload-photo">
    <?php echo form_open_multipart('member/photos');?>
    <input type="file" name="userfile" size="50" /><br/><br/>
    Category:    <?php echo form_dropdown('category', $category_ar,'1',"style='width:150px'"); ?><br/><br/>     
    <input type="submit" value="upload" />
    <?php echo form_close(); ?>
    </div> 
    
    <div id="photo-info">
        <table>            
            <tr>                
                <td>Photo Name:</td>
                <td> <div id="photoname"><?=$displayphoto['name']?></div></td>
            </tr>
            <tr>
                <td>Category:</td>
                <td><div id="photocategory"><?=$displayphoto['cat']?></div></td>
            <tr>
            <tr>
                <td>Description:</td>
                <td><div id="photodescription"><?=$displayphoto['desc']?></div></td>                
            <tr>
            
         </table>
    </div>
    
    
    
    <?php if(isset($photo_info)) {?>
     <div id="photo-edit">   

        <?php echo form_open_multipart('member/updatePhotoInfo');?>
        <h3>Edit Photo Information</h3>
        <?php         
            
    
        $data = array(
              'name'        => 'description',
              'id'          => 'description',
              'value'       => $photo_info->description,
              'cols'   => '40',
              'rows'        => '5',              
            );

        ?>
          <table>
            
            <tr>
                
                <td>Photo Name:</td>
                <td><?php echo form_input('photo_name', $photo_info->name); ?>
                <input type="hidden" name="id" value="<?=$photo_info->photo_id?>" />
                </td>
            </tr>
            <tr>
                <td>Category:</td>
                <td><?php echo form_dropdown('category', $category_ar, $photo_info->category); ?></td>
            <tr>
            <tr>
                <td>Description:</td>
                <td><?php echo form_textarea($data); ?></td>
                <td rowspan="4">
                    <img src="<?php echo base_url().'uploads/'.$photo_info->photo; ?>" width="100px" height="100px"/>
                </td>
            <tr>
            
         </table>
         <?php echo form_submit(array('name' => 'edit','class' => 'btn small','style'=>'float:right; margin-right:140px;'), 'Submit'); ?>
        <?php echo form_close(); ?>
    </div>   
    <?php }   
        
    ?>
    
    


</div>

<script type="text/javascript">
    function changePhotoInfo(i)
    {
        document.getElementById('preview').src=document.getElementById('img'+i).src; 
        document.getElementById('id').value=document.getElementById('imgid'+i).value;
        document.getElementById('photoname').innerHTML=document.getElementById('imgname'+i).value;
        document.getElementById('photocategory').innerHTML=document.getElementById('imgcat'+i).value;
        document.getElementById('photodescription').innerHTML=document.getElementById('imgdesc'+i).value;
    }
    
</script>