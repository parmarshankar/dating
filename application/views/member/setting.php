<?php 
$userid = $this->session->userdata('id');
?>
<h2>Profile Settings</h2>
<div class="message">
    <?php if(isset($message['success'])): ?>
        <div class="success">
            <?php echo $message['success'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['warn'])): ?>
        <div class="warn">
            <?php echo $message['warn'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['error'])): ?>
        <div class="error">
            <?php echo $message['error'];?>
        </div>
    <?php endif; ?>
</div>


    <div class="profile-setting-left">
        <div class="ui-widget-content">
            <div class="ui-state-default hedding">My Profile</div>         
           
            <div class="Row">
                <a href="../profile?id=<?=$userid?>">View</a><br>
                View your existing profile.
            </div>
            <div class="Row">
                <a href="edit">Edit</a><br>
                Edit your existing profile.
            </div>
            <div class="Row">
                <a href="edit_age_location">Edit Age/Location</a><br>
                Edit your Age and Location details.
            </div>
            <div class="Row">
                <a href="editLookingFor">Looking For</a><br>
                Edit your "looking for" settings.
            </div>
            
            <div class="Row">
                <a href="editAutoReply">Auto-Reply</a><br>
                Edit, switch on/off your Auto-Reply message.
            </div>
            <div class="Row">
                <a href="rename">Rename</a><br>
                Change your username.
            </div>
                    
                    
        </div>
<!--        <br/>
        <div class="ui-widget-content">
            <div class="ui-state-default hedding">SMS Alerts & Messages</div>
        </div>-->
    </div>
    <div class="profile-setting-right">
<!--        <div class="ui-widget-content">
            <div class="ui-state-default hedding">My Membership</div>
        </div>
        <br/>-->
        <div class="ui-widget-content">
        <?php echo $template['partials']['profile_complete']; ?>
         </div>
        <br/>    
        <div class="ui-widget-content">
            <div class="ui-state-default hedding">Profile Preferences</div>
            <div class="Row">
                <a href="generalSetting">General Settings</a><br>
                Add contact details, and change other account settings.
            </div>           
            <div class="Row">
                <a href="changePassword">Change Password</a><br>
                Set your own password.
            </div>
<!--            <div class="Row">
                <a href="#">Change Email Address</a><br>
                This email address is for admin contact (not displayed on profile).
            </div>-->
            <div class="Row">
                <a href="removeProfile">Remove Profile</a><br>
                Permanently remove your profile.
            </div>
        </div>
    </div>

