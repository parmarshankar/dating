<div class="message">
    <?php if(isset($message['success'])): ?>
        <div class="success">
            <?php echo $message['success'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['warn'])): ?>
        <div class="warn">
            <?php echo $message['warn'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['error'])): ?>
        <div class="error">
            <?php echo $message['error'];?>
        </div>
    <?php endif; ?>
</div>


    <div id="container">
	<div id="content">
	    <div id="accountinfo">
		<p><b><?php 
                 $britharray = explode('-',$bday);
                $age = date('Y') - (int)$britharray[0];                 
                
                if(isset($usertypes[$sex]))
                echo $usertypes[$sex].' '; echo $age . " years" ?></b></p>
		
		<p><b>Location: </b><?php echo $location; ?></p>
                 <?php if(isset($mem_settings['telephone_no']) && $mem_settings['tp_no_visible']==1) :?>
                <p><b>Phone No.:</b><?=$mem_settings['telephone_no']?></p>
                <?php endif;?>		
                <p><b>Seeking:</b> <?php  if(is_array(unserialize($lookingFor['seeking']))) {
                    $lkfs = unserialize($lookingFor['seeking'] );
                    $lookingFordispay = array();
                    foreach ($lkfs as $lkf)
                    {
                       if(isset($usertypes[$lkf]))
                        $lookingFordispay[] = $usertypes[$lkf];
                    }
                     echo implode(' or ',$lookingFordispay); 
                }
                ?></p>
		<p><b>Who Are:</b> <?php echo $lookingFor['min_age']. ' - '. $lookingFor['max_age']?></p>

		<div id="tabs1">
		    <ul>
			<li><a href="#tabs-1">Physical</a></li>
			<li><a href="#tabs-2">Intimate</a></li>
			<li><a href="#tabs-3">Life Style</a></li>
		    </ul>
		    <div id="tabs-1">
			<li><b>Body type:</b><?php echo $body_type; ?></li>
			<li><b>Height:</b><?php echo $height; ?></li>
			<li><b>Race:</b><?php echo $race_background; ?></li>
			<li><b>Eye colour:</b><?php echo $eye_colour; ?></li>
			<li><b>Hair colour:</b><?php echo $hair_colour; ?></li>
			<li><b>Hair length:</b><?php echo $hair_length; ?></li>




		    </div>
		    <div id="tabs-2">
			<li><b>Participation:</b><?php echo $participation_level; ?></li>
			<li><b>Orientation:</b><?php echo $sexual_orientation; ?></li>
			<li><b>Safe Sex:</b><?php echo $safe_sex; ?></li>
			<li><b>Personality:</b><?php echo $personality; ?></li>
		    </div>
		    <div id="tabs-3">
			<li><b>Relationship:</b><?php echo $relationship_status; ?></li>
			<li><b>Star sign:</b><?php echo $sexual_orientation; ?></li>
			<li><b>Drinking:</b><?php echo $drinking; ?></li>
			<li><b>Smoking:</b><?php echo $smoking; ?></li>
		    </div>
		</div>

		<div id="tabs2">
		    <ul>
			<li><a href="#tabs-1">General Interest</a></li>
			<li><a href="#tabs-2">Sexual Interest</a></li>
			<li><a href="#tabs-3">Fetish Interest</a></li>
		    </ul>
		    <div id="tabs-1">
			<?php
			if (is_array(unserialize($general_interests)))
			    foreach (unserialize($general_interests) as $value)
			    {
				?>
				<li><?php echo $value; ?></li>
	<?php
    }
?>


		    </div>
		    <div id="tabs-2">
			<?php
			if (is_array(unserialize($sexual_interests)))
			    foreach (unserialize($sexual_interests) as $value)
			    {
				?>
				<li><?php echo $value; ?></li>
				<?php
			    }
			?>

		    </div>
		    <div id="tabs-3">
			<?php
			if (is_array(unserialize($fetish_interests)))
			    foreach (unserialize($fetish_interests) as $value)
			    {
				?>
				<li><?php echo $value; ?></li>
	<?php
    }
?>


		    </div>
		</div>
	    </div>
	</div><!-- #content-->
    </div><!-- #container-->

    <div class="sidebar" id="sideLeft">

<?php if ($photos)
{ ?>
    	<div id="photoarea">
    	    <img id="preview"  src="<?php echo base_url() . 'uploads/' . $photos[0]->photo; ?>" alt="No Image Loaded"/>
    	</div>

    <?php
    $i = 1;
    foreach ($photos as $photo):
	?>
		<div class="thumbnails">
		    <img onclick="<?php echo "document.getElementById('preview').src=document.getElementById('img" . $i . "').src"; ?>" id="<?php echo "img" . $i; ?>" src="<?php echo base_url() . 'uploads/' . $photo->photo; ?>" alt="Image Not Loaded"/>
		</div>

	<?php
	$i++;
    endforeach;
    ?>
    	<br/>
    	<br/>
 
<?php } else{ ?>
        <div id="photoarea">
                <img id="preview"  src="assets/images/no_avatar.jpg" alt="No Image Loaded"/>
        </div>    
    <?php   
} ?>

    
    </div><!-- .sidebar#sideLeft -->

    <div class="my-account" id="sideRight">
            
       
            <div id="Requests">
                <div class="Requests">
                    
                    <ul>
                        <li><a href="<?=site_url('message');?>"> ( <?=$unReadMsg->count;?> )  New Message(s) </a> </li>           
                        <li><a href="<?=site_url('flirter');?>"> ( <?=$unReadFlirter->count;?> )   New Flirt(s) </a></li>  
                        <li><a href="<?=site_url('event/alert');?>"> ( <?=$unReadEventAlert?> )   New Event Alert(s) </a></li>  
                     </ul>
                </div>
                <div class="Requests">
                    
                    <ul>
                        <li><a href="<?=site_url('lists/friends');?>"> ( <?=$unReadFriend->count;?> ) Friend Request(s) </a>   </li> 
                    </ul>
                </div>
             </div>
        <?php if(count($friends)> 0):?>    
        <div id="friends">             
            <h4>Friends</h4>
            <?php foreach($friends as $friend):?>
            <div class="thumbnails">
                <a href="<?php echo base_url().'profile?id='.$friend->id?>">
                <?php if($friend->photo != ''):?>
               <img  src="uploads/<?=$friend->photo?>" alt="" title="<?=$friend->firstname?>">
               <?php endif;?>
               <?php if($friend->photo == ''):?>
               <img src="assets/images/no_avatar.jpg" title="<?=$friend->firstname?>">
               <?php endif;?>
               </a>
           </div>
            <?php endforeach;?>
        </div>
       <?php endif;?>     
        
        <div id="photoInfo">
        <div class="Requests">
            <h2>Photos & Video</h2>
            <ul>
                <?php foreach($photosCount as $pc):?>
                <li><a href="<?=site_url('profile/photos/'.$pfid.'/'.$pc->id);?>"> ( <?=$pc->count?> )  in <?=$pc->name?> </a> </li>           
                <?php endforeach;?>  
             </ul>
        </div>        
       
   </div>
        
   
   <?php echo $template['partials']['profile_complete']; ?>
   
</div><!-- .sidebar#sideRight -->


<div class="push"></div>

<script type="text/javascript">
     jQuery.noConflict();
    jQuery(document).ready(function() {
	jQuery( "#tabs1" ).tabs();
	jQuery( "#tabs2" ).tabs();
    });
</script>