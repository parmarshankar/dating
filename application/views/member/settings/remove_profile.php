
<h2>Profile Settings - Remove Profile</h2>

<div id="edit_profile">
    
    <h3> Remove your Dating Account</h3>
    <hr/>
    
    <div class="ui-widget-content">        
        <?php  echo form_open('member/removeProfile');?>
        <div class="message">
            <div class="error">
                If you remove your profile you will no longer be able to use this site.<br/>
                Are you sure you want to delete your account?
            </div>        
        </div>
        
        <table width="100%">                        
            <tr>
                <td><?php echo anchor('member/settings', ' << Back to Profile Settings',array('class' => 'btn small')); ?></td>                
                <td></td>
            </tr>
        </table>
        <?php echo form_submit(array('name' => 'edit','class' => 'button','style'=>'margin-left:70px;float:right'), 'Delete Profile'); ?>
        <?php  echo form_close();?>
    </div>
</div>
