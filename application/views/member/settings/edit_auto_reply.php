
<h2>Profile Settings - Auto Reply</h2>

<div id="edit_profile">
    <div class="message">
    <?php if(isset($message['success'])): ?>
        <div class="success">
            <?php echo $message['success'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['warn'])): ?>
        <div class="warn">
            <?php echo $message['warn'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['error'])): ?>
        <div class="error">
            <?php echo $message['error'];?>
        </div>
    <?php endif; ?>
    </div>
    <h3>Your Auto Reply Message</h3>
    <hr/>
    
    <div class="ui-widget-content">        
        <?php  echo form_open('member/updateAutoReply');
         $col_width = 'style="width: 50px; left:200px;"';
         
        ?>
        <table width="100%">
            <tr>
                <td ><lable>Your Message</lable></td>
                <td>:</td>
                <td rowspan="2">
                    <?php
                    $data_auto_reply_text = array(
                      'name'        => 'auto_reply_text',
                      'id'          => 'auto_reply_text',
                      'value'       => $memeberSetting['auto_reply_text'],
                      'cols'   => '30',
                      'rows'        => '5',   
                      'onKeyDown'   => 'limitText(this.form.auto_reply_text,countdown,3500)',
                        '' =>'limitText(this.form.limitedtextarea,countdown,3500)'
                    );
                    echo form_textarea($data_auto_reply_text); ?>
                </td>
            </tr>
             <tr>
                <td style="font-size: 10px;">(Max. 3500 Characters) <br/><br/>
                No contact details allowed<br/>
                No HTML markup allowed</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="font-size: 10px;"><span id="countdown"></span> </td>                
            </tr>
            <tr>
                <td><lable>Auto-Reply On</lable></td>
                <td>:</td>
                <td>
                    <?php 
                    $data_auto_reply_on_off = array(
                        'name'        => 'auto_reply_on_off',
                        'id'          => 'auto_reply_on_off',
                        'value'       => '1',
                        'checked'     => $memeberSetting['auto_reply_on_off'],
                        'style'       => 'margin:10px'
                        
                        );                    
                    echo  form_checkbox($data_auto_reply_on_off);
                    
                    ?>
                </td>
            </tr>            
            <tr>
                <td><?php echo anchor('member/settings', ' << Back to Profile Settings',array('class' => 'btn small')); ?></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <?php echo form_submit(array('name' => 'edit','class' => 'button','style'=>'margin-left:70px;float:right'), 'Submit'); ?>
        <?php  echo form_close();?>
    </div>
</div>

<script language="javascript" type="text/javascript">
function limitText(limitField, limitcount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		document.getElementById('countdown').innerHTML = 'You have '+ (limitNum - limitField.value.length) + ' characters left';
	}
}
</script>
