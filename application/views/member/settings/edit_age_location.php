<?php
$maxday = (date('Y')-10).'-'.date('m-d');
$minday = (date('Y')-90).'-'.date('m-d');
$maxy = (date('Y')-10);
$miny = (date('Y')-90);
?>
<script>
        jQuery.noConflict();
	jQuery(function() {                 
		jQuery( "#bday" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: '<?=$maxday?>',
                    minDate: '<?=$minday?>',
                    yearRange: "<?=$miny?>:<?=$maxy?>",
                    changeMonth: true,
                    changeYear: true
                });
               
	});
</script>

<h2>Profile Settings - Edit Age / Location</h2>

<div id="edit_profile">
    <h3>I am </h3>
    <hr/>
    
    <div class="ui-widget-content">        
        <?php  echo form_open('member/update_age_location');
         $col_width = 'style="width: 200px; left:200px;"';
        $location_options = $locations;
        ?>
        <table>
            <tr>
                <td><lable>Gender</lable></td>
                <td>:</td>
                <td>   
                    <?php foreach($user_types as $key=>$user_types_option):?>
                        <input <?php echo set_radio('sex', $key); ?> value="<?=$key?>"  name="sex" class="txt-register_tutor_input_radio" type="radio"  <?php if($user['sex']==$key) echo 'checked="checked"';?>/> <span style="font-size: 14px;margin-right: 5px;"><?=$user_types_option?></span>
                        <br/>
                    <?php endforeach;?>
                   </td>
            </tr>
            <tr>
                <td><lable>My birthday</lable></td>
                <td>:</td>
                <td>
                    
                     <?php
//                    $bdayarray = explode('-', $user['bday']); 
//                    
//                    
//                   
//                    
//                    $cutoff = date('Y')-100;
//                    // current year
//                    $now = date('Y')-10;
//
//                    // build years menu
//                    echo '<select name="year" style="width: 55px;>' . PHP_EOL;
//                    for ($y=$now; $y>=$cutoff; $y--) {
//                         if($y==$bdayarray[0]){$yselect='selected="selected"';} else {$yselect=""; }
//                        echo '  <option value="'.$y.'"  '.$yselect.'>' . $y . '</option>' . PHP_EOL;
//                    }
//                    echo '</select>' . PHP_EOL;
//
//                    // build months menu
//                    echo '<select name="month" style="width: 100px;>' . PHP_EOL;
//                    for ($m=0; $m<=12; $m++) {
//                        if($m==$bdayarray[1]){$mselect='selected="selected"';} else {$mselect=""; }
//                        echo '  <option value="' . $m . '"  '.$mselect.' >' . date('F', mktime(0,0,0,$m,1,2000)) . '</option>';
//                    }
//                    echo '</select>';
//
//                    // build days menu
//                    echo '<select name="day" style="width: 45px;>' . PHP_EOL;
//                    for ($d=0; $d<=31; $d++) {
//                        if($d==$bdayarray[2]){$dselect='selected="selected"';} else {$dselect=""; }
//                        echo '  <option value="' . $d . '"  '.$dselect.'>' . $d . '</option>' . PHP_EOL;
//                    }
//                    echo '</select>' . PHP_EOL;
                    ?>
                    <input id="bday" name="bday" type="text" value="<?=$user['bday']?>" <?=$col_width?>>
                </td>
            </tr>
            <tr>
                <td><lable>State</lable></td>
                <td>:</td>
                <td>
                   <?php echo form_dropdown('location', $location_options, $user['location'],$col_width); ?>
                </td>
            </tr>
             <tr>
                <td><lable>City or suburb</lable></td>
                <td>:</td>
                <td>
                    <?php echo form_input('city_suburb', $user['city_suburb'],$col_width); ?>                 
                </td>
            </tr>
            <tr>
                <td><lable>Postcode</lable></td>
                <td>:</td>
                <td><?php echo form_input('postcode', $user['postcode'],$col_width); ?></td>
            </tr>
            <tr>
                <td><?php echo anchor('member/settings', ' << Back to Profile Settings',array('class' => 'btn small')); ?></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <?php echo form_submit(array('name' => 'edit','class' => 'button','style'=>'margin-left:70px;float:right'), 'Submit'); ?>
        <?php  echo form_close();?>
    </div>
</div>
