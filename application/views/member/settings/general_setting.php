
<h2>Profile Settings - General Settings</h2>

<div id="edit_profile">
    
    
          
        <?php  echo form_open('member/generalSetting');
         $col_width = 'style="width: 200px; left:200px;"';
         $small_col_width = 'style="width: 30px;"';
        ?>
        <div class="message">
        <?php if(isset($message['success'])): ?>
            <div class="success">
                <?php echo $message['success'];?>
            </div>
        <?php endif; ?>
        <?php if(isset($message['warn'])): ?>
            <div class="warn">
                <?php echo $message['warn'];?>
            </div>
        <?php endif; ?>
        <?php if(validation_errors()!='' || isset($message['error'])): ?>
            <div class="error">
                <?php echo $message['error'];?>
                <div class="form_errors">
                <?php echo validation_errors(); ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
        <h3>Number of Items Displayed</h3>
        <hr/>
    <div class="ui-widget-content">  
        <table width="100%">
            <tr>
                <td width="300px"><lable>Number of messages per page</lable></td>                
                <td><?php echo form_input('num_messages_pp', $memeberSetting['num_messages_pp'], $small_col_width); ?></td>
            </tr>
            <tr>
                <td><lable>Number of flirts per page</lable></td>                
                <td><?php echo form_input('num_flirts_pp', $memeberSetting['num_flirts_pp'], $small_col_width); ?></td>
            </tr>
            <tr>
                <td><lable>Number of member listings per page in searches</lable></td>                
                <td><?php echo form_input('num_searches_pp', $memeberSetting['num_searches_pp'], $small_col_width); ?></td>
            </tr>            
        </table>
    </div>
        <br/>
        
    <h3>Display Contact Details</h3>
        <hr/>
    <div class="ui-widget-content">
        
        <div class="message">
            <div class="error">
                <b>It is a crime to publish other people's personal contact details.</b><br/>
            </div>
        </div>
        <table width="100%">
            <tr>
                <td width="300px"><lable>Telephone Number:</lable></td>                
                <td><?php echo form_input('telephone_no', $memeberSetting['telephone_no'], $col_width); ?></td>
            </tr>
            <tr>
                <td></td>                
                <td>
                     <?php 
                    $data_tp_no_visible = array(
                        'name'        => 'tp_no_visible',
                        'id'          => 'tp_no_visible',
                        'value'       => '1',
                        'checked'     => $memeberSetting['tp_no_visible'],
                        'style'       => 'margin:10px'
                        
                        );                    
                    echo  form_checkbox($data_tp_no_visible);?>
                    Visible 
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                     <?php 
                    $data_tp_no_sms_only = array(
                        'name'        => 'tp_no_sms_only',
                        'id'          => 'tp_no_sms_only',
                        'value'       => '1',
                        'checked'     => $memeberSetting['tp_no_sms_only'],
                        'style'       => 'margin:10px'
                        
                        );                    
                    echo  form_checkbox($data_tp_no_sms_only);?>
                    SMS Only
                </td>
            </tr>
            
        </table>
    </div>
        <br/>
        
    <h3>Visibility & Privacy Settings</h3>
        <hr/>
    <div class="ui-widget-content">
        
        <table width="100%">
            <tr>
                <td width="300px"><lable>I want my profile visible for others to find</lable></td>                
                <td>
                     <?php 
                    $data_visible_to_others = array(
                        'name'        => 'visible_to_others',
                        'id'          => 'visible_to_others',
                        'value'       => '1',
                        'checked'     => $memeberSetting['visible_to_others'],
                        'style'       => 'margin:10px'
                        
                        );                    
                    echo  form_checkbox($data_visible_to_others);?>               
                </td>
            </tr>
            <tr>
                <td><lable>I want other members to know when I am online</lable></td>                
                <td>
                    <?php 
                    $data_visible_as_online = array(
                        'name'        => 'visible_as_online',
                        'id'          => 'visible_as_online',
                        'value'       => '1',
                        'checked'     => $memeberSetting['visible_as_online'],
                        'style'       => 'margin:10px'
                        
                        );                    
                    echo  form_checkbox($data_visible_as_online);?>                      
                </td>
            </tr>
            <tr>
                <td><lable>Do not let members know that I have viewed<br/> their profile</lable></td>                
                <td>
                     <?php 
                    $data_not_let_know_i_view = array(
                        'name'        => 'not_let_know_i_view',
                        'id'          => 'not_let_know_i_view',
                        'value'       => '1',
                        'checked'     => $memeberSetting['not_let_know_i_view'],
                        'style'       => 'margin:10px'
                        
                        );                    
                    echo  form_checkbox($data_not_let_know_i_view);?>   
                </td>
            </tr>            
        </table>
    </div>
        <br/>
     <h3>Change your mail message preferences</h3>
        <hr/>
    <div class="ui-widget-content">       
        <table width="100%">
            <tr>
                <td width="300px"><lable>I Don't accept new messages</lable></td>                
                <td>
                     <?php 
                    $data_not_accept_new_msg = array(
                        'name'        => 'not_accept_new_msg',
                        'id'          => 'not_accept_new_msg',
                        'value'       => '1',
                        'checked'     => $memeberSetting['not_accept_new_msg'],
                        'style'       => 'margin:10px'
                        
                        );                    
                    echo  form_checkbox($data_not_accept_new_msg);?>
                </td>
            </tr>            
            <tr>
                <td><?php echo anchor('member/settings', ' << Back to Profile Settings',array('class' => 'btn small')); ?></td>                
                <td></td>
            </tr>          
        </table>
        <?php echo form_submit(array('name' => 'edit','class' => 'button','style'=>'margin-left:70px;float:right'), 'Submit'); ?>
        <?php  echo form_close();?>
    </div>
</div>
