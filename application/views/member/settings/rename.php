
<h2>Profile Settings - Rename</h2>

<div id="edit_profile">
    
    <h3> Change your Username (Handle)</h3>
    <hr/>
    
    <div class="ui-widget-content">        
        <?php  echo form_open('member/rename');
         $col_width = 'style="width: 200px; left:200px;"';
         $colage_width = 'style="width: 30px;"';
         
       
        ?>
        <div class="message">
        <?php if(isset($message['success'])): ?>
            <div class="success">
                <?php echo $message['success'];?>
            </div>
        <?php endif; ?>
        <?php if(isset($message['warn'])): ?>
            <div class="warn">
                <?php echo $message['warn'];?>
            </div>
        <?php endif; ?>
        <?php if(validation_errors()!='' || isset($message['error'])): ?>
            <div class="error">
                <?php echo $message['error'];?>
                <div class="form_errors">
                <?php echo validation_errors(); ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
        
        <table width="100%">
            <tr>
                <td width="200px"><lable>New Username</lable></td>                
                <td><?php echo form_input('newusername', $user['username'], $col_width); ?></td>
            </tr>            
            <tr>
                <td><?php echo anchor('member/settings', ' << Back to Profile Settings',array('class' => 'btn small')); ?></td>                
                <td></td>
            </tr>
        </table>
        <?php echo form_submit(array('name' => 'edit','class' => 'button','style'=>'margin-left:70px;float:right'), 'Submit'); ?>
        <?php  echo form_close();?>
    </div>
</div>
