<link href="<?= asset_url(); ?>css/jquery.multiSelect.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?= asset_url(); ?>js/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="<?= asset_url(); ?>js/jquery.multiSelect.js"></script>

<h2>Profile Settings - Looking For</h2>

<div id="edit_profile">
    <div class="message">
    <?php if(isset($message['success'])): ?>
        <div class="success">
            <?php echo $message['success'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['warn'])): ?>
        <div class="warn">
            <?php echo $message['warn'];?>
        </div>
    <?php endif; ?>
    <?php if(isset($message['error'])): ?>
        <div class="error">
            <?php echo $message['error'];?>
        </div>
    <?php endif; ?>
    </div>
    <h3>I Would Like To Meet </h3>
    <hr/>
    
    <div class="ui-widget-content">        
        <?php  echo form_open('member/updateLookingFor');
         $col_width = 'style="width: 200px; left:200px;"';
         $colage_width = 'style="width: 30px;"';
         
        $location_options = $locations;        
        
         
        $rel_status = array(	
	'Single' => 'Single',
	'Married' => 'Married',
	'Divorced' => 'Divorced',
	'Separated' => 'Separated',
	'Attached' => 'Attached'
        );        
       
        ?>
        <table width="100%">
            <tr>
                <td width="200px"><lable>I would like to meet a</lable></td>
                <td>:</td>
                <td><?php echo form_multiselect('seeking[]', $user_types, unserialize($lookingfor['seeking']), 'id="seeking"'); ?></td>
            </tr>
            <tr>
                <td><lable>Who is aged between</lable></td>
                <td>:</td>
                <td> <?php echo form_input('min_age', $lookingfor['min_age'],$colage_width); ?> 
                    &nbsp;&nbsp;&nbsp;and&nbsp;&nbsp;&nbsp;
                     <?php echo form_input('max_age', $lookingfor['max_age'],$colage_width); ?> 
                </td>
            </tr>
             <tr>
                <td><lable>Whose Relationship Status is</lable></td>
                <td>:</td>
                <td><?php echo form_dropdown('staus',$rel_status , $lookingfor['staus'], $col_width); ?>  
                </td>
            </tr>
            <tr>
                <td><lable>Who lives in</lable></td>
                <td>:</td>
                <td>
                   <?php echo form_dropdown('location', $location_options, 0,$col_width); ?>
                </td>
            </tr>           
           
        </table>
        </div>
    
        <br/><br/>
        <h3>In My Own Words </h3>
        <hr/>
        <div class="ui-widget-content"> 
        
         <table width="100%">
              <tr>
                <td width="200px">I'm Looking To Meet</td>
                <td>:</td>
                <td>
                    <?php
                    $data_looking_to_meet = array(
                      'name'        => 'looking_to_meet',
                      'id'          => 'looking_to_meet',
                      'value'       => $profile['looking_to_meet'],
                      'cols'   => '30',
                      'rows'        => '5',              
                    );

                    echo form_textarea($data_looking_to_meet); ?>
                </td>
            </tr>
            <tr>
                <td><?php echo anchor('member/settings', ' << Back to Profile Settings',array('class' => 'btn small')); ?></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <?php echo form_submit(array('name' => 'edit','class' => 'button','style'=>'margin-left:70px;float:right'), 'Submit'); ?>
        <?php  echo form_close();?>
    </div>
</div>
<script type="text/javascript">

	$(document).ready( function() {

		$("#seeking").multiSelect({
		  selectAll: false,
                  width: 200
		});
	});

</script>