<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of user
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Oct 31, 2011 3:02:49 PM
 * @filesource user.php
 */
class User extends FrontControler
{

    public function __construct()
    {
	parent::__construct();
    }

    public function login()
    {
	$this->load->library('form_validation');
	$this->load->model('user_model', 'user');
        
	$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[6]|xss_clean');
	$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|md5');

	if ($this->form_validation->run() == FALSE)
	{        
               $location = 'user/login';    
                $this->template->build($location);            
            
             
	}
	else
	{
	    //validate credentials and redirect to memebers_area
	    $username = $this->input->post('username');
	    $password = $this->input->post('password');

	    $resul = $this->makeLogin($username, $password);

	    if ($resul == 'fail' || $resul == 'not_activated')
	    {
		$data = $this->form_validation->_error_array;
                if ($resul == 'fail')
                    $data['custom'] = "Entered username or password is incorrect.";
                if ( $resul == 'not_activated')
                    $data['custom'] = "Please check your mail inbox, and active your membership.";
		$this->form_validation->_error_array = $data;
		$this->template->build('user/login');
	    }            
	    elseif ($resul == 'success')
	    {
               
		redirect('member');
	    }
	}
    }

    private function makeLogin($username, $password)
    {
	$this->load->model('user_model', 'user');

	$query = $this->user->getUser($username);

	if ($query->num_rows() > 0)
	{
	    $row = $query->row_array();

	    if (trim($password) != trim($row['password']))
	    {
		return 'fail';
	    }
	    else
	    {
                if($row['activated']==1)
                {
                    unset($row['password']);

                    $this->session->set_userdata($row);
                    $this->session->set_userdata('logged', TRUE);
                    $this->user->increaseLoggedCount($row['id']); 

                    return 'success';
                }
                 else {
                    return 'not_activated'; 
                }
	    }
	}
	else
	{
	    return 'fail';
	}
    }

    public function register()
    {
	$this->load->library('form_validation');
	$this->load->model('user_model', 'user');
        $this->data['user_types'] = $this->user->getUserTypes();
        $this->data['locations'] = $this->user->getUserLocations();
	$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[6]|xss_clean|strtolower|callback_isExistingUsername');
	$this->form_validation->set_rules('firstname', 'First name', 'trim|required|alpha|xss_clean');
	$this->form_validation->set_rules('lastname', 'Last name', 'trim|required|alpha|xss_clean');
	$this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email|xss_clean');
	$this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
	$this->form_validation->set_rules('sex', 'I am / We are', 'trim|required|xss_clean');	
	$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[password_confirm]|md5');
	$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim');
        $this->form_validation->set_rules('bday', 'Birthday', 'trim|required');

	if ($this->form_validation->run() == TRUE)
	{
	    $post = $this->input->post();           
           
            
	    $this->user->addUser($post);

	    $encryptionKey = $this->config->item('encryption_key');
	    $key = md5($encryptionKey . $post['email']);

	    $active_url = 'user/verify/e/' . $this->input->post('email') . '/k/' . $key;
	    $active_link = anchor($active_url, base_url() . $active_url);

	    $this->load->library('email');
	    $this->email->from('no-reply@dating-site.com', 'Dating Site');
	    $this->email->to($post['email']);
	    $this->email->subject('E-mail Verification | Dating Site');
	    $this->data['active_url'] = $active_link;
	    $this->data['first_name'] = $post['firstname'];
	    $this->data['email'] = $post['email'];
            $this->data['bday'] = $post['bday'];
            $this->data['bday'] = $post['bday'];

	    $body = $this->load->view('email/_verify_email', $this->data, TRUE);

	    $this->email->message($body);
	    $email_rc = $this->email->send();
            
            

	    $this->template->build('user/register_success',$this->data);
	}
	else
	{
           
	    $this->template->build('user/register',$this->data);
	}
    }
    
    public function quickregister()
    {
	$this->load->library('form_validation');
	$this->load->model('user_model', 'user');
        $this->data['user_types'] = $this->user->getUserTypes();
        $this->data['locations'] = $this->user->getUserLocations();
	$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[6]|xss_clean|strtolower|callback_isExistingUsername');
	$this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email|xss_clean');
	$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[password_confirm]|md5');
	$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim');

	if ($this->form_validation->run() == TRUE)
	{
	    $post = $this->input->post();
           
            unset($post['Join']);            
	    $this->user->addUser($post);

	    $encryptionKey = $this->config->item('encryption_key');
	    $key = md5($encryptionKey . $post['email']);

	    $active_url = 'user/verify/e/' . $this->input->post('email') . '/k/' . $key;
	    $active_link = anchor($active_url, base_url() . $active_url);

	    $this->load->library('email');
	    $this->email->from('no-reply@dating-site.com', 'Dating Site');
	    $this->email->to($post['email']);
	    $this->email->subject('E-mail Verification | Dating Site');
	    $this->data['active_url'] = $active_link;
	    $this->data['first_name'] = $post['username'];
	    $this->data['email'] = $post['email'];
            

	    $body = $this->load->view('email/_verify_email', $this->data, TRUE);

	    $this->email->message($body);
	    $email_rc = $this->email->send();            
	    $this->template->build('user/register_success',$this->data);
	}
	else
	{
	    $this->template->build('user/register',$this->data);
	}
    }

    /**
     * Check whether username exists
     * @param type $username
     * @return type
     */
    function isExistingUsername($username)
    {
	$this->form_validation->set_message('isExistingUsername', 'This username already exists, please choose another one');
	return $this->user->check_exists_username($username);
    }

    public function verify()
    {
	$get = $this->uri->uri_to_assoc();
	$encryptionKey = $this->config->item('encryption_key');
         
	$key = md5($encryptionKey . $get['e']);
       
	if (trim($key) == trim($get['k']))
	{
	    $this->load->model('user_model', 'user');
	    $userRs = $this->user->getUserByEmail($get['e']);

	    $userrow = $userRs->row_array();

	    $this->data['email'] = $get['e'];

	    if(!empty ($userrow) && $userrow['activated'] ==0)
	    {
		$this->user->makeVerified($userrow['id']);
		$this->template->build('user/verify_success', $this->data);
	    }
	    else
	    {
		$this->template->build('user/verify_success', $this->data);
	    }
	}
	else
	{
	    $this->template->build('user/verify_error');
	}
    }

    public function logout()
    {
	$this->session->unset_userdata('logged');
	$this->session->unset_userdata('username');
	$this->session->unset_userdata('firstname');
	$this->session->unset_userdata('lastname');
	$this->session->unset_userdata('email');
	$this->session->unset_userdata('sex');
	$this->session->unset_userdata('age');
	$this->session->unset_userdata('id');
	redirect('home');
    }

}

/* End of file home.php */
/* Location: ./application/controllers/user.php */