<?php

include_once 'publicevent.php';

class Event extends Publicevent {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged')) {
            redirect('user/login');
        }
        $this->data['active_menu'] = 2;
        $this->template->set_partial('member_left_menu', 'partials/member_left_menu');
    }

    public function add() {
        $message = array();
        $this->load->model('User_model', 'user');
        $this->load->model('Event_model', 'event');
        $this->data['guest_types'] = $this->user->getUserTypes();
        $this->data['location'] = $this->user->getUserLocations();
        $this->data['category_id'] = $this->event->getEventCategoies();

        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('event_title', 'Name / Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('helddate', 'Date Held', 'trim|required|xss_clean');
            $this->form_validation->set_rules('aged1', 'Min Age ', 'trim|numeric|xss_clean');
            $this->form_validation->set_rules('aged2', 'Max Age ', 'trim|numeric|xss_clean');
            $this->form_validation->set_rules('event_description', 'Description ', 'trim|required|xss_clean');
            $this->form_validation->set_rules('max_attendess', 'Maximum attendees ', 'trim|numeric|xss_clean');
            $this->form_validation->set_rules('event_rules', 'Rules ', 'trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->template->build('event/add', $this->data);
            } else {
                $insert_data['event_added_by'] = $this->session->userdata('id');
                $insert_data['added_date'] = date('Y-m-d');
                $insert_data['category_id'] = serialize($this->input->post('category_id'));
                $insert_data['event_title'] = $this->input->post('event_title');
                $insert_data['held_date'] = $this->input->post('helddate');
                $insert_data['guest_types'] = serialize($this->input->post('guest_types'));
                $insert_data['aged1'] = $this->input->post('aged1');
                $insert_data['aged2'] = $this->input->post('aged2');
                $insert_data['event_description'] = $this->input->post('event_description');
                $insert_data['max_attendess'] = $this->input->post('max_attendess');
                $insert_data['accept_interest'] = $this->input->post('accept_interest');
                $insert_data['ticket_booking_url'] = $this->input->post('ticket_booking_url');
                $insert_data['event_rules'] = $this->input->post('event_rules');
                $insert_data['theme'] = $this->input->post('theme');
                $insert_data['state'] = $this->input->post('state');
                $insert_data['time'] = $this->input->post('time');
                $insert_data['location'] = $this->input->post('location');
                $insert_data['dress_code'] = $this->input->post('dress_code');
                $insert_data['entry_fee'] = $this->input->post('entry_fee');
                $insert_data['show_contact_number'] = $this->input->post('contact_number');
                $insert_data['show_email'] = $this->input->post('show_email');
                $insert_data['website_url'] = $this->input->post('website_url');

                if ($this->input->server('REQUEST_METHOD') === 'POST' && isset($_FILES['userfile'])) {
                    $config['upload_path'] = './uploads/events/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '1024';
                    $config['max_width'] = '2048';
                    $config['max_height'] = '768';
                    $config['encrypt_name'] = true;
                    $config['overwrite'] = FALSE;


                    $this->load->library('upload', $config);
                    $fle = $this->upload->data();
                    $this->upload->set_filename($config['upload_path'], $_FILES['userfile']['name']);
                    if (!$this->upload->do_upload()) {
                        $this->data['error'] = $this->upload->display_errors();

                        $this->template->build('event/add', $this->data);
                    } else {

                        $data = $this->upload->data();

                        //add record to DB                
                        $insert_data['banner_name'] = $data['file_name'];
                    }
                }
                $insert_id = $this->event->insert($insert_data);
                $this->sendEventAlert('NEW',$insert_id);
                $this->data['message']['success'] = 'Successfully added the event';
                $this->myevents();
            }
        } else {

            $this->data['message'] = $message;
            $this->template->build('event/add', $this->data);
        }
    }

    public function myevents() {
        $this->load->library('pagination');
        $this->load->model('Event_model', 'event');
        $config['base_url'] = site_url('event/myevents');
        $config['per_page'] = 5;
        $config['total_rows'] = $this->event->getMyEventsCount();
        $this->pagination->initialize($config);

        $limit = (int) $this->uri->segment(3);
        $events = $this->event->getMyEvents($config['per_page'], $limit);

        $this->data['events'] = $events;
        $this->template->build('event/index', $this->data);
        $this->template->build('event/my', $this->data);
    }

    public function edit() {
        if ($this->input->post('id'))
            $event_id = $this->input->post('id');
        else
            $event_id = (int) $this->uri->segment(3);

        $this->load->model('Event_model', 'event');
        $this->load->model('User_model', 'user');

        $this->data['guest_types'] = $this->user->getUserTypes();
        $this->data['location'] = $this->user->getUserLocations();
        $this->data['category_id'] = $this->event->getEventCategoies();
        $event = $this->event->getEvent($event_id);
        if (!isset($event->id) || !$event->id > 0) {
            $this->data['message']['success'] = 'Wrong access';
            ;
            $this->myevents();
        } else {


            $user->id = $event->event_added_by;
            $this->data['event'] = $event;

            if ($this->session->userdata('id') != $event->event_added_by) {
                $this->data['message']['error'] = 'Sorry you cannot edit this event';
                $this->view($event_id);
            } else {
                if ($this->input->post()) {
                    $this->load->library('form_validation');
                    $this->form_validation->set_rules('event_title', 'Name / Title', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('helddate', 'Date Held', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('aged1', 'Min Age ', 'trim|numeric|xss_clean');
                    $this->form_validation->set_rules('aged2', 'Max Age ', 'trim|numeric|xss_clean');
                    $this->form_validation->set_rules('event_description', 'Description ', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('max_attendess', 'Maximum attendees ', 'trim|numeric|xss_clean');
                    $this->form_validation->set_rules('event_rules', 'Rules ', 'trim|xss_clean');

                    if ($this->form_validation->run() == FALSE) {
                        $this->template->build('event/edit', $this->data);
                    } else {
                        $insert_data['id'] = $event_id;
                        $insert_data['event_added_by'] = $this->session->userdata('id');
                        $insert_data['added_date'] = date('Y-m-d');
                        $insert_data['category_id'] = serialize($this->input->post('category_id'));
                        $insert_data['event_title'] = $this->input->post('event_title');
                        $insert_data['held_date'] = $this->input->post('helddate');
                        $insert_data['guest_types'] = serialize($this->input->post('guest_types'));
                        $insert_data['aged1'] = $this->input->post('aged1');
                        $insert_data['aged2'] = $this->input->post('aged2');
                        $insert_data['event_description'] = $this->input->post('event_description');
                        $insert_data['max_attendess'] = $this->input->post('max_attendess');
                        $insert_data['accept_interest'] = $this->input->post('accept_interest');
                        $insert_data['ticket_booking_url'] = $this->input->post('ticket_booking_url');
                        $insert_data['event_rules'] = $this->input->post('event_rules');
                        $insert_data['theme'] = $this->input->post('theme');
                        $insert_data['state'] = $this->input->post('state');
                        $insert_data['time'] = $this->input->post('time');
                        $insert_data['location'] = $this->input->post('location');
                        $insert_data['dress_code'] = $this->input->post('dress_code');
                        $insert_data['entry_fee'] = $this->input->post('entry_fee');
                        $insert_data['show_contact_number'] = $this->input->post('contact_number');
                        $insert_data['show_email'] = $this->input->post('show_email');
                        $insert_data['website_url'] = $this->input->post('website_url');
                        if ($this->input->server('REQUEST_METHOD') === 'POST' && isset($_FILES['userfile'])) {
                            $config['upload_path'] = './uploads/events/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '1024';
                            $config['max_width'] = '2048';
                            $config['max_height'] = '768';
                            $config['encrypt_name'] = true;
                            $config['overwrite'] = FALSE;


                            $this->load->library('upload', $config);
                            $fle = $this->upload->data();
                            $this->upload->set_filename($config['upload_path'], $_FILES['userfile']['name']);
                            if (!$this->upload->do_upload()) {
                                $this->data['error'] = $this->upload->display_errors();

                                $this->template->build('event/add', $this->data);
                            } else {

                                $data = $this->upload->data();
                                //add record to DB                
                                $insert_data['banner_name'] = $data['file_name'];
                            }
                        }
                        $this->event->update($insert_data);
                        $this->sendEventAlert('EDIT',$event_id);
                        $this->data['message']['success'] = 'Successfully updated the event';
                        $this->myevents();
                    }
                } else {

                    $view_by = $this->session->userdata('id');
                    $this->template->build('event/edit', $this->data);
                }
            }
        }
    }
    
    public function delete()
    {
        if ($this->input->post('id'))
            $event_id = $this->input->post('id');
        else
            $event_id = (int) $this->uri->segment(3);
        
        $this->load->model('Event_model', 'event');
        $this->load->model('User_model', 'user');
        $event= $this->event->getEvent($event_id);
        $this->data['matched'] =0;        
       
        if ($this->session->userdata('id') != $event->event_added_by) {
                $this->data['message']['error'] = 'Sorry you cannot edit this event';
                $this->view($event_id);
        } else {
            
             if ($this->input->post()) {
                 
                  $this->event->deleteEvent($event_id);
                  $this->sendEventAlert('DELETE',$event_id);
                  $this->data['message']['success'] = 'Successfully Deleted';
                  $this->myevents();
             }
             else{
                $user->id = $event->event_added_by;    
                $users=  $this->user->getUserByUID($user->id);
                $users = $this->refineUsersWithPhoto($users);

                $this->data['event'] = $event;
                $this->data['from'] ='publicevent';
                $this->data['event_added_by'] =$users[0];
                $this->data['guestCount']= $this->event->getGuestCount($event_id);

                $view_by = $this->session->userdata('id');
                if($view_by>0)
                {
                    $view_by=  $this->user->getUserByUID($view_by);
                    $this->data['view_by'] =$view_by[0];
                    $this->isMatched($event,$view_by[0]);       
                }
               $this->template->build('event/delete', $this->data);
             }
       }
    }
    
    function sendEventAlert($code,$event_id)
    {
         $this->load->model('Event_model', 'event');
         
        if($code == 'EDIT' || $code== 'DELETE')
           $users = $this->event->getGuests($event_id);
        else 
           $users = $this->event->getSuitableUsers($event_id);
       
        $template = $this->event->getEventAlertTemplate($code);
        
        $event= $this->event->getEvent($event_id);
        
        //create allert message
        $link = '<a href="'.site_url('publicevent/view/'.$event->id).'" target="_blank">'.$event->event_title.'</a>';
        $name = $event->event_title;
        
        
        $phrase  = $template[0]->template;
        $healthy = array("[EVENT_NAME]", "[EVENT_LINK]");
        $yummy   = array($name, $link);
        $newphrase = str_replace($healthy, $yummy, $phrase);
        
        $insert_data['event_id']=$event->id;
        $insert_data['message']=$newphrase;
        $insert_data['date']=date('Y-m-d');
        
        foreach($users as $user)
        {
            $insert_data['user_id']=$user->id;
            $this->event->sendEventAlert($insert_data);
        }
        
    }
    
    public function alert()
    {
         $this->load->library('pagination');
         $this->load->model('Event_model', 'event');
         $user_id = $this->session->userdata('id');
         
        $config['base_url'] = site_url('event/alert');
        $config['per_page'] = 10;        
        $config['total_rows'] = $this->event->getAllertCount($user_id);
        $this->pagination->initialize($config);
        $limit = (int) $this->uri->segment(3);
        
        $events = $this->event->getAllerts($user_id,$config['per_page'], $limit);
        $this->data['events'] = $events;
        $this->template->build('event/alert', $this->data);
    }
    
    public function alertRead()
    {
        $alertid = $this->input->post('id');
        $this->load->model('Event_model', 'event');
        $this->event->updateRead($alertid);
        echo "true";
        return true;
    }
    
    public function delete_alert()
    {
        $alertid = $this->input->post('id');
        $this->load->model('Event_model', 'event');
        $this->event->deleteAlert($alertid);
        echo "true";
        return true;
    }
    

}

?>