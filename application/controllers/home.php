<?php
/**
 * Description of index
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Oct 31, 2011 2:43:46 PM
 * @filesource home.php
 */
class Home extends FrontControler
{
    public function __construct()
    {
	parent::__construct();
        if ($this->session->userdata('logged')) {
            $this->load->model('Members_model', 'member');
            $this->load->model('User_model', 'user');
            $this->load->model('messages_model', 'message');
            $this->load->model('Flirter_model', 'flirter');
            $this->load->model('Lists_model', 'lists');
            $this->load->model('Event_model', 'event');
            
            $userid = $this->session->userdata('id');
        
            $this->data['profile'] = $this->member->GetProfileByUserId($userid);

            $this->data['lookingFor'] = $this->member->GetLookingForByUserId($userid);

            //$zodiac_sign =  $user = $this->User_model->GetSign($user[''])
            $this->data['photos'] = $this->member->get_photos_by_id($userid);
            //get photos info

            $this->data['unReadMsg'] = $this->message->getUnReadCount($userid);
            $this->data['unReadFlirter'] = $this->flirter->getUnReadCount($userid);
            $this->data['unReadFriend'] = $this->lists->getUnReadFriendRequestCount($userid);
            $this->data['unReadEventAlert'] = $this->event->getUnReadAllertCount($userid);
            
            $this->data['friends'] =$this->refineUsersWithPhoto($this->lists->getFriendsByOrder($this->session->userdata('id')));
            
            
            $this->data['location_options'] = $this->user->getUserLocations(1);
            $this->data['user_types'] = $this->user->getUserTypes();
            
            $this->template->set_partial('member_data', 'partials/member_data');
            $this->template->set_partial('profile_complete', 'partials/profile/profile_complete');

            $this->data['profile_complete'] = $this->member->getProfileComplete($this->session->userdata('id'));
        } 
    }
    
    public function index()
    {
        $this->load->model('User_model', 'user');
        $this->load->model('Lists_model', 'lists');
        $this->data['location_options'] = $this->user->getUserLocations(1);
        $this->data['user_types'] = $this->user->getUserTypes();
        $this->data['getAllUserCount'] = number_format($this->user->getAllUserCount());
        $this->data['hot'] = $this->refineUsersWithPhoto($this->lists->getHotModels());
	$this->template->build('home',$this->data);
    }
}

?>
