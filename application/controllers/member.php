<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of member
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Nov 3, 2011 7:02:48 PM
 * @filesource member.php
 */
class Member extends FrontControler {

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('logged')) {
            redirect('user/login');
        } else {
            $this->template->set_partial('member_header', 'partials/member_header');
            $this->template->set_partial('profile_complete', 'partials/profile/profile_complete');
            $this->load->model('Members_model', 'member');
            $this->data['mem_settings'] = $this->member->GetMemberSettingByUserId($this->session->userdata('id'));
            $this->data['profile_complete'] = $this->member->getProfileComplete($this->session->userdata('id'));
        }
    }

    public function index() {
        $this->load->model('Members_model', 'member');
        $this->load->model('User_model', 'user');
        $this->load->model('messages_model', 'message');
        $this->load->model('Flirter_model', 'flirter');
        $this->load->model('Lists_model', 'lists');
        $this->load->model('Photo_model', 'photo');
        $this->load->model('Event_model', 'event');
        
        $userid = $this->session->userdata('id');

        //select user data by userid
        $user = $this->user->getUserByID($userid);

        //check the user profile record is existis
        if ($this->member->record_exists($userid)) {

            //select profile data by userid
            $profile = $this->member->GetProfileByUserId($userid);

            $lookingFor['lookingFor'] = $this->member->GetLookingForByUserId($userid);
            $lookingFor['usertypes'] =$this->user->getUserTypes();
            //$zodiac_sign =  $user = $this->User_model->GetSign($user[''])
            $photos['photos'] = $this->photo->getPublicPhotosByUser($userid);
            $photos['photosCount'] = $this->photo->getPhotosCountByUser($userid);
            $photos['pfid'] = $userid;
            //get photos info

            $unReadCount['unReadMsg'] = $this->message->getUnReadCount($userid);
            $unReadCount['unReadFlirter'] = $this->flirter->getUnReadCount($userid);
            $unReadCount['unReadFriend'] = $this->lists->getUnReadFriendRequestCount($userid);
            $unReadCount['unReadEventAlert'] = $this->event->getUnReadAllertCount($userid);

            
            $friends['friends'] =$this->refineUsersWithPhoto($this->lists->getFriendsByOrder($this->session->userdata('id')));
             
            $view_data = array_merge($user, $profile, $photos, $unReadCount, $lookingFor,$friends,$this->data);


            $this->template->build('member/index', $view_data);
        } else {
            $insert_data = array(
                'userid' => $userid
            );
            $this->member->InsertMembersProfile($insert_data);
            $this->member->InsertMembersLookingFor($insert_data);
            //select profile data by userid
            $profile = $this->member->GetProfileByUserId($userid);
            $lookingFor['lookingFor'] = $this->member->GetLookingForByUserId($userid);
            $photos['photos'] = $this->member->get_photos_by_id($userid);
            $message['message']['success'] = 'Thank you for joining, First start with completeing your profile';
            $view_data = array_merge($user, $profile, $photos, $message, $lookingFor);
            $this->template->build('member/edit', $view_data);
        }
    }

    public function edit() {
        $this->load->model('Members_model', 'member');
        $userid = $this->session->userdata('id');
        $message = array();
        // if form was posted - save data in DB
        if ($this->input->post()) {

            $update_data = array(
                'userid' => $userid,
                'relationship_status' => $this->input->post('rel_status'),
                'participation_level' => $this->input->post('part_level'),
                'sexual_orientation' => $this->input->post('sex_orienatation'),
                'race_background' => $this->input->post('race'),
                'personality' => $this->input->post('personality'),
                'my_looks' => $this->input->post('my_looks'),
                'body_type' => $this->input->post('body_type'),
                'height' => $this->input->post('height'),
                'eye_colour' => $this->input->post('eye_colour'),
                'hair_colour' => $this->input->post('hair_colour'),
                'hair_length' => $this->input->post('hair_length'),
                'drinking' => $this->input->post('drinking'),
                'smoking' => $this->input->post('smoking'),
                'general_interests' => serialize($this->input->post('general_interests')),
                'safe_sex' => $this->input->post('safe_sex'),
                'body_hair' => $this->input->post('body_hair'),
                'public_hair' => $this->input->post('public_hair'),
                'endowment_length' => $this->input->post('endowment_length'),
                'endowment_thickness' => $this->input->post('endowment_thickness'),
                'intimate_piercings' => $this->input->post('intimate_piercings'),
                'sexual_personality' => $this->input->post('sexual_personality'),
                'sexual_interests' => serialize($this->input->post('sexual_interests')),
                'sexual_preferences' => serialize($this->input->post('sexual_preferences')),
                'fetish_interests' => serialize($this->input->post('fetish_interests')),
                'ideal_sexy_location' => $this->input->post('ideal_sexy_location'),
                'sexy_must_haves' => $this->input->post('must_have'),
                'favourite_activity' => $this->input->post('favourite_activity'),
                'profile_title' => $this->input->post('profile_title'),
                'about_me' => $this->input->post('about_me'),
                'looking_to_meet' => $this->input->post('looking_to_meet')
            );
            
           

            $this->member->UpdateMembersProfile($update_data);
            $message['message']['success'] = 'Successfully updated your existing profile data';
            $this->template->build('member/setting', $message);
        } else {
            //select profile data by userid
            $profile = $this->member->GetProfileByUserId($userid);
            $view_data = array_merge($profile, $message);
            $this->template->build('member/edit', $view_data);
        }
    }

    public function photos() {
        $this->load->model('Members_model', 'member');
        $this->load->model('Photo_model', 'photo');

        $this->data['photos'] = $this->photo->getPhotosByUser($this->session->userdata('id'));
        $this->data['photoCat'] = $this->photo->getPhotoCategorys();
        $this->data['error'] = '';

        if ($this->input->server('REQUEST_METHOD') === 'POST' && isset($_FILES['userfile'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
//            $config['max_size'] = '1024';
//            $config['max_width'] = '2048';
//            $config['max_height'] = '768';
            $config['encrypt_name'] = true;
            $config['overwrite'] = FALSE;


            $this->load->library('upload', $config);
            $fle = $this->upload->data();
            $this->upload->set_filename($config['upload_path'], $_FILES['userfile']['name']);
            if (!$this->upload->do_upload()) {
                $this->data['message']['error'] = $this->upload->display_errors(); 
                $this->template->build('member/photos', $this->data);
            } else {

                $data = $this->upload->data();

                //add record to DB
                $photo['userid'] = $this->session->userdata('id');
                $photo['photo'] = $data['file_name'];
                $this->member->insert_photo($photo);
                $re= $this->member->getPhotoIDbyName($photo['photo']);
                
                $photoInfo['photo_id']  = $re['id'];
                $photoInfo['category'] = $this->input->post('category');
                $this->photo->insertInfo($photoInfo);               

                redirect('member/photos');
            }
        } else {
         
            $this->template->build('member/photos', $this->data);
        }
    }

    function managePhotos() {
        $post = $this->input->post();
        if (isset($post['delete'])) {
            $this->deletePhotos();
        } else {
            $this->editPhotos();
        }
    }

    function editPhotos() {
        $post = $this->input->post();
        $id = $post['id'];
        $this->load->model('Members_model', 'member');
        $this->load->model('Photo_model', 'photo');

        $this->data['photos'] = $this->photo->getPhotosByUser($this->session->userdata('id'));

        if (!$this->photo->record_exists($id)) {
            $insert_data = array(
                'photo_id' => $id
            );
            $this->photo->insertInfo($insert_data);
        }
        $this->data['photoCat'] = $this->photo->getPhotoCategorys();
        $this->data['photo_info'] = $this->photo->getPhotoInfo($id);
        $this->template->build('member/photos', $this->data);
    }

    function updatePhotoInfo() {
        $this->load->model('Photo_model', 'photo');
        $post = $this->input->post();
        $this->photo->updateCategory($post['id'], $post['category']);
        $this->photo->updateName($post['id'], $post['photo_name']);
        $this->photo->updateDescription($post['id'], $post['description']);
        $this->photos();
    }

    function deletePhotos() {
        $this->load->model('Members_model', 'member');
        $this->load->model('Photo_model', 'photo');

        $this->load->helper('file');
        $post = $this->input->post();

        $photo = $this->member->getPhoto($post['id']);

        if (read_file('./uploads/' . $photo[0]->photo))
            $resonse = unlink('./uploads/' . $photo[0]->photo);

        $this->member->deletePhoto($post['id']);
        $this->photo->deletePhotoInfo($post['id']);

        $this->photos();
    }

    function upload_photo() {

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1024';
        $config['max_width'] = '2048';
        $config['max_height'] = '768';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('view_my_photos', $error);
        } else {

            $data = $this->upload->data();

            //add record to DB
            $photo['userid'] = $this->session->userdata('userid');
            $photo['photo'] = $data['file_name'];
            $this->Members_model->insert_photo($photo);

            $this->view_data['photos'] = $this->Members_model->get_photos_by_id($this->session->userdata('userid'));

            $this->view_data['error'] = '';
            $this->load->view('view_my_photos', $this->view_data);
        }
    }

   
    public function settings() {
    
        $this->template->build('member/setting');
    }

    public function edit_age_location() {
        $this->load->model('User_model', 'user');
        $userid = $this->session->userdata('id');
        $this->data['user'] = $this->user->getUserByID($userid);
        $this->data['locations'] = $this->user->getUserLocations();
        $this->data['user_types'] = $this->user->getUserTypes();
        $this->template->build('member/settings/edit_age_location', $this->data);
    }

    public function update_age_location() {
        $this->load->model('User_model', 'user');


        if ($this->input->post()) {

            $update_data = array(
                'sex' => $this->input->post('sex'),
                'bday' => $this->input->post('bday'),
                'postcode' => $this->input->post('postcode'),
                'city_suburb' => $this->input->post('city_suburb'),
                'location' => $this->input->post('location')
            );


            $this->user->UpdateUser($update_data);
            $message['success'] = 'Successfully updated age/location';
            $this->data['message'] = $message;
        }
        $this->template->build('member/setting', $this->data);
    }

    public function editLookingFor() {
        $this->load->model('Members_model', 'member');
         $this->load->model('User_model', 'user');
        $userid = $this->session->userdata('id');

        $insert_data = array(
            'userid' => $userid
        );

        $this->data['profile'] = $this->member->GetProfileByUserId($userid);

        if (count($this->member->GetLookingForByUserId($userid)) == 0)
            $this->member->InsertMembersLookingFor($insert_data);

        $this->data['lookingfor'] = $this->member->GetLookingForByUserId($userid);
        $this->data['locations'] = $this->user->getUserLocations();
        $this->data['user_types'] = $this->user->getUserTypes();
        $message['success'] = 'We can match you to other members who share your interests.';
        $this->data['message'] = $message;
        $this->template->build('member/settings/edit_looking_for', $this->data);
    }

    public function updateLookingFor() {
        $this->load->model('Members_model', 'member');
        $userid = $this->session->userdata('id');
        if ($this->input->post()) {

            $updateDataLookingFor = array(
                'userid' => $userid,
                'seeking' => serialize($this->input->post('seeking')),
                'min_age' => $this->input->post('min_age'),
                'max_age' => $this->input->post('max_age'),
                'staus' => $this->input->post('staus'),
                'location' => $this->input->post('location')
            );

            $updateDataMember = array(
                'userid' => $userid,
                'looking_to_meet' => $this->input->post('looking_to_meet')
            );


            $this->member->UpdateMembersLookingFor($updateDataLookingFor);
            $this->member->UpdateMembersProfile($updateDataMember);

            $message['success'] = 'Successfully updated looking for settings';
            $this->data['message'] = $message;
        }
        $this->template->build('member/setting', $this->data);
    }

    public function editAutoReply() {
        $this->load->model('Members_model', 'member');
        $userid = $this->session->userdata('id');

        $insert_data = array(
            'userid' => $userid
        );

        if (count($this->member->GetMemberSettingByUserId($userid)) == 0)
            $this->member->InsertMembersSetting($insert_data);

        $this->data['memeberSetting'] = $this->member->GetMemberSettingByUserId($userid);

        $message['success'] = 'This message will be sent to anyone who mails you through this site.';
        $this->data['message'] = $message;
        $this->template->build('member/settings/edit_auto_reply', $this->data);
    }

    public function updateAutoReply() {
        $this->load->model('Members_model', 'member');
        $userid = $this->session->userdata('id');
        if ($this->input->post()) {

            $updateData = array(
                'userid' => $userid,
                'auto_reply_text' => $this->input->post('auto_reply_text'),
                'auto_reply_on_off' => $this->input->post('auto_reply_on_off'),
            );

            $this->member->UpdateMembersSetting($updateData);

            $message['success'] = 'Successfully updated "Auto Reply" settings';
            $this->data['message'] = $message;
        }
        $this->template->build('member/setting', $this->data);
    }

    public function rename() {
        $this->load->library('form_validation');
        $this->load->model('User_model', 'user');
        $userid = $this->session->userdata('id');
        $this->data['user'] = $this->user->getUserByID($userid);
        $message = array();

        if ($this->input->post()) {
            $this->form_validation->set_rules('newusername', 'Username', 'trim|required|alpha_numeric|min_length[6]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $message['error'] = '';
                $this->data['message'] = $message;
                $this->template->build('member/settings/rename', $this->data);
            } else {
                $isUserExistes = $this->user->isUserNameAvailble($this->input->post('newusername'));
                if (count($isUserExistes) == 0) {
                    $updateData = array(
                        'username' => $this->input->post('newusername')
                    );

                    $this->user->UpdateUser($updateData);
                    $message['success'] = 'Successfully updated new username';
                    $this->data['message'] = $message;
                    $this->template->build('member/setting', $this->data);
                } else {
                    $message['error'] = '"' . $this->input->post('newusername') . '" is already exists, choose another one';
                    $this->data['message'] = $message;
                    $this->template->build('member/settings/rename', $this->data);
                }
            }
        } else {
            $this->template->build('member/settings/rename', $this->data);
        }
    }

    public function generalSetting() {
        $this->load->library('form_validation');
        $this->load->model('User_model', 'user');
        $this->load->model('Members_model', 'member');

        $userid = $this->session->userdata('id');
        $this->data['user'] = $this->user->getUserByID($userid);
        $this->data['memeberSetting'] = $this->member->GetMemberSettingByUserId($userid);
        $message = array();

        if ($this->input->post()) {
            $this->form_validation->set_rules('num_messages_pp', 'Number of messages', 'trim|required|numeric|xss_clean');
            $this->form_validation->set_rules('num_flirts_pp', 'Number of flirts', 'trim|required|numeric|xss_clean');
            $this->form_validation->set_rules('num_searches_pp', 'Number of listings per search', 'trim|required|numeric|xss_clean');
            $this->form_validation->set_rules('telephone_no', 'Telephone No', 'trim|alpha_numeric|min_length[4]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $message['error'] = '';
                $this->data['message'] = $message;
                $this->template->build('member/settings/general_setting', $this->data);
            } else {
                $updateData = array(
                    'userid' => $userid,
                    'num_messages_pp' => $this->input->post('num_messages_pp'),
                    'num_flirts_pp' => $this->input->post('num_flirts_pp'),
                    'num_searches_pp' => $this->input->post('num_searches_pp'),
                    'telephone_no' => $this->input->post('telephone_no'),
                    'tp_no_visible' => $this->input->post('tp_no_visible'),
                    'tp_no_sms_only' => $this->input->post('tp_no_sms_only'),
                    'visible_to_others' => $this->input->post('visible_to_others'),
                    'visible_as_online' => $this->input->post('visible_as_online'),
                    'not_let_know_i_view' => $this->input->post('not_let_know_i_view'),
                    'not_accept_new_msg' => $this->input->post('not_accept_new_msg')
                );

                $this->member->UpdateMembersSetting($updateData);
                $message['success'] = 'Successfully updated General settings';
                $this->data['message'] = $message;
                $this->template->build('member/setting', $this->data);
            }
        } else {
            $insert_data = array(
                'userid' => $userid
            );

            if (count($this->member->GetMemberSettingByUserId($userid)) == 0)
                $this->member->InsertMembersSetting($insert_data);

            $this->data['memeberSetting'] = $this->member->GetMemberSettingByUserId($userid);

            $this->template->build('member/settings/general_setting', $this->data);
        }
    }

    public function changePassword() {
        $this->load->library('form_validation');
        $this->load->model('User_model', 'user');

        $userid = $this->session->userdata('id');
        $this->data['user'] = $this->user->getUserByID($userid);

        if ($this->input->post()) {
            $this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required|md5');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[password_confirm]|md5');
            $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim');

            if ($this->form_validation->run() == TRUE) {
                
                $oldPass = $this->input->post('oldpassword');
                if($this->user->validateUser($userid,$oldPass))
                {

                    $updateData = array(
                        'password' => $this->input->post('password')
                    );

                    $this->user->UpdateUser($updateData);
                    $message['success'] = 'Successfully updated new password';
                    $this->data['message'] = $message;
                    $this->template->build('member/setting', $this->data);
                }
                 else {
                      $message['error'] = 'Not Updated, Old Password is wrong';
                      $this->data['message'] = $message;
                      $this->template->build('member/settings/change_password', $this->data);
                }
            } else {
                $message['error'] = '';
                $this->data['message'] = $message;
                $this->template->build('member/settings/change_password', $this->data);
            }
        } else {
            $this->template->build('member/settings/change_password', $this->data);
        }
    }

    public function removeProfile() {
        $this->load->model('User_model', 'user');
        $userid = $this->session->userdata('id');
        $this->load->model('Members_model', 'member');

        if ($this->input->post()) {
            $updateData = array(
                'removed' => '1',
                'username' => 'Removed Profile',
                'activated' => '0'
            );
            $this->member->deleteMembersSetting($userid);
            $this->member->deleteMembersLookingFor($userid);
            $this->user->UpdateUser($updateData);
            redirect('user/logout');
        } else {
            $this->template->build('member/settings/remove_profile', $this->data);
        }
    }
    
    function notification()
    {
        $this->load->model('Members_model', 'member');
        $userid = $this->session->userdata('id');
        $notifications = $this->member->GetNotificationByUserId($userid);
        $displaySring = "<ul>";
        foreach ($notifications as $notification)
        {            
            $displaySring .= '<li onclick="updateNotificationRead('.$notification->id.')">'.$notification->text.' <small>( '.date('Y-m-d h:i a',  strtotime($notification->datetime)).')</small></li>';
        }
        $displaySring .= "</ul>";
        echo $displaySring;
    }
    
    function notificationupdate()
    {
        $this->load->model('Members_model', 'member');
        $id = $this->input->post('id');
        $insert_info = array(
            'id' => $id,
            'read' => 1        
        );        
        $this->member->UpdateNotification($insert_info);
        echo json_encode(array("result"=>true));
    }    
   
    
}

/* End of file member.php */
/* Location: ./application/controllers/member.php */