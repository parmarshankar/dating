<?php

class Search extends FrontControler {

    public function __construct() {
        parent::__construct();
       
      
    }

    public function index() {
        $this->search();
    }

     public function search() {
        $this->load->model('User_model', 'user');
        $this->data['location_options'] = $this->user->getUserLocations(1);
        $this->data['looking_for'] = $this->user->getUserTypes();
        $this->template->build('member/search',$this->data);
    }

    public function searchresults() {
        $this->load->model('search_model', 'search');

        $looking_for = $this->input->post('looking_for');
        $seeking = $this->input->post('seeking');
        $aged1 = $this->input->post('aged1');
        $aged2 = $this->input->post('aged2');
        $location = $this->input->post('location');
        $city_suburb = $this->input->post('city_suburb');
        $postcode = $this->input->post('postcode');
        $with_photos = $this->input->post('with_photos');
        $date_joined = $this->input->post('date_joined');


        $this->view_data['search_results'] = $this->refineUsersWithPhoto($this->search->SearchAll($aged1, $aged2, $location,$city_suburb,$postcode,$looking_for,$date_joined));
        $this->view_data['looking_for'] = $looking_for;
        $this->view_data['seeking'] = $seeking;
        $this->view_data['num_rs']  = 5;
        $this->view_data['with_photos'] = $with_photos;
        if(isset( $this->data['mem_settings']['num_searches_pp']))
        $this->view_data['num_rs'] = $this->data['mem_settings']['num_searches_pp']; 
        $this->template->build('member/search_result', $this->view_data);
    }

   

}

?>