<?php

/**
 * Description of message
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Nov 6, 2011 12:36:49 PM
 * @filesource message.php
 */
include_once 'member.php';

class Message extends Member
{

    
    public function __construct()
    {
	parent::__construct();
        $this->data['active_menu'] = 0;
        $this->template->set_partial('member_left_menu', 'partials/member_left_menu',$this->data);
    }

    public function index()
    {
       $this->show();
    }

    public function show($view=null)
    {
        if($view==null)
            $view = $this->uri->segment(3);
        
        $this->load->library('pagination');
        if($view=='' )
           $view = 'inbox';
        
        $config['base_url'] = site_url('message/show/'.$view); 
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        if(isset($this->data['mem_settings']['num_messages_pp']))
            $config['per_page'] = $this->data['mem_settings']['num_messages_pp'];  
        
	$this->load->model('messages_model', 'message');
	$userId = $this->session->userdata('id');
	$mails = array();
	$action = 'inbox';
	$limit = $this->uri->segment(4);
       
	if($view!='')
	{
	    if($view=='sent')
	    {
		$mails = $this->message->getMessages($userId,$config['per_page'], $limit,'sent');
                $c = $this->message->getMessagesCount($userId,'sent');
		$action = 'sent';
	    }
	    elseif($view=='trash')
	    {
		$mails = $this->message->getMessages($userId,$config['per_page'], $limit,'trash');
                $c = $this->message->getMessagesCount($userId,'trash');
		$action = 'trash';
	    }
             else
	    {
		$mails = $this->message->getMessages($userId,$config['per_page'], $limit);
                $c = $this->message->getMessagesCount($userId);
		
	    }
	}
	else
	{
	    $mails = $this->message->getMessages($userId,$config['per_page'], $limit);
           $c = $this->message->getMessagesCount($userId);
	}
	$this->data['mails'] = $mails;
        $config['total_rows'] =$c;
	$this->data['action'] = $action;
        $this->pagination->initialize($config);
       
       
	$this->template->build('message/index',$this->data);
    }
    function send_message()
    {

	$this->load->model('messages_model', 'message');
        $this->load->model('Lists_model', 'lists');
        $this->load->model('Members_model', 'member');
        
        $to_id =0;
        $title ='';
        
        if ($this->input->get())
	{
            
             $to_id =$_GET['to_id'];
             $title =$_GET['title'];
             
	    $insert_data = array(
		'from' => $this->session->userdata('id'),
		'to' => $_GET['to_id'],                
		'title' => $_GET['title'],
		'message' => $_GET['body'],
	    );
            
           $notAcceptMsg = 0;
           $notAcceptMsgObj = $this->member->notAcceptNewMessages($to_id);
           if(isset($notAcceptMsgObj[0]->not_accept_new_msg) && $notAcceptMsgObj[0]->not_accept_new_msg== 1)
               $notAcceptMsg = 1;   
           

            if(!$this->lists->isBlockedMe($to_id) && $notAcceptMsg==0)
                $this->message->send_message($insert_data);
	}

	if ($this->input->post())
	{            
            $to_id =$_POST['to_id'];
            $title =$_POST['title'];
            
	    $insert_data = array(
		'from' => $this->session->userdata('id'),
		'to' => $_POST['to_id'],               
		'title' => $_POST['title'],
		'message' => $_POST['body'],
	    );
            
             $notAcceptMsg = 0;
             $notAcceptMsgObj = $this->member->notAcceptNewMessages($to_id);
             if(isset($notAcceptMsgObj[0]->not_accept_new_msg) && $notAcceptMsgObj[0]->not_accept_new_msg== 1)
               $notAcceptMsg = 1;      
            

            if(!$this->lists->isBlockedMe($to_id) && $notAcceptMsg==0)
                $this->message->send_message($insert_data);
	}
        
        //send auto reply
        $rely_text = $this->message->getAutoReplyMsg($to_id);
        
               
        
        if($rely_text!= null)
        {
           $insert_data = array(
		'from' => $to_id,
		'to' => $this->session->userdata('id'),               
		'title' => 'Re: '.$title,
		'message' => $rely_text->auto_reply_text,
	    );


	    $this->message->send_message($insert_data); 
        }
        
         if($this->lists->isBlockedMe($to_id))
                 echo json_encode('Sorry, you cannot send message to this user'); 
         else if($notAcceptMsg == 1)
             echo json_encode('Sorry, this user is not accepting new messages'); 
         else 
             echo json_encode('Successfully send the message'); 
    }
    
    function reply_message()
    {

	$this->load->model('messages_model', 'message');

	if ($this->input->get())
	{
            $reply = 0;
            if(isset($_GET['reply_id']))
                $reply = $_GET['reply_id'];
            
	    $insert_data = array(
		'from' => $this->session->userdata('id'),		
                'reply' => $reply,
                'to' => $_GET['to_id'], 
		'title' => $_GET['title'],
		'message' => $_GET['body'],
	    );

	    $this->message->send_message($insert_data);
            echo json_encode(array("result"=>true));
	}

	if ($this->input->post())
	{
            $reply = 0;
            if(isset($_POST['reply_id']))
                $reply = $_POST['reply_id'];
            
	    $insert_data = array(
		'from' => $this->session->userdata('id'),		
                'reply' => $reply,
                'to' => $_POST['to_id'], 
		'title' => $_POST['title'],
		'message' => $_POST['body'],
	    );


	    $this->message->send_message($insert_data);
            echo json_encode(array("result"=>true));
	}
    }

    function view()
    {
	$get = $this->uri->uri_to_assoc(2);
	$action = $get['action'];
	$id	= $get['view'];

	$this->load->model('messages_model', 'message');
	$msg = $this->message->getMsg($id,$action);

        $this->message->updateRead($id);
        $old_msg = array();
        if($msg->reply > 0)
        {
            $this->getMessageThread($msg->reply,$action,$old_msg); 
           // print_r($this->data['old_msg']);;
        }
        else {
           $this->data['old_msg']= $old_msg; 
        }
       
	$this->data['msg'] = $msg;       
	$this->data['action']= $action;

	$this->template->build('message/view',$this->data);
    }
    
    function getMessageThread($id,$action,$msg_array)
    {
        $this->load->model('messages_model', 'message');
        
        $m = (array)$this->message->getMsg($id,$action);  
        
        $msg_array[count($msg_array)] = $m;  
       
        
        if($m['reply'] > 0)
        {  
            $this->getMessageThread($m['reply'],$action,$msg_array);
        }
        else 
        {       
            $this->data['old_msg'] = $msg_array;
           // return $msg_array;
        }
    }

    function delete()
    {
	$post  = $this->input->post();
	$this->load->model('messages_model', 'message');
	$this->message->moveToTrash($post['msg']);


	header("Content-type: application/json");
	echo json_encode(array("result"=>true));
    }
    
    function moveToInbox()
    {
        $post  = $this->input->post();
	$this->load->model('messages_model', 'message');
	$this->message->moveToInbox($post['msg']);

	header("Content-type: application/json");
	echo json_encode(array("result"=>true));
    }
    
    function deleteForever()
    {
        $post  = $this->input->post();
	$this->load->model('messages_model', 'message');
	$this->message->deleteForever($post['msg']);

	header("Content-type: application/json");
	echo json_encode(array("result"=>true));
    }
    
    function deleteChecked()
    {
        $post  = $this->input->post();
        $this->load->model('messages_model', 'message');
        
        foreach ($post['del'] as $id)
        {           
            $this->message->moveToTrash($id);
        }
        if($post['path']== 'message')
            $this->show('inbox');
        else
            $this->show('sent');
       
        
    }

}

?>