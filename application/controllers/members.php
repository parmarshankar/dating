<?php

class Members extends  CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->view_data['base_url'] = base_url();
        $this->load->model('Members_model');
        $this->load->model('User_model');
        $this->load->model('Search_model');
        $this->load->model('Messages_model');
    }


    function members_area()
    {
    if ($this->is_logged_in())
    {
    $this->load->view('view_members_area',$this->view_data);
    }
    else
    {
    $this->load->view('view_access_denied',$this->view_data);
    }
    }

    function my_photos()
    {
    if ($this->is_logged_in())
    {
    $this->view_data['photos'] = $this->Members_model->get_photos_by_id($this->session->userdata('userid'));
    $this->view_data['error'] = '';
    $this->load->view('view_my_photos',$this->view_data);


    }
    else
    {
    $this->load->view('view_access_denied',$this->view_data);
    }
    }

    function search()
    {
    if ($this->is_logged_in())
    {


    $this->load->view('view_search');
    }
    else
    {
    $this->load->view('view_access_denied',$this->view_data);
    }
    }

    function searchresults()
    {


    if ($this->is_logged_in())
    {


    $aged1         = $this->input->post('aged1');
    $aged2         = $this->input->post('aged2');
    $location     = $this->input->post('location');
    $with_photos  = $this->input->post('with_photos');


    $this->view_data['search_results']  = $this->Search_model->SearchAll($aged1,$aged2,$location,$with_photos);

    $this->load->view('view_search_results',$this->view_data);
    }
    else
    {
    $this->load->view('view_access_denied',$this->view_data);
    }
    }

    //view my profile action
    function view_profile()
    {
//    if ($this->is_logged_in())
//    {

    $userid = $this->session->userdata('id');

    //select user data by userid
    $user = $this->User_model->getUserByID($userid);
    //select profile data by userid
    $profile = $this->Members_model->GetProfileByUserId($userid);
    //zodiac sign
    //$zodiac_sign =  $user = $this->User_model->GetSign($user[''])
    $photos['photos'] = $this->Members_model->get_photos_by_id($userid);
    //get photos info

    $view_data = array_merge($user, $profile,$photos);
    //var_dump($view_data);
    $this->load->view('view_members_profile',$view_data);

//    }
//    else
//    {
//    $this->load->view('view_access_denied',$this->view_data);
//    }
    }

        //view search profile action
    function search_profile()
    {
    if ($this->is_logged_in())
    {



    //select user data by username
    $currentid = $this->User_model->getUserID($id=$this->uri->segment(3));



    //select user data by userid
    $user = $this->User_model->getUserByID($currentid);

    //select profile data by userid
    $profile = $this->Members_model->GetProfileByUserId($currentid);
    //zodiac sign
    //$zodiac_sign =  $user = $this->User_model->GetSign($user[''])
    $photos['photos'] = $this->Members_model->get_photos_by_id($currentid);
    //get photos info

    $view_data = array_merge($user, $profile,$photos);
    //var_dump($view_data);
    $this->load->view('view_search_profile',$view_data);

    }
    else
    {
    $this->load->view('view_access_denied',$this->view_data);
    }
    }

    //edit profile action
    function edit_profile()
    {
    if ($this->is_logged_in())
    {
       $userid = $this->session->userdata('userid');
    // if form was posted - save data in DB
     if ($this->input->post())
     {


    $update_data  = array(
                'userid' => $userid,
                'relationship_status' => $this->input->post('rel_status'),
                'participation_level' => $this->input->post('part_level'),
                'sexual_orientation' => $this->input->post('sex_orienatation'),
                'race_background' => $this->input->post('race'),
                'personality' => $this->input->post('personality'),
                'my_looks' => $this->input->post('my_looks'),
                'body_type' => $this->input->post('body_type'),
                'height' => $this->input->post('height'),
                'eye_colour' => $this->input->post('eye_colour'),
                'hair_colour' => $this->input->post('hair_colour'),
                'hair_length' => $this->input->post('hair_length'),
                'drinking' => $this->input->post('drinking'),
                'smoking' => $this->input->post('smoking'),
                'general_interests' =>  serialize($this->input->post('general_interests')),
                'safe_sex' => $this->input->post('safe_sex'),
                'body_hair' => $this->input->post('body_hair'),
                'public_hair' => $this->input->post('public_hair'),
                'endowment_length' => $this->input->post('endowment_length'),
                'endowment_thickness' => $this->input->post('endowment_thickness'),
                'intimate_piercings' => $this->input->post('intimate_piercings'),
                'sexual_personality' => $this->input->post('sexual_personality'),
                'sexual_interests' => serialize($this->input->post('sexual_interests')),
                'sexual_preferences' => serialize($this->input->post('sexual_preferences')),
                'fetish_interests' => serialize($this->input->post('fetish_interests')),
                'ideal_sexy_location' => $this->input->post('ideal_sexy_location'),
                'sexy_must_haves' => $this->input->post('must_have'),
                'favourite_activity' => $this->input->post('favourite_activity'),
                'profile_title' => $this->input->post('profile_title'),
                'about_me' => $this->input->post('about_me'),
                'looking_to_meet' => $this->input->post('looking_to_meet')


        );
         $this->Members_model->UpdateMembersProfile($update_data);



     }

    //select profile data by userid
     $profile = $this->Members_model->GetProfileByUserId($userid);


    $this->load->view('edit_members_profile',$profile);
    }
    else
    {
    $this->load->view('view_access_denied',$this->view_data);
    }
    }



    function is_logged_in()
    {

    $is_logged_in = $this->session->userdata('is_logged_in');

    if(!isset($is_logged_in) || $is_logged_in != true)
    {
    return false;
    }
    else
    {
    return true;
    }

    }

    function upload_photo()
    {
     if ($this->is_logged_in())
            {

                $config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '1024';
		$config['max_width']  = '2048';
		$config['max_height']  = '768';
                $config['encrypt_name'] = true;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('view_my_photos', $error);
		}
		else
		{

                    $data = $this->upload->data();

                        //add record to DB
                        $photo['userid'] = $this->session->userdata('userid');
                        $photo['photo']  = $data['file_name'];
                        $this->Members_model->insert_photo($photo);

                        $this->view_data['photos'] = $this->Members_model->get_photos_by_id($this->session->userdata('userid'));

                        $this->view_data['error'] = '';
			$this->load->view('view_my_photos',$this->view_data);
		}

    }
    else
    {
    $this->load->view('view_access_denied',$this->view_data);
    }
    }


}

?>