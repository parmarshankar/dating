<?php

class User extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->view_data['base_url'] = base_url();
        $this->view_data['user_pass_invalid'] = '';        
        $this->load->model('User_model');

    }


    


   function login()
   {

    $this->load->library('form_validation');


    $this->form_validation->set_rules('username','Username','trim|required|alpha_numeric|min_length[6]|xss_clean|strtolower');
    $this->form_validation->set_rules('password','Password','trim|required|alpha_numeric|min_length[6]|xss_clean');

   if ($this->form_validation->run() == FALSE)
       {
       $this->load->view('view_login',$this->view_data);
       }
   else
       {
       //validate credentials and redirect to memebers_area
       $username  = $this->input->post('username');
       $password  = $this->input->post('password');

       $is_valid = $this->User_model->validate($username,$password);
   
       if ($is_valid)
       {
           $userID   = $this->User_model->getUserID($username);

           $data = array(
           'username'=>$username,
           'userid' => $userID,
           'is_logged_in' =>true
       );

      
       $this->session->set_userdata($data);
       redirect(base_url().'members/members_area');
       }
       else 
       {
       $this->view_data['user_pass_invalid'] = 'Invalid username or password';        
       $this->load->view('view_login',$this->view_data);
       }

       }

   }


   function logout()
   {
   $data = array(
           'username'=>'',
           'userid' =>'',
           'is_logged_in' =>false
       );

   $this->session->unset_userdata($data);
   redirect(base_url().'welcome');
   }

   
   function register()
   {
       $this->load->library('form_validation');
       $this->load->helper('string');

       $this->form_validation->set_rules('username','Username','trim|required|alpha_numeric|min_length[6]|xss_clean|strtolower|callback_username_not_exists');
       $this->form_validation->set_rules('firstname','First name','trim|required|alpha|xss_clean');
       $this->form_validation->set_rules('lastname','Last name','trim|required|alpha|xss_clean');
       $this->form_validation->set_rules('email','Email address','trim|required|valid_email|xss_clean');
       $this->form_validation->set_rules('location','Location','trim|required|xss_clean');
       $this->form_validation->set_rules('sex','Sex','trim|required|xss_clean');
       $this->form_validation->set_rules('age','Age','trim|required|numeric|xss_clean');
       $this->form_validation->set_rules('password','Password','trim|required|alpha_numeric|min_length[6]|md5');
       $this->form_validation->set_rules('password_confirm','Password Confirmation','trim|required|alpha_numeric|min_length[6]|matches[password]|xss_clean');

       if ($this->form_validation->run() == FALSE)
       {
       $this->load->view('view_register',$this->view_data);       
       }

       else{

           $username  = $this->input->post('username');
           $firstname = $this->input->post('firstname');
           $lastname  = $this->input->post('lastname');
           $email     = $this->input->post('email');
           $location     = $this->input->post('location');
           $sex     = $this->input->post('sex');
           if ($sex=='m') $sex = 1; else $sex = 0;
           $age     = $this->input->post('age');

           $password     = $this->input->post('password');

           $activation_code = random_string('alnum', 10);

           $this->User_model->register_user($username, $firstname, $lastname, $email,$location,$sex,$age,$password,$activation_code);
           //Send an email to confirm
           $this->load->library('email');
           $this->email->from('noreply@hi-techwatch.com');
           $this->email->to($email);
           $this->email->subject('Registration confirmation');
           $this->email->message('Please click this link to confirm your registration'. anchor(base_url().'user/register_confirm/'.$activation_code,'Confirm registration'));
           $this->email->send();

           echo 'An email has been sent. Please confirm your account.';
       }

   }

   function register_confirm()
   {
   $registretion_code = $this->uri->segment(3);

   $reg_confirmed = $this->User_model->confirm_registration($registretion_code);
   
   if ($reg_confirmed)
     { 
      echo 'You have successfully registered! You can '.anchor(base_url().'user/login','login').' now';
     }
   else 
    {
      echo 'You have not registered:(';
    }

   }

   function username_not_exists($username)
   {
    $this->form_validation->set_message('username_not_exists','This username already exists, please choose another one');
    if ($this->User_model->check_exists_username($username))
    {
        return false;
    }
    else 
    {
        return true;
    }

   }

}

?>