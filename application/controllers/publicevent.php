<?php

class Publicevent extends FrontControler {

    public function __construct() {
        parent::__construct();
        $this->search();
        $this->template->set_partial('event_current', 'partials/events/event_current');
        $this->template->set_partial('event_past', 'partials/events/event_past');
    }

    public function index() {
        $this->all();
    }

    public function search() {
        $CategoryLocation = array();
        $PastCategoryLocation = array();
        $this->load->model('Event_model', 'event');
        $this->load->model('User_model', 'user');
        $categories = $this->event->getEventCategoies();
        $locations = $this->user->getLocations('all');

        foreach ($locations as $location) {
            // $CategoryLocation[$location->short_name] = 'Parties & events in '. $location->short_name;
            foreach ($categories as $cat_id => $category) {

                $CategoryLocationCount = $this->event->getEventsByCategoryLocationCount($cat_id, $location->id);
                
                $CategoryLocation[$location->short_name][$category . ' in ' . $location->short_name]['count'] = $CategoryLocationCount;
                $CategoryLocation[$location->short_name][$category . ' in ' . $location->short_name]['category'] = $cat_id;
                $CategoryLocation[$location->short_name][$category . ' in ' . $location->short_name]['location'] = $location->id;       
            }
            $PastCategoryLocationCount = $this->event->getEventsByLocationCount($location->id,'past');            
            $PastCategoryLocation['Past Events in ' . $location->short_name]['count'] = $PastCategoryLocationCount;
            $PastCategoryLocation['Past Events in ' . $location->short_name]['location'] = $location->id;   
        }

        $this->data['search'] = $CategoryLocation;
        $this->data['searchpast'] = $PastCategoryLocation;
    }

    public function all() {
        $this->load->library('pagination');
        $this->load->model('Event_model', 'event');
        $config['base_url'] = site_url('publicevent/all');
        $config['per_page'] = 5;
        $config['total_rows'] = $this->event->getEventsCount();
        $this->pagination->initialize($config);

        $limit = (int) $this->uri->segment(3);
        $events = $this->event->getEvents($config['per_page'], $limit);

        $this->data['events'] = $events;
        $this->template->build('event/index', $this->data);
    }

    public function category() {
        $this->load->library('pagination');
        $this->load->model('Event_model', 'event');
        $catId = (int) $this->uri->segment(3);
        $config['base_url'] = site_url('publicevent/category/' . $catId);
        $config['per_page'] = 5;
        $config['total_rows'] = $this->event->getEventsByCategoryCount($catId);
        $config['uri_segment'] = 4;


        $limit = (int) $this->uri->segment(4);
        $this->data['category'] = $this->event->getEventCategoy($catId);
        $events = $this->event->getEventsByCategory($catId, $config['per_page'], $limit);

        $this->data['events'] = $events;
        $this->pagination->initialize($config);
        $this->template->build('event/index', $this->data);
    }

    public function getActiveMenu($location1)
    {
        $locations = $this->user->getLocations('all');
        $count = 0;
        foreach ($locations as $location) {
            if($location->id==$location1)
            {
                $this->data['active_menu'] = $count;
                break;
            }
            $count++;
        }
    }
    public function location() {
        $this->load->library('pagination');
        $this->load->model('Event_model', 'event');
        $location = (int) $this->uri->segment(3);
        $catId = (int) $this->uri->segment(4);
        $this->data['active_menu'] = 0;
        
        
        
        
        if (isset($location) && isset($catId) && $location > -1 && $catId > 0) {
            $this->getActiveMenu($location);
            $config['base_url'] = site_url('publicevent/location/' . $location . '/' . $catId);
            $config['per_page'] = 5;
            $config['total_rows'] = $this->event->getEventsByCategoryLocationCount($catId, $location);
            $config['uri_segment'] = 5;

            $limit = (int) $this->uri->segment(5);
            $cat = $this->event->getEventCategoy($catId);
            if (count($cat) > 0) {
                $this->data['category'] = $cat;
                $loc = $this->user->getTheLocation($location);
                if (count($loc) > 0) {
                    $this->data['location'] = $loc;
                    $events = $this->event->getEventsByCategoryLocation($location, $catId, $config['per_page'], $limit);

                    $this->data['events'] = $events;
                    $this->pagination->initialize($config);
                    $this->template->build('event/index', $this->data);
                } else {
                    $this->index();
                }
            } else {
                $this->index();
            }
        } else {
            $this->index();
        }
    }
    
    public function past() {
        $this->load->library('pagination');
        $this->load->model('Event_model', 'event');
        $location = (int) $this->uri->segment(3);
        $this->data['past'] = true;

        if (isset($location) && $location > -1 ) {
            $config['base_url'] = site_url('publicevent/past/' . $location );
            $config['per_page'] = 5;
            $config['total_rows'] = $this->event->getEventsByLocationCount( $location, 'past');
            $config['uri_segment'] = 4;

            $limit = (int) $this->uri->segment(4);
           
           
                $loc = $this->user->getTheLocation($location);
                if (count($loc) > 0) {
                    $this->data['location'] = $loc;
                    $events = $this->event->getEventsByLocation($location, $config['per_page'], $limit, 'past');
                    $this->data['events'] = $events;
                    $this->pagination->initialize($config);
                    $this->template->build('event/index', $this->data);
                } else {
                    $this->index();
                }
           
        } else {
            $this->index();
        }
    }

    public function view($event_id=null) {
        if ($event_id == null)
            $event_id = (int) $this->uri->segment(4);

        $this->load->model('Event_model', 'event');
        $this->load->model('User_model', 'user');
        $event = $this->event->getEvent($event_id);
        $this->data['matched'] = 0;
        $this->data['ispast'] = 0; 

        if (!isset($event->id) || !$event->id > 0) {
            $this->data['message']['success'] = 'Wrong access';            
            $this->index();
        } else {

            $user->id = $event->event_added_by;
            $users = $this->user->getUserByUID($user->id);
            $users = $this->refineUsersWithPhoto($users);

            $this->data['event'] = $event;
            
            $exp_date = $event->held_date;
            $todays_date = date("Y-m-d");

            $today = strtotime($todays_date);
            $expiration_date = strtotime($exp_date);

            if ($expiration_date < $today) {
                 $this->data['ispast'] = true; 
                
            } 
            
            $this->data['from'] = 'publicevent';
            $this->data['event_added_by'] = $users[0];
            $this->data['guestCount'] = $this->event->getGuestCount($event_id);

            $view_by = $this->session->userdata('id');
            if ($view_by > 0) {
                $view_by = $this->user->getUserByUID($view_by);
                $this->data['view_by'] = $view_by[0];
                $this->isMatched($event, $view_by[0]);
            }

            $this->template->build('event/one', $this->data);
        }
    }

    public function isMatched($event, $user) {
        $guest_types = unserialize($event->guest_types);
        $minage = $event->aged1;
        $maxage = $event->aged2;
        $this->data['matched_type'] = 'notmatch';
        $this->data['matched_age'] = 'notmatch';

        if ($user->sex == 1)
            $user_sex = 'Man';
        if ($user->sex == 0)
            $user_sex = 'Woman';

        if (count($guest_types) > 0) {
            foreach ($guest_types as $guest_type) {
                if (trim($guest_type) == $user_sex) {
                    $this->data['matched_type'] = 'match';
                    break;
                }
            }
        }

        $user_age = (int) substr($user->age, 2, -6);
        if ($user_age >= $minage && $user_age <= $maxage)
            $this->data['matched_age'] = 'match';

        $user->age = $user_age;
        if ($this->data['matched_age'] == 'match' && $this->data['matched_type'] == 'match')
            $this->data['matched'] = 1;

        $this->data['view_by'] = $user;
    }

    public function register() {
        $this->load->model('Event_model', 'event');
        if ($this->input->post()) {
            $event_id = $this->input->post('event_id');
            $commnet = $this->input->post('comment');
            $user_id = $this->session->userdata('id');

            $event = $this->event->getEvent($event_id);
            $currentCount = $this->event->getGuestCount($event_id);


            if (($event->max_attendess != 0 && $event->max_attendess > $currentCount) || $event->max_attendess == 0) {
                $insert_data['event_id'] = $event_id;
                $insert_data['guest_id'] = $user_id;
                $insert_data['comment'] = $commnet;
                $this->event->registerEvent($insert_data);

                $this->data['message']['success'] = 'Successfully registered for this event';
                $this->view($event_id);
            } else {
                $this->data['message']['error'] = 'Sorry maximum attendess exceed';
                $this->view($event_id);
            }
        }
    }
    
    public function viewGuests($event_id=null)
    {
        if ($event_id == null)
            $event_id = (int) $this->uri->segment(3);
        
        $this->load->model('Event_model', 'event');
        $this->load->model('User_model', 'user');
        $event = $this->event->getEvent($event_id);
         $this->data['event'] = $event;
        if (!isset($event->id) || !$event->id > 0) {
            $this->data['message']['success'] = 'Wrong access';            
            $this->index();
        } else {
             $this->data['guests'] = $this->refineUsersWithPhoto($this->event->getGuests($event_id));
             $this->template->build('event/guest', $this->data);
        }
    }

}

?>