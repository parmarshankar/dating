<?php

include_once 'member.php';

class Lists extends Member {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->template->build('lists/index', $this->data);
    }

    public function sendRequest() {
        $this->load->model('Lists_model', 'lists');
        if ($this->input->post()) {
            $insert_data = array(
                'userid' => $this->session->userdata('id'),
                'list_user' => $_POST['to_id'],
                'list_code' => $_POST['code']
            );

            //Add to DB
            $this->lists->sendRequest($insert_data);

            //TODO: send a msg
            echo json_encode(array("result" => true));
        }

        //come throuh add as friend link
        if ($this->input->get()) {
            $insert_data = array(
                'userid' => $this->session->userdata('id'),
                'list_user' => $_GET['to'],
                'list_code' => 'FRIEND'
            );
            $rs = $this->lists->getRequest($this->session->userdata('id'), $_GET['to'], 'FRIEND');
            if (!isset($rs[0])) {
                $this->lists->sendRequest($insert_data);
                $this->data['message']['success'] = 'Request is sent';
            } else {
                $this->data['message']['success'] = 'Request has already sent';
            }
            $this->friends();
        }
    }

    public function sendKnownRequest() {
        $this->load->model('Lists_model', 'lists');
        $this->load->model('User_model', 'user');

        $userid = $this->session->userdata('id');
        if ($this->input->post()) {
            $uname = $_POST['uname'];

            if ($this->user->getUserID($uname) > 0) {
                $insert_data = array(
                    'userid' => $userid,
                    'list_user' => $this->user->getUserID($uname),
                    'list_code' => $_POST['code']
                );

                $rs = $this->lists->getRequest($userid, $this->user->getUserID($uname), $_POST['code']);

                if (!isset($rs[0])) {

                    $this->lists->sendRequest($insert_data);

                    if ($_POST['code'] == 'FRIEND')
                        echo json_encode('Successfully sent the request');
                    if ($_POST['code'] == 'HOT')
                        echo json_encode('Successfully added to your hot list');
                    if ($_POST['code'] == 'SHORT')
                        echo json_encode('Successfully added to your short list');
                    if ($_POST['code'] == 'BLOCK')
                        echo json_encode('Successfully blocked');
                    if ($_POST['code'] == 'PVTPHOTO')
                        echo json_encode('Successfully added to your private gallery list');
                    if ($_POST['code'] == 'PVTVIDEO')
                        echo json_encode('Successfully added to your private video list');
                }
                else {
                    if ($_POST['code'] == 'FRIEND')
                        echo json_encode('already sent request to ' . $uname);
                    if ($_POST['code'] == 'HOT')
                        echo json_encode($uname . ' is already in your hot list ');
                    if ($_POST['code'] == 'SHORT')
                        echo json_encode($uname . ' is already in your short list ');
                    if ($_POST['code'] == 'BLOCK')
                        echo json_encode($uname . ' is already in your block list ');
                    if ($_POST['code'] == 'PVTPHOTO')
                        echo json_encode($uname . ' is already in your private gallery list');
                    if ($_POST['code'] == 'PVTVIDEO')
                        echo json_encode($uname . ' is already in your private video list');
                }
            }
            else {
                echo json_encode('There is no such user');
            }
        }
    }

    public function getCurrentFriends() {
        $this->load->model('Lists_model', 'lists');
        $userid = $this->session->userdata('id');

        //FRIENDS are reverse
        $f_lists = $this->refineUsersWithPhoto($this->lists->getFriendsInLists('FRIEND', 1));
        if (is_array($f_lists))
            $f_lists = array_merge($f_lists, $this->refineUsersWithPhoto($this->lists->getFriendsInListsR('FRIEND', 1)));

        else {
            $f_lists = $this->refineUsersWithPhoto($this->lists->getFriendsInListsR('FRIEND'));
        }

        $myfrineds = array();
        foreach ($f_lists as $f_list) {
            if ($f_list->id != $userid)
                $myfrineds[$f_list->id] = $f_list;
        }

        $this->data['friend'] = $myfrineds;
    }

    public function friends() {
        $this->load->model('Lists_model', 'lists');
        $userid = $this->session->userdata('id');

        $this->getCurrentFriends();
        $this->getFriendOrder();
              
        $this->data['pending_friend_byme'] = $this->refineUsersWithPhoto($this->lists->getPendingRequestsR($userid, 'FRIEND'));
        $this->data['pending_friend'] = $this->refineUsersWithPhoto($this->lists->getPendingRequests($userid, 'FRIEND'));

        $this->template->build('lists/friends', $this->data);
    }

    public function acceptFriends() {
        $this->load->model('Lists_model', 'lists');

        if ($this->input->get()) {
            $userid = $_GET['userid'];
            $list_user = $this->session->userdata('id');
            $list_code = 'FRIEND';
            $this->lists->updateAccepted($userid, $list_user, $list_code);
        }
        $this->friends();
    }

    public function ignoreFriends() {
        $this->load->model('Lists_model', 'lists');

        if ($this->input->get()) {
            $userid = $_GET['userid'];
            $list_user = $this->session->userdata('id');
            $list_code = 'FRIEND';
            $this->lists->delete($userid, $list_user, $list_code);
        }
        $this->friends();
    }

    public function hotList() {
        $this->load->model('Lists_model', 'lists');
        $userid = $this->session->userdata('id');

        $this->getCurrentFriends();
        $this->data['my_hot_lists'] = $this->refineUsersWithPhoto($this->lists->getFriendsInLists('HOT'));
        $this->data['i_in_hot_lists'] = $this->refineUsersWithPhoto($this->lists->getFriendsInListsR('HOT'));

        $this->template->build('lists/hot', $this->data);
    }

    public function shortList() {
        $this->load->model('Lists_model', 'lists');
        $userid = $this->session->userdata('id');

        $this->getCurrentFriends();
        $this->data['my_short_lists'] = $this->refineUsersWithPhoto($this->lists->getFriendsInLists('SHORT'));
        $this->data['i_in_short_lists'] = $this->refineUsersWithPhoto($this->lists->getFriendsInListsR('SHORT'));

        $this->template->build('lists/short', $this->data);
    }

    public function blockList(){
        $this->load->model('Lists_model', 'lists');
        $userid = $this->session->userdata('id');
        
        $this->data['my_block_lists'] = $this->refineUsersWithPhoto($this->lists->getFriendsInLists('BLOCK'));        
        
        $this->template->build('lists/block', $this->data);
    }
    
    public function profileViewList() {
        $this->load->model('Lists_model', 'lists');
        $userid = $this->session->userdata('id');

        $this->getCurrentFriends();
        $this->data['my_pfv_lists'] = $this->refineUsersWithPhoto($this->lists->getMyProfileViews());
        $this->data['i_in_pfv_lists'] = $this->refineUsersWithPhoto($this->lists->getProfileViewsByMe());

        $this->template->build('lists/profile_view', $this->data);
    }
    
    public function privateAccessList() {
        $this->load->model('Lists_model', 'lists');
        $userid = $this->session->userdata('id');

        $this->getCurrentFriends();
        $this->data['my_private_photo_lists'] = $this->refineUsersWithPhoto($this->lists->getFriendsInLists('PVTPHOTO'));
        $this->data['my_private_video_lists'] = $this->refineUsersWithPhoto($this->lists->getFriendsInLists('PVTVIDEO'));

        $this->template->build('lists/private_access', $this->data);
    }
    
    public function deleteFrom() {
        $this->load->model('Lists_model', 'lists');
        $this->load->model('User_model', 'user');
        $messageSuccess = 'Successfuly deleted ';
        $count = 0;

        $userid = $this->session->userdata('id');
        if ($this->input->post()) {
            $results = $_POST['results'];
            $code = $_POST['code'];

            $unames = explode(" : ", $results);
            foreach ($unames as $uname) {
                $uname = trim($uname);
                if ($this->user->getUserID($uname) > 0) {
                    $uid = $this->user->getUserID($uname);
                    $this->lists->delete($userid, $uid, $code);
                    $messageSuccess .= $uname . ', ';
                    $count++;
                }
            }

            if ($count !== 0)
                echo json_encode($messageSuccess);
            else {
                echo json_encode('You have not selected any to delete');
            }
        }
    }

    public function deleteFriends() {
        $this->load->model('Lists_model', 'lists');
        $this->load->model('User_model', 'user');
        $messageSuccess = 'Successfuly deleted ';
        $count = 0;
        $userid = $this->session->userdata('id');
        if ($this->input->post()) {
            $results = $_POST['results'];
            $code = 'FRIEND';

            $unames = explode(" : ", $results);
            foreach ($unames as $uname) {
                $uname = trim($uname);
                if ($this->user->getUserID($uname) > 0) {
                    $uid = $this->user->getUserID($uname);
                    $this->lists->delete($userid, $uid, $code);
                    $this->lists->delete($uid, $userid, $code);
                    $this->lists->deleteFriendOrderR($userid, $uid);                    
                    $messageSuccess .= $uname . ', ';
                    $count++;
                }
            }

            if ($count !== 0)
                echo json_encode($messageSuccess);
            else {
                echo json_encode('You have not selected any to delete');
            }
        }
    }
    
    public function saveOrder()
    {
        $this->load->model('Lists_model', 'lists');
         if ($this->input->post()) {
             $order = $this->input->post('friend_order');
             $friend_ids = $this->input->post('friend_id');
             
             $this->lists->deleteFriendOrder($this->session->userdata('id'));
             
             foreach($friend_ids as $key=>$friend_id)
             {
                 if(trim($order[$key])!= '' && $order[$key] > 0)
                 {
                     $insert_data = array(
                        'userid' => $this->session->userdata('id'),
                        'friend_id' => $friend_id,
                        'order' =>$order[$key]
                    );
                     $this->lists->insertFriendOrder($insert_data);
                     $this->data['message']['success'] = 'Successfully Save';
                 }
             }             
         }
         
         $this->friends();
    }
    
    public function getFriendOrder()
    {
        $friends = array();
        $this->load->model('Lists_model', 'lists');
        $order = $this->refineUsersWithPhoto($this->lists->getFriendsByOrder($this->session->userdata('id')));
        foreach($order as $or)
        {
            $friends[$or->id] = $or;
        }
        $this->data['friend_order'] = $friends;        
    }
    
    

}

?>