<?php

/**
 * Description of profile
 *
 * @author Udana Udayanga <udanaudayanga@gmail.com>
 * @date Nov 12, 2011 11:02:50 AM
 * @filesource profile.php
 */
class Profile extends FrontControler
{

     public function __construct() {
        parent::__construct();

        if ($this->session->userdata('logged')) {
            $this->template->set_partial('member_header', 'partials/member_header');        } 
    }
    
    public function index()
    {
	$this->load->model('Members_model', 'member');
	$this->load->model('User_model', 'user');
        $this->load->model('messages_model', 'message');
        $this->load->model('Flirter_model', 'flirter');
        $this->load->model('Lists_model', 'lists');
        $this->load->model('Stats_model', 'stats');
        $this->load->model('Photo_model', 'photo');
        
	$currentid = $this->input->get('id');
         if (!$this->member->record_exists($currentid)) {
             $insert_data = array(
                'userid' => $currentid
            );
            $this->member->InsertMembersProfile($insert_data);            
         }
         
	//select user data by userid
	$user = $this->user->getUserByID($currentid);

	//select profile data by userid
	$profile = $this->member->GetProfileByUserId($currentid);
        
         $insert_data = array(
            'userid' => $currentid		
        );
         
         if(count($this->member->GetLookingForByUserId($currentid))== 0)          
            $this->member->InsertMembersLookingFor($insert_data);       
       
        
        $profile['lookingFor'] = $this->member->GetLookingForByUserId($currentid);
         $profile['usertypes'] =$this->user->getUserTypes();
	
	//get only public photos
	$photos['photos'] = $this->photo->getPublicPhotosByUser($currentid);
        
        //get ALL photos
	//$photos['privatephotos'] = $this->photo->getPhotosByUser($currentid);
        $photos['photosCount'] = $this->photo->getPhotosCountByUser($currentid);
        
       
        $unReadCount['unReadMsg'] = $this->message->getUnReadCount($currentid);        
        
        $flirters['flirters'] = $this->flirter->getFlirters();
        
        //get friends order        
        $friends['friends'] =$this->refineUsersWithPhoto($this->lists->getFriendsByOrder($this->session->userdata('id')));
        $mem_settings['mem_settings'] = $this->member->GetMemberSettingByUserId($currentid);
        
        //update profile views
        $userid = $this->session->userdata('id');
        if($currentid != $userid){
        $this->stats->updateProfileView($currentid,$userid);
        
        /*notification data*/
        $text = 'Your profile is view by <a href="'.site_url('profile?id='.$userid).'" traget="_blank">'.$this->session->userdata('firstname'). '</a>';
        $insert_info = array(
            'user_id' => $currentid,
            'text' => $text            
        );
       
        $notf = $this->member->GetNotificationByUserId($currentid);
        $dontinsert = false;
        foreach($notf as $not)
        {
            if($not->text == $text )
            {
                $dontinsert = true;
            }
        }
        if(!$dontinsert)
            $this->member->InsertNotification($insert_info);
        
        }
        
	$view_data = array_merge($user, $profile, $photos,$unReadCount,$flirters,$friends,$mem_settings);
	//var_dump($view_data);       
	$this->template->build('profile/index',$view_data);

    }
    
    public function photos()
    {
        $currentid = $this->uri->segment(3);
        $album = $this->uri->segment(4);
        $userid = $this->session->userdata('id');
        $hasAccess = 0;
        if($currentid==$userid) $hasAccess = 1;        
        
        
        $this->load->model('Photo_model', 'photo');
        $privatephotos = $this->photo->getPhotosByUserAbllum($currentid,$album);
        
        if($this->photo->hasAccess($currentid) && $privatephotos[0]->ispublic==0) $hasAccess = 1;
        if($privatephotos[0]->ispublic==1) $hasAccess = 1;
        
        $this->data['photos'] = $privatephotos;
        $this->data['hasAccess'] = $hasAccess;
        $this->data['currentid'] = $currentid;
        
        $this->template->build('profile/album',$this->data);        
        
    }
    
    public function statistics()
    {        
         $this->load->model('stats_model', 'stats');
         $userid = $this->session->userdata('id');
         $LoginCount = $this->stats->getLoginCount($userid);
         $this->data['logged_count'] = $LoginCount->logged_count;
         $this->data['view_count_all'] = $this->stats->getProfileViewCount($userid);
         $thisMonth = date('Y-m-').'1';
         $this->data['view_count_this'] = $this->stats->getProfileViewCount($userid,$thisMonth);
         $this->data['view_count_last'] = $this->stats->getProfileViewCount($userid,$LoginCount->last_login);
         $this->data['i_view_count'] = $this->stats->getProfileViewMyCount($userid,$thisMonth);
         
         $this->data['friends'] = $this->stats->getFriendCount($userid);
         
         $this->data['messages']['from']= $this->stats->getMessageFromCount($userid);
         $this->data['messages']['to']= $this->stats->getMessageToCount($userid);
         $this->data['messages']['reply'] = $this->stats->getMessageReplyCount($userid);
         $this->data['flirter']['from']= $this->stats->getFlirterFromCount($userid);
         $this->data['flirter']['to']= $this->stats->getFlirterToCount($userid);
         $this->data['flirter']['reply'] = $this->stats->getFlirterReplyCount($userid);
         $this->data['hot_count'] = $this->stats->hotListCount($userid);
         $this->data['short_count'] = $this->stats->shortListCount($userid);
         
         $this->data['event_post_count'] = $this->stats->getPostEventCount($userid);
         $this->data['event_interest_count'] = $this->stats->getInterestEventCount($userid);
         
                  $messaage['success'] = "Can't keep track? We've got your stats in order. Please click to view:";
         $this->data['message'] = $messaage;                 
         $this->template->build('profile/statistics',$this->data);  
    }
    
   

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */