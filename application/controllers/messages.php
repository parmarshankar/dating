<?php
class Messages extends CI_Controller {

	function __construct()
        {
        parent::__construct();
        $this->view_data['base_url'] = base_url();
        //$this->load->model('Message_model');
        $this->load->model('Messages_model');
        }



        function send_message()
        {


        if ($this->input->get())
        {

        $insert_data = array(
        'from_id' => $this->session->userdata('userid'),
        'to_id'   => $_GET['to_id'],
        'title'   => $_GET['title'],
        'message'    => $_GET['body'],
        );

        //var_dump($insert_data);


        $this->Messages_model->send_message($insert_data);
        }

        if ($this->input->post())
        {

        $insert_data = array(
        'from_id' => $this->session->userdata('userid'),
        'to_id'   => $_POST['to_id'],
        'title'   => $_POST['title'],
        'message' => $_POST['body'],
        );

        //var_dump($insert_data);


        $this->Messages_model->send_message($insert_data);
        }

        }

}
?>