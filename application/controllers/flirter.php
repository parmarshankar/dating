<?php


include_once 'member.php';

class Flirter extends Member
{    
    public function __construct()
    {
	parent::__construct();
        $this->data['active_menu'] = 1;
        $this->template->set_partial('member_left_menu', 'partials/member_left_menu',$this->data);
      
    }

    public function show($view=null)
    {
         if($view==null)
            $view = $this->uri->segment(3);
         
        $view = $this->uri->segment(3);
        $this->load->library('pagination');
         if($view=='')
           $view = 'inbox';
        
        
        $config['base_url'] = site_url('flirter/show/'.$view); 
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        if(isset($this->data['mem_settings']['num_flirts_pp']))
            $config['per_page'] = $this->data['mem_settings']['num_flirts_pp'];  
        
        
	$this->load->model('flirter_model', 'flirter');
	$userId = $this->session->userdata('id');
	$mails = array();
	$action = 'inbox';
        $limit = $this->uri->segment(4);
        
	
	if($view!='')
	{
	    if($view=='sent')
	    {
		$mails = $this->flirter->getUserFlirters($userId,$config['per_page'], $limit,'sent');
                $c = $this->flirter->getUserFlirtersCount($userId,'sent');
		$action = 'sent';
	    }
	    elseif($view=='trash')
	    {
		$mails = $this->flirter->getUserFlirters($userId,$config['per_page'], $limit,'trash');
                 $c = $this->flirter->getUserFlirtersCount($userId,'trash');
		$action = 'trash';
	    }
            else
	    {
		$mails = $this->flirter->getUserFlirters($userId,$config['per_page'], $limit);
                $c = $this->flirter->getUserFlirtersCount($userId);
		
	    }
	}
	else
	{
	    $mails = $this->flirter->getUserFlirters($userId,$config['per_page'], $limit);
            $c = $this->flirter->getUserFlirtersCount($userId);
	}        
	$this->data['mails'] = $mails;
	$this->data['action'] = $action;
        $config['total_rows'] =$c;
        $this->pagination->initialize($config);
	$this->template->build('flirter/index',$this->data);
    }
    
    public function index()
    {
	$this->show();
    }
    
    
    public function send()
    {
        $this->load->model('flirter_model', 'flirter');
         $this->load->model('Lists_model', 'lists');
         $this->load->model('user_model', 'user');
         
	$userId = $this->session->userdata('id');
        $to_id = $this->input->get('to_id'); 
        $fid = $this->input->get('fid'); 
        $thisFlitr =  $this->flirter->getFlirter($fid);
        $insert_data = array(
		'from' => $this->session->userdata('id'),
		'to' => $to_id,	
		'message' => $thisFlitr->text,
	    );
        
        if(!$this->lists->isBlockedMe($to_id))
            $this->flirter->send_flirter($insert_data);	
       
         $data = $this->user->profile($to_id);
         
        if($this->lists->isBlockedMe($to_id))
              $data['message']['error']='Sorry, you cannot send flirters to this user'; 
         else
             $data['message']['success']='Successfully send the flirter'; 
        
        
         $this->template->build('profile/index',$data);

       
    }
    
    function reply_flirter()
    {

	$this->load->model('flirter_model', 'flirter');

	if ($this->input->get())
	{
            $reply = 0;
            if(isset($_GET['reply_id']))
                $reply = $_GET['reply_id'];
            
	    $insert_data = array(
		'from' => $this->session->userdata('id'),		
                'reply' => $reply,
                'to' => $_GET['to_id'],		
		'message' => $_GET['body'],
	    );

	    $this->flirter->send_flirter($insert_data);
            echo json_encode(array("result"=>true));
	}

	if ($this->input->post())
	{
            $reply = 0;
            if(isset($_POST['reply_id']))
                $reply = $_POST['reply_id'];
            
	    $insert_data = array(
		'from' => $this->session->userdata('id'),		
                'reply' => $reply,
                'to' => $_POST['to_id'], 		
		'message' => $_POST['body'],
	    );


	    $this->flirter->send_flirter($insert_data);
            echo json_encode(array("result"=>true));
	}
    }
    
     function view()
    {
	$get = $this->uri->uri_to_assoc(2);
	$action = $get['action'];
	$id	= $get['view'];

	$this->load->model('flirter_model', 'flirter');
	$flt = $this->flirter->getUserFlirter($id,$action);

        $this->flirter->updateRead($id);
        $old_msg = array();
        if($flt->reply > 0)
        {
            $this->getFlirterThread($flt->reply,$action,$old_msg);            
        }
        else {
           $this->data['old_msg']= $old_msg; 
        }
       
	$this->data['msg'] = $flt;       
	$this->data['action']= $action;
        $this->data['flirters'] = $this->flirter->getFlirters();
	$this->template->build('flirter/view',$this->data);
    }
    
    function getFlirterThread($id,$action,$msg_array)
    {
        $this->load->model('flirter_model', 'flirter');
        
        $m = (array)$this->flirter->getUserFlirter($id,$action);  
        
        $msg_array[count($msg_array)] = $m;  
       
        
        if($m['reply'] > 0)
        {  
            $this->getFlirterThread($m['reply'],$action,$msg_array);
        }
        else 
        {       
            $this->data['old_msg'] = $msg_array;           
        }
    }
    
     function delete()
    {
	$post  = $this->input->post();
	$this->load->model('flirter_model', 'flirter');
	$this->flirter->moveToTrash($post['msg']);


	header("Content-type: application/json");
	echo json_encode(array("result"=>true));
    }
    
    function moveToInbox()
    {
        $post  = $this->input->post();
	$this->load->model('flirter_model', 'flirter');
	$this->flirter->moveToInbox($post['msg']);

	header("Content-type: application/json");
	echo json_encode(array("result"=>true));
    }
    
    function deleteForever()
    {
        $post  = $this->input->post();
	$this->load->model('flirter_model', 'flirter');
	$this->flirter->deleteUserFlirter($post['msg']);

	header("Content-type: application/json");
	echo json_encode(array("result"=>true));
    }
    
    function deleteChecked()
    {
        $post  = $this->input->post();
        $this->load->model('flirter_model', 'flirter');
        
        foreach ($post['del'] as $id)
        {           
            $this->flirter->moveToTrash($id);
        }
        if($post['path']== 'flirter')
            $this->show('inbox');
        else
            $this->show('sent');
    }

   

}

?>